﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mHardklor_2;

namespace AcquisitionMethod
{
    class PepIsotopes
    {
        public readonly List<double> IsotopeMZs = new List<double>();
        public readonly int BasepeakIndex = 0;
        //public readonly List<double> IsotopeInts = new List<double>();
        private int _lifecycle = 0;
        private DateTime _deadline = DateTime.Now;
        /// <summary>
        /// </summary>
        /// <param name="pep"></param>
        /// <param name="lifecycle">in minisecond</param>
        public PepIsotopes(PeptideMatch pep, int lifecycle)
        {
            _lifecycle = lifecycle;


            //CaluculateIsotopeMZs(pep);
            double monoMZ = (pep.monoMass + Molecules.Proton * pep.charge) / pep.charge;
            int idx = (int)Math.Round((monoMZ - pep.baseMZ) * pep.charge, 0);
            double offset = Molecules.Proton / pep.charge;
            BasepeakIndex = 0 - idx;

            IsotopeMZs.Add(monoMZ); // the first peak is monoMZ
            //// use the mz value of baseMZ to calculate the mz values of other isotopes
            for (int i = 1; i < pep.distribution.Count; i++)
            {
                double dis = (idx + i) * offset;
                IsotopeMZs.Add(pep.baseMZ + dis);
                //double ratio = pep.distribution[i]/pep.distribution[idx];
                //IsotopeInts.Add(pep.intensity*ratio);
            }

            //for (int i = 0; i < 5; i++)
            //{
            //    double dis = (i)*offset;
            //    IsotopeMZs.Add(pep.baseMZ+dis);
            //}
        }

        public DateTime Deadline
        {
            get { return _deadline; }
        }
        public void StartCount()
        {
            _deadline = DateTime.Now + new TimeSpan(0, 0, 0, 0, _lifecycle);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class BinsResult
    {
        /// <summary>
        /// the value of this dictionary is a list of ions in particular bin 
        /// the ions are sorted based on their monoMZ
        /// </summary>
        private Dictionary<int, List<Ion>> _binIdxToIons = new Dictionary<int, List<Ion>>();
        private readonly int _minCharge = 2;
        private readonly int _maxCharge = 4;
        private readonly double _threshold;
        public Ion RepresentIon = null;
        public BinsResult(Dictionary<int,List<Ion>> binIdxToIons,int minCharge, int maxCharge, double threshold)
        {
            _binIdxToIons = binIdxToIons;
            _maxCharge = maxCharge;
            _minCharge = minCharge;
            _threshold = threshold;
            //    _para = parameter;
        }
        public BinsResult(Dictionary<int,List<Ion>> binIdxToIons,int minCharge, int maxCharge):this (binIdxToIons,minCharge,maxCharge,0d){}

        public List<Ion> QualifiedIons(int idx)
        {
            List<Ion> qualifiedIons = new List<Ion>();
            if (_binIdxToIons.ContainsKey(idx))
            {
                double value = 0d;
                int count = 0;

                // charge [filter]
                foreach (Ion ion in _binIdxToIons[idx])
                {
                    if (ion.Charge < _minCharge || ion.Charge > _maxCharge)
                        continue;
                    qualifiedIons.Add(ion);
                }

                #region I tried differnt things to decide which m/z window we are going to analyze

                //switch (_para.type)
                //{
                //    case Parameter.CountingType.AveragePeakInt:
                //        foreach (Ion ion in qualifiedIons)
                //        {
                //            value += ion.Intensity;
                //            count++;
                //        }
                //        if (count > 0)
                //            value = Math.Log10(value / count);
                //        break;
                //    case Parameter.CountingType.TopPeakInt:
                //        Ion hion = null;
                //        foreach (Ion ion in qualifiedIons)
                //        {
                //            if (hion == null || hion.Intensity < ion.Intensity)
                //                hion = ion;
                //        }
                //        if (hion != null)
                //            value = Math.Log10(hion.Intensity);
                //        break;
                //    case Parameter.CountingType.NumberOfPeak:
                //        value = qualifiedIons.Count;
                //        break;
                //}

                #endregion
            }
            return qualifiedIons;
        }

        public Dictionary<int, double> QualifiedBins()
        {
            Dictionary<int, double> matched = new Dictionary<int, double>();
            foreach (int bidx in _binIdxToIons.Keys)
            {
                int value = QualifiedIons(bidx).Count;

                #region I tried differnt things to decide which m/z window we are going to analyze

                //switch (_para.type)
                //{
                //    case Parameter.CountingType.AveragePeakInt:
                //        foreach (Ion ion in qualifiedIons)
                //        {
                //            value += ion.Intensity;
                //            count++;
                //        }
                //        if (count > 0)
                //            value = Math.Log10(value / count);
                //        break;
                //    case Parameter.CountingType.TopPeakInt:
                //        Ion hion = null;
                //        foreach (Ion ion in qualifiedIons)
                //        {
                //            if (hion == null || hion.Intensity < ion.Intensity)
                //                hion = ion;
                //        }
                //        if (hion != null)
                //            value = Math.Log10(hion.Intensity);
                //        break;
                //    case Parameter.CountingType.NumberOfPeak:
                //        value = qualifiedIons.Count;
                //        break;
                //}

                #endregion

                if (value >= _threshold)
                    matched.Add(bidx, value);
            }
            return matched;
        }

        
    }
}

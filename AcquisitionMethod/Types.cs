﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class Types
    {
        public enum ScanType
        {
            RegularMSone = 11,
            RegularMStwo = 22,
            sDIA = 33,
            SIM = 44,
            None = 55,
            aDIA = 66,
        }

        //public enum ScanType
        //{
        //    Custom,
        //    Repeat,
        //}

        public enum ScanOrder
        {
            MS,
            MS2,
        }

        //public enum AcquisitionType
        //{
        //    Time,
        //    Count,
        //}
        public enum AcuqisitionMethodType
        {
            None,
            Naive,
            UpdateList,
            SimpleN,
            BinVotes,
            FeatureVotes,
            mDDA,
            BinCharge,
            DDABin,
            DynamicMS,
            TargetDDAp,
            DIADA,
            DIAIA,
        }
        public enum AcquisitionOrder
        {
            Forward,
            Reverse
        }
    }
}

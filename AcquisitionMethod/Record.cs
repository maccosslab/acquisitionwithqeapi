﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    internal class Record
    {
        //BinsResult
        /// <summary>
        /// this is sorted index list
        /// </summary>
        public readonly List<int> IndexList = null;
        public readonly BinsResult BinIdx = null;
        public double mzStart;
        public double mzEnd;
        public Record(BinsResult binIdx)
        {
            IndexList = binIdx.QualifiedBins().Keys.ToList();
            IndexList.Sort();
            BinIdx = binIdx;
        }
    }
}

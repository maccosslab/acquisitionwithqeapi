﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace AcquisitionMethod
{
    public class ParameterFileIO
    {

        public static Parameter GetParameterSetting(string filepath)
        {
            Parameter parameter = new Parameter();
            parameter.ParameterFileName = Path.GetFileNameWithoutExtension(filepath);
            if (File.Exists(filepath))
            {
                UpdateParameterWithFileContent(filepath, ref parameter);
            }
            
            return parameter;
        }

        private static void UpdateParameterWithFileContent(string filepath, ref Parameter parameter)
        {
            StreamReader sr = new StreamReader(filepath);
            List<double> reporterions = new List<double>();
            MSScanParameter msScanParameter = null;

            double value = 0d;
            double isolationwidth = 0d;
            double startmz = 0d;
            double endmz = 0d;
            float fvalue = 0f;
            string[] range;

            while (sr.Peek() != -1)
            {
                string oneline = sr.ReadLine();
                if (oneline.StartsWith("#"))
                    continue;
                string[] info = oneline.Split("\t".ToCharArray());
                if (info.Length >= 2)
                {
                    string content = info[1].Trim();
                    //Console.WriteLine(info[0]);

                    switch (info[0])
                    {
                        
                        case "Acquisition Method:":
                            Types.AcuqisitionMethodType selected =
                                (Types.AcuqisitionMethodType) Enum.Parse(typeof (Types.AcuqisitionMethodType), content);
                            parameter.AcquisitionMethod = selected;
                            break;
                        case "Isolate on basepeak or monoisotopic peak:":

                            if (content == "Mono")
                                parameter.UseMonoMZToDecideBins = true;
                            else
                                parameter.UseMonoMZToDecideBins = false;

                            break;
                        case "Precursor m/z range:":
                            range = content.Split("-".ToCharArray());
                            if (double.TryParse(range[0], out startmz) && double.TryParse(range[1], out endmz))
                            {
                                if (startmz < endmz)
                                {
                                    parameter.startMZ = startmz;
                                    parameter.endMZ = endmz;
                                }
                            }
                            break;
                        case "Isolation window width:":
                            if (double.TryParse(content, out isolationwidth))
                                parameter.windowWidth = isolationwidth;
                            break;
                        case "Peptide charge states:":
                            range = content.Split("-".ToCharArray());
                            int min;
                            int max;
                            if (int.TryParse(range[0], out min) && int.TryParse(range[1], out max))
                            {
                                if (min < max && max <= 5)
                                {
                                    parameter.minCharge = min;
                                    parameter.maxCharge = max;
                                }
                            }
                            break;
                        case "Peptide intensity threshold:":
                            float threshold;
                            if (float.TryParse(content, out threshold))
                            {
                                parameter.IntensityThreshold = threshold;
                            }
                            break;
                        case "Loop count (TopN):":
                            int topn;
                            if (int.TryParse(content, out topn))
                                parameter.TopN = topn;
                            break;
                        case "Dynamic exclustion (sec):":
                            int sec;
                            if (int.TryParse(content, out sec))
                                parameter.DynamicExclusion = sec;
                            break;
                        case "Dynamic exclustion (ppm):":
                            int ppm;
                            if (int.TryParse(content, out ppm))
                                parameter.PepTolerance = ppm;
                            break;
                        case "Exclude isotopes:":
                            bool excludeiso;
                            if (bool.TryParse(content, out excludeiso))
                                parameter.ExcludeIsotopes = excludeiso;
                            break;
                        case "Scan type:":
                            switch (content)
                            {
                                case"MS one":
                                    msScanParameter = parameter.MSoneSetting;
                                    msScanParameter.NCE = "0";
                                    //Console.WriteLine("MSone");
                                    msScanParameter.IsolationLow = startmz.ToString();
                                    msScanParameter.IsolationHigh = endmz.ToString();
                                    msScanParameter.FirstMass = startmz.ToString();
                                    msScanParameter.LastMass = endmz.ToString();
                                    break;
                                case "MS two":
                                    msScanParameter = parameter.MStwoSetting;
                                    msScanParameter.IsolationWindowWidth = isolationwidth.ToString();
                                    //Console.WriteLine("MStwo");
                                    break;
                                case "SIM":
                                    msScanParameter = parameter.SIMSetting;
                                    msScanParameter.NCE = "0";
                                    //Console.WriteLine("SIM");
                                    break;
                                case "sDIA":
                                    msScanParameter = parameter.sDIASetting;
                                    //Console.WriteLine("DIA");
                                    break;
                                case "aDIA":
                                    msScanParameter = parameter.aDIASetting;
                                    break;
                            }
                            //this will be overwirte if the scan type is DIA scan
                            //msScanParameter.FirstMass = startmz.ToString();
                            //msScanParameter.LastMass = endmz.ToString();
                            msScanParameter.Polarity = "0";
                            break;
    
                        case "Scan order:":
                            switch (content)
                            {
                                case "MS":
                                    msScanParameter.Order = Types.ScanOrder.MS;

                                    break;
                                case "MS2":
                                    msScanParameter.Order = Types.ScanOrder.MS2;
                                    break;
                            }
                            break;

                        case "Resolution:":

                            if (double.TryParse(content, out value))
                            {
                                if (value > 0)
                                    msScanParameter.Resolution = content;
                            }

                            break;
                        case "AGC Target:":
                            if (double.TryParse(content, out value))
                            {
                                if (value > 0)
                                    msScanParameter.AGC_Target = content;
                            }

                            break;
                        case "MaxIT:":
                            if (double.TryParse(content, out value))
                            {
                                if (value > 0)
                                    msScanParameter.MaxIT = content;
                            }

                            break;
                        case "NCE:":
                            if (double.TryParse(content, out value))
                            {
                                if (value > 0)
                                    msScanParameter.NCE = content;
                            }

                            break;
                        case "Processing delay:":
                            if (double.TryParse(content, out value))
                            {
                                if (value > 0)
                                    msScanParameter.SingleProcessingDelay = value;
                            }

                            break;
                        case "DIA window width:":
                            if (double.TryParse(content, out value))
                            {
                                if (value > 0)
                                    msScanParameter.IsolationWindowWidth = content;
                            }
                            break;
                        case "Target reporter ions:":
                            string[] mzs = content.Split(";".ToCharArray());
                            foreach (string mz in mzs)
                            {
                                if (double.TryParse(mz, out value))
                                {
                                    reporterions.Add(value);
                                }
                            }

                            parameter.ReportIons.Clear();
                            parameter.ReportIons.AddRange(reporterions);

                            break;
                        case "Reporter ion in # consecutive scans:":
                            int n = 1;
                            if (int.TryParse(content, out n))
                            {
                                if (n < 1)
                                    n = 1;
                            }
                            parameter.ReporterIonInNscans = n;
                            break;
                        case "Match All reporter ion:":
                            bool ans = bool.Parse(content);
                            if (ans)
                                parameter.MatchAllRepoeters = true;
                            else
                                parameter.MatchAllRepoeters = false;
                            break;

                        case "Reporter ion intensity threshold:":
                            if (float.TryParse(content, out fvalue))
                            {
                                if (fvalue >= 0)
                                    parameter.FragmentIntThreshold = fvalue;
                            }
                            break;
                    }
                }
            }
            sr.Close();
        }

        public static Types.AcuqisitionMethodType AcquisitionTypeInSettingFile(string filepath)
        {
            Types.AcuqisitionMethodType actype = Types.AcuqisitionMethodType.None;
            if (!File.Exists(filepath))
                return actype;

            StreamReader sr = new StreamReader(filepath);
            while (sr.Peek() != -1)
            {
                string online = sr.ReadLine();
                if (online.StartsWith("Acquisition Method"))
                {
                    string[] info = online.Split('\t');
                    string method = info[1].Trim();
                    try
                    {
                        actype = (Types.AcuqisitionMethodType) Enum.Parse(typeof (Types.AcuqisitionMethodType), method);
                    }
                    catch (ArgumentException e)
                    {
                    }
                    break;
                }
            }
            sr.Close();
            return actype;
        }

        public static void WriteCurrentParameter(Parameter parameter, string outputpath)
        {
            StreamWriter sw = new StreamWriter(outputpath);
            StringBuilder sb = new StringBuilder();
            
            //sb.AppendLine("<Acquisition>"+parameter.AcquisitionMethod.ToString()+"</Acquisition>");
            //sb.AppendLine("<Parameters>");
            //sb.AppendLine("<Common>");
            //sb.AppendLine("<>");
            //sb.AppendLine("</Common>");
            //sb.AppendLine("</Parameters>");

            sb.AppendLine("Acquisition Method:\t" + parameter.AcquisitionMethod.ToString());
            sb.Append("Isolate on basepeak or monoisotopic peak:\t");
            if (parameter.UseMonoMZToDecideBins)
                sb.AppendLine("Mono");
            else
                sb.AppendLine("BasePeak");
            sb.AppendLine("Precursor m/z range:\t"+parameter.startMZ+"-"+parameter.endMZ);
            sb.AppendLine("Isolation window width:\t" + parameter.windowWidth);
            sb.AppendLine("Peptide charge states:\t" + parameter.minCharge + "-" + parameter.maxCharge);
            //sb.AppendLine("Polarity:\t"+parameter.MSoneSetting.Polarity);
            //sb.AppendLine("Polarity:\t" + parameter.MStwoSetting.Polarity);
            sb.AppendLine("Peptide intensity threshold:\t" + parameter.IntensityThreshold);
            if( parameter.AcquisitionMethod == Types.AcuqisitionMethodType.mDDA)
            {
                sb.AppendLine("Loop count (TopN):\t"+parameter.TopN);
                sb.AppendLine("Dynamic exclustion (sec):\t" + parameter.DynamicExclusion);
                sb.AppendLine("Dynamic exclustion (ppm):\t" + parameter.PepTolerance);
                sb.AppendLine("Exclude isotopes:\t" + parameter.ExcludeIsotopes);
            }

            sb.AppendLine("\n#### SCAN SETTING ###");
            sb.AppendLine("Scan type:\tMS one");
            sb.AppendLine(ReportMSScanParameter(parameter.MSoneSetting));
            //sb.AppendLine("Scan m/z Range:\t" + parameter.MSoneSetting.FirstMass + "-" + parameter.MSoneSetting.LastMass);

            if (parameter.AcquisitionMethod != Types.AcuqisitionMethodType.DIAIA)
            {
                sb.AppendLine("##");
                sb.AppendLine("Scan type:\tMS two");
                sb.AppendLine(ReportMSScanParameter(parameter.MStwoSetting));
            }

            
            if (parameter.AcquisitionMethod == Types.AcuqisitionMethodType.DIADA ||
                parameter.AcquisitionMethod == Types.AcuqisitionMethodType.DIAIA)
            {
                sb.AppendLine("##");
                sb.AppendLine("Scan type:\tsDIA");
                sb.Append(ReportMSScanParameter(parameter.sDIASetting));
                sb.AppendLine("DIA window width:\t" + parameter.sDIASetting.IsolationWindowWidth+"\n");

                if (parameter.AcquisitionMethod == Types.AcuqisitionMethodType.DIADA)
                {
                    sb.AppendLine("##");
                    sb.AppendLine("Scan type:\tSIM");
                    sb.AppendLine(ReportMSScanParameter(parameter.SIMSetting));
                }
                else
                {
                    sb.AppendLine("##");
                    sb.AppendLine("Scan type:\taDIA");
                    sb.Append(ReportMSScanParameter(parameter.aDIASetting));
                    sb.AppendLine("DIA window width:\t" + parameter.aDIASetting.IsolationWindowWidth);
                }
                
                //sb.AppendLine("Scan m/z Range:\t" + parameter.DIASetting.FirstMass + "-" + parameter.DIASetting.LastMass);

                sb.AppendLine("\n###DIADA/DIAIA specific parameters###");

                sb.Append("Target reporter ions:\t");
                if (parameter.ReportIons.Count == 0)
                    sb.AppendLine("None");
                else
                {
                    StringBuilder ions = new StringBuilder();
                    ions.Append(parameter.ReportIons[0].ToString());
                    for (int i = 1; i < parameter.ReportIons.Count; i++)
                        ions.Append(";" + parameter.ReportIons[i].ToString());
                    sb.AppendLine(ions.ToString());
                }
                sb.AppendLine("Reporter ion in # consecutive scans:\t"+parameter.ReporterIonInNscans.ToString());
                sb.AppendLine("Match All reporter ion:\t"+parameter.MatchAllRepoeters.ToString());
                sb.AppendLine("Reporter ion intensity threshold:\t" + parameter.FragmentIntThreshold.ToString());
                //sb.Append("Target neutral loss:\t")
            }
            
            sw.WriteLine(sb.ToString());
            //Console.WriteLine(sb.ToString());
            sw.Close();
        }
        //public string 

        private static string ReportMSScanParameter(MSScanParameter mssetting)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Scan order:\t"+mssetting.Order);
            sb.AppendLine("Resolution:\t" + mssetting.Resolution);
            sb.AppendLine("AGC Target:\t"+mssetting.AGC_Target);
            sb.AppendLine("MaxIT:\t" + mssetting.MaxIT);
            sb.AppendLine("NCE:\t" + mssetting.NCE);
            sb.AppendLine("Processing delay:\t" + mssetting.SingleProcessingDelay.ToString());

            //sb.AppendLine("IsolationWidth:\t"+mssetting.IsolationWindowWith);
            //sb.AppendLine("First m/z:\t" + mssetting.FirstMass);
            //sb.AppendLine("End m/z:\t" + mssetting.LastMass);
            //sb.AppendLine("Isolation Low:\t" + mssetting.IsolationLow);
            //sb.AppendLine("Isolation High:\t" + mssetting.IsolationHigh);

            Console.WriteLine(sb.ToString());
            return sb.ToString();
        }
    }
}

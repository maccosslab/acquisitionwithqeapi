﻿using System;
using System.Collections.Generic;
using System.Text;

// this is copy from Chemicals project
//namespace Chemicals
namespace AcquisitionMethod
{
    public class Molecules
    {
        static double proton = 1.00782503207d;
        static double nitrogen = 14.0030740048d;
        static double hydrogen = 1.00782503207d;
        static double carbon = 12d;
        static double oxygen = 15.99491461956d;

        public static double Cterm { get { return oxygen + hydrogen; } }
        public static double Nterm { get { return hydrogen; } }
        public static double Water { get { return hydrogen + hydrogen + oxygen; } }
        public static double Ammonia { get { return nitrogen + hydrogen + hydrogen + hydrogen; } }
        public static double CO { get { return oxygen +carbon; } }
        public static double NH { get { return nitrogen + hydrogen; } }
        public static double Phosphate { get { return 79.9663304084d; } }

        public static double Proton { get { return proton; } }
        public static double Nitrogen { get { return nitrogen; } }
        public static double Hydrogen { get { return hydrogen; } }
        public static double Carbon { get { return carbon; } }
        public static double Oxygen { get { return oxygen; } }
        

    }
}

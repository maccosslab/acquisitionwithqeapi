﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using Thermo.Interfaces.ExactiveAccess_V2.Control.Methods;
using Thermo.Interfaces.InstrumentAccess_V1;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Acquisition;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Acquisition.Modes;
using Thermo.Interfaces.ExactiveAccess_V1;
using Thermo.Interfaces.ExactiveAccess_V1.Control;
using Thermo.Interfaces.ExactiveAccess_V1.Control.Acquisition;
using Thermo.Interfaces.ExactiveAccess_V1.Control.Acquisition.Workflow;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Methods;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Scans;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;


using System.IO;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace AcquisitionMethod
{
	/// <summary>
	/// This class manages the instrument access and general functionality. The program will stop after
	/// some minutes.
	/// </summary>
    public class InstrumentInstance : IDisposable
	{
		private int m_disposed = 0;
		private ManualResetEvent m_wait = null;
		static private double m_frequency = 0;
		static private long m_startCounter = 0;
		static private DateTime m_startTime = DateTime.MinValue;

        private string instrumentID = "Thermo Exactive.API_Clr2_32_V1"; // mask on 2016 Aug 05 

        /// <summary>
        /// Name of the instrument in the registry.
        /// </summary>
        private const string InstrumentName = "Thermo Exactive";

        /// <summary>
        /// This string will be used as a registry value name to access the file name of the API assembly.
        /// </summary>
        private const string ApiFileNameDescriptor = "ApiFileName_Clr2_32_V1";

        /// <summary>
        /// This string will be used as a registry value name to access the class name of the API.
        /// </summary>
        private const string ApiClassNameDescriptor = "ApiClassName_Clr2_32_V1";



        private int OperationTime = 100 * 1000;
        private bool _success = true;
	    /// <summary>
	    /// Create a new <see cref="HybridMethod.InstrumentInstance"/>
	    /// </summary>
	    /// <param name="arguments">arguments that drive the behavious of this instance</param>
	    public InstrumentInstance()
	    {

            

            # region base on old api example code 
            ////check COM connection
            //Type type = null;
            //try
            //{
            //    type = Type.GetTypeFromProgID(instrumentID, true);
            //}
            //catch (Exception e1)
            //{
            //    string message = e1.Message + "\n\nCannot determine the type of the API via COM.\nIs the desired Exactive library registered?";
            //    Exception error = new Exception(message);
            //    if (e1.InnerException != null)
            //    {
            //        Console.WriteLine("(" + e1.InnerException.Message + ")");
            //    }
            //    throw error;
            //}


            //object o = null;
            //try
            //{
            //    o = Activator.CreateInstance(type);
            //    if (o == null)
            //    {
            //        throw new Exception("Cannot create an instance of " + type.FullName);
            //    }
            //}
            //catch (Exception e2)
            //{
            //    Console.WriteLine("Cannot create an object of the API via COM.");
            //    Console.WriteLine(e2.Message);
            //    if (e2.InnerException != null)
            //    {
            //        Console.WriteLine("(" + e2.InnerException.Message + ")");
            //    }
            //    Console.WriteLine();
            //    Console.WriteLine("Are the desired Exactive library and its dependencies registered?");
            //    Console.WriteLine("Is the machine platform well set?");
                
            //    throw e2;
            //    //Environment.Exit(1);
            //}

	        #endregion

            #region new api connnection on 2016 Aug 05
            object o = null;
            string baseName = ((IntPtr.Size > 4) ? @"SOFTWARE\Wow6432Node\Finnigan\Xcalibur\Devices\" : @"SOFTWARE\Finnigan\Xcalibur\Devices\") + InstrumentName;
            
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(baseName))
            {
                if (key != null)
                {
                    string asmName = (string)key.GetValue(ApiFileNameDescriptor, null);
                    string typeName = (string)key.GetValue(ApiClassNameDescriptor, null);
                    if (!string.IsNullOrEmpty(asmName) && !string.IsNullOrEmpty(typeName))
                    {
                        Assembly asm = Assembly.LoadFrom(asmName);
                        o = asm.CreateInstance(typeName);
                    }
                }
            }
            
            #endregion

            _success = false;
            //object o;
            Container = o as IInstrumentAccessContainer;


            if (Container == null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Cannot cast the gotten API to the desired type.");
                sb.AppendLine("Desired type: " + typeof(IInstrumentAccessContainer).FullName);
                sb.AppendLine("Supported types:");
                sb.AppendLine(o.GetType().FullName);

                foreach (Type supported in o.GetType().GetInterfaces())
                {
                    sb.AppendLine(supported.FullName);
                }
                sb.AppendLine("Did you update the locally used interface library?");
                Console.Write(sb.ToString());

                throw new Exception(sb.ToString());
            }

            IInstrumentAccess instrument = Container.Get(1);
            if (instrument == null)
            {
                Console.WriteLine("Cannot access the first instrument");
                throw new Exception("Cannot access the first instrument");
            }

            Access = instrument as IExactiveInstrumentAccess;

            if (Access == null)
            {
                Console.WriteLine("The first instrument is not of the Exactive family");
                throw new Exception("The first instrument is not of the Exactive family");
            }

            try // does not work if off-line
            {
                ScanManager = instrument.Control.GetScans(true); // exclusively -- really need to try
            }
            catch (Exception e)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(e.Message);
                sb.AppendLine("Can't get IScans from instrumet control. it may due to no instrument is connected");
                Console.Write(sb.ToString());
                throw new Exception(sb.ToString());

            }

            try
            {
                ScanContainer = Access.GetMsScanContainer(0);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\nCan't get scan container");
                throw new Exception(e.Message + "\nCan't get scan container");
            }

            _success = true;

	        
	    }

	    public bool CreateSuccfully
	    {
	        get { return _success; }
	    }

	    /// <summary>
		/// The final destructor releases allocated system resources.
		/// </summary>
		~InstrumentInstance()
		{
			// Let the GC dispose managed members itself.
			Dispose(false);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposeEvenManagedStuff">true to dispose managed and unmanaged resources; false to dispose unmanaged resources</param>
		protected void Dispose(bool disposeEvenManagedStuff)
		{
		    try
		    {
		        // prevent double disposing
		        if (Interlocked.Exchange(ref m_disposed, 1) != 0)
		        {
		            return;
		        }

		        if (disposeEvenManagedStuff)
		        {
		            if (Access != null)
		            {
		                Access.Dispose();
		                Access = null;
		            }
		        }


		        IInstrumentAccessContainer container = Container;
		        Container = null;
		        if (container != null)
		        {
		            IDisposable disp = container as IDisposable;
		            if (disp != null)
		            {
		                disp.Dispose();
		            }
		        }

		        if (ScanManager != null)
		        {
		            ScanManager.Dispose();
		            ScanManager = null;
		        }
		    }
		    catch (Exception e)
		    {
		        Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                
		    }

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		virtual public void Dispose()
		{
			// Dispose managed and unmanaged resources and tell GC we don't need the destructor getting called.
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Get access to the flag whether this object is disposed.
		/// </summary>
		internal bool Disposed { get { return m_disposed != 0; } }

		/// <summary>
		/// Access to the particular instrument instance.
		/// </summary>
		public IExactiveInstrumentAccess Access { get; set; }

	    /// <summary>
	    /// Access to the control interface of the instrument.
	    /// </summary>
	    public Thermo.Interfaces.ExactiveAccess_V2.Control.IExactiveControl Control
	    {
	        get { return Access.Control as Thermo.Interfaces.ExactiveAccess_V2.Control.IExactiveControl ; }
	    }

		/// <summary>
		/// Access to the acquisition interface of the instrument.
		/// </summary>
        public IExactiveAcquisition Acquisition { get { return Control.Acquisition; } }

		/// <summary>
		/// Access to the instrument container which is nice for time access.
		/// </summary>
		private IInstrumentAccessContainer Container { get; set; }

	    public IScans ScanManager { get; private set; }
	    //public ICustomScan NewCustomScan { get { return ScanManager.CreateCustomScan(); } }
	    public IMsScanContainer ScanContainer { get; private set; }
        //public IMethodEditor MethodEditor { get; private set; }
        //public IMethods Methods { get; private set; }


	    [DllImport("kernel32.dll")]
		extern static private short QueryPerformanceCounter(ref long x);

		[DllImport("kernel32.dll")]
		extern static private short QueryPerformanceFrequency(ref long x);

		/// <summary>
		/// Return the current time in GMT with top-most accuracy.
		/// </summary>
		static internal DateTime Now
		{
			get
			{
				if (m_frequency == 0D)
				{
					long frequency = 0;
					if (QueryPerformanceFrequency(ref frequency) != 0)
					{
						// Let the function be compiled and ready:
						QueryPerformanceCounter(ref m_startCounter);
						m_startTime = DateTime.UtcNow;
						// Try to avoid a preemption by forcing a preemption
						Thread.Sleep(0);
						if (QueryPerformanceCounter(ref m_startCounter) != 0)
						{
							m_startTime = DateTime.Now;
							m_frequency = frequency;
						}
						else
						{
							return DateTime.Now;
						}
					}
					else
					{
						return DateTime.Now;
					}
				}

				long counter = 0;
				QueryPerformanceCounter(ref counter);
				double seconds = (counter - m_startCounter) / m_frequency;
				return m_startTime.AddSeconds(seconds);
			}
		}

		/// <summary>
		/// Access to the program arguments.
		/// </summary>
		//internal Arguments Arguments { get; private set; }

		/// <summary>
		/// Cycle through the modes On/Off/Standby if we are already in one of the modes.
		/// We leave in On mode if we are already in one of the three modes.
		/// </summary>
		/// <param name="end">timeout</param>
		/// <returns>true if we are not in On/Off/Standby initially or if we reached On mode after the cycle.</returns>
		private bool CycleModes(DateTime end)
		{
			if (!Acquisition.WaitFor(TimeSpan.Zero, SystemMode.Off, SystemMode.Standby, SystemMode.On))
			{
				return true;
			}

            
			// Test every possible switch!
			KeyValuePair<IMode, SystemMode>[] switches = {
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateStandbyMode(), SystemMode.Standby),
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateOnMode(), SystemMode.On),
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateStandbyMode(), SystemMode.Standby),
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateOffMode(), SystemMode.Off),
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateOnMode(), SystemMode.On),
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateOffMode(), SystemMode.Off),
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateStandbyMode(), SystemMode.Standby),
															new KeyValuePair<IMode, SystemMode>(Acquisition.CreateOnMode(), SystemMode.On),
														 };
  
			return true;
		}

		/// <summary>
		/// This method ensures the system runs, either by a user-provided method/acquisition or by an
		/// already running acquisition.
		/// </summary>
		/// <param name="started">time stamp when the machine has started</param>
		/// <returns>true if the system is running, false if the instrument is disconnected</returns>
		private bool EnsureRunningSystem(DateTime started)
		{
			
            DateTime end = started + TimeSpan.FromMilliseconds(OperationTime);

			// wait until the instrument is connected
			if (Acquisition.WaitFor(TimeSpan.Zero, SystemMode.Disconnected, SystemMode.Maintenance, SystemMode.Malconfigured))
			{
				Console.WriteLine("Waiting for a connection...");

				if (!Acquisition.WaitForOtherThan(end - Now, SystemMode.Disconnected, SystemMode.Maintenance, SystemMode.Malconfigured))
				{
					return false;
				}

				// let the instrument settle for a maximum of 45 seconds after a reconnect.
				int settleTime = Math.Max(45, (int) (end - Now).TotalSeconds);
				Acquisition.WaitFor(TimeSpan.FromSeconds(settleTime), SystemMode.On, SystemMode.Off, SystemMode.Standby);
			}

			if (!CycleModes(end))
			{
				return false;
			}

			Console.WriteLine("Mode=" + Acquisition.State.SystemMode);
			Console.WriteLine("State=" + Acquisition.State.SystemState);
			if (Acquisition.WaitForOtherThan(TimeSpan.Zero, SystemMode.Disconnected, SystemMode.Maintenance, SystemMode.Malconfigured, SystemMode.Off, SystemMode.Standby))
			{
				return true;
			}
			Console.WriteLine("Waiting for a system mode where data gets processed...");
			return Acquisition.WaitForOtherThan(end - Now, SystemMode.Disconnected, SystemMode.Maintenance, SystemMode.Malconfigured, SystemMode.Off, SystemMode.Standby);
		}

		/// <summary>
		/// Start a method if one if given in the arguments to the program or perform another method-like acquisition.
		/// For this, the instrument needs to be On.
		/// </summary>
		/// <param name="started">time stamp when the machine has started</param>
		/// <returns>true if the system is running somehow, false if the instrument is disconnected</returns>
		private bool StartMethodIfSystemIsIdle(DateTime started)
		{
			DateTime end = started + TimeSpan.FromMilliseconds(OperationTime);

			if (Acquisition.WaitForOtherThan(TimeSpan.Zero, SystemMode.On))
			{
				return true;
			}

			// the instrument is on, test if we have to start an acquisition.
			IAcquisitionWorkflow methodWorkflow = null;


			// start the method
			if (methodWorkflow != null)
			{
				// stop in standby mode to illustrate common value settings like starttrigger, RAW file, etc:
				methodWorkflow.Continuation = AcquisitionContinuation.Standby;
				ChangeResult result = Control.Acquisition.StartAcquisition(methodWorkflow);
				switch (result)
				{
					case ChangeResult.ForeignControl:
						// taken under foreign control in between which is OK

					case ChangeResult.IllegalOperationState:
						// taken under foreign control in between which is OK

					case ChangeResult.InstrumentDisconnected:
						// The instrument disconnected in between, which is OK

					default:
						// Assume a harmless, unknown problem
						return true;

					case ChangeResult.UnknownRequestType:
						Console.WriteLine("The interface rejected the request to start an acquisition because of an unknown request type.");
						// let this stop to show the error prominently.
						return false;
						
					case ChangeResult.IllegalValues:
						Console.WriteLine("The interface rejected the request to start an acquisition because of invalid values.");
						// let this stop to show the error prominently.
						return false;
						
					case ChangeResult.Submitted:
						return true;
				}
			}

			// no acquisition by user selected
			return true;
		}

		/// <summary>
		/// Let the selected operations happen.
		/// </summary>
		internal void Go()
		{
            Console.WriteLine("InstrumentInstance ID:   " + Access.InstrumentId);
            Console.WriteLine("InstrumentInstance Name: " + Access.InstrumentName);
            Console.WriteLine("Connected:       " + Access.Connected);

			try
			{
				DateTime started = Now;
                m_wait = new ManualResetEvent(Access.Connected);
				Acquisition.StateChanged += new EventHandler<StateChangedEventArgs>(Acquisition_StateChanged);

			}
			finally
			{
                //Access.ErrorsArrived -= new EventHandler<ErrorsArrivedEventArgs>(Instrument_ErrorsArrived);
                //Access.UserRolesChanged -= new EventHandler(UserRolesChanged);
                //Access.ConnectionChanged -= new EventHandler(ConnectionChanged);
			}
		}

		/// <summary>
		/// When the instrument changes its state, the new state is dumped here.
		/// </summary>
		/// <param name="sender">doesn't matter</param>
		/// <param name="e">contains the state that gets dumped</param>
		private void Acquisition_StateChanged(object sender, StateChangedEventArgs e)
		{
			IState state = e.State;
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("STATE: " + state.Description);
			sb.AppendLine("       " + state.SystemState);
			sb.AppendLine("Mode:  " + state.SystemMode);
			if (state.ElapsedRuntime.HasValue)
			{
				sb.AppendLine("Done:  " + state.ElapsedRuntime.Value.ToString("F2") + "%");
			}
			Console.Write(sb.ToString());
		}

		/// <summary>
		/// When the connection state changes we emit a message about that.
		/// </summary>
		/// <param name="sender">doesn't matter</param>
		/// <param name="e">doesn't matter</param>
		private void ConnectionChanged(object sender, EventArgs e)
		{
			ManualResetEvent wait = m_wait;
            if (Access.Connected && (wait != null))
			{
				wait.Set();
			}
            Console.WriteLine("The connection to the instrument " + ((Access.Connected) ? "has been established" : "dropped"));
		}



		
	}
}

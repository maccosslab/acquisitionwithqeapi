﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AcquisitionMethod
{
    public class IsoloationWindowCalculator
    {
        // this is simplfied version of what we have in Skyline.
        private const double OPTIMIZED_WINDOW_WIDTH_MULTIPLE = 1.00045475d;
        private const double OPTIMIZED_WINDOW_OFFSET = 0.25d;

        /// <summary>
        /// Get a list of optimized isolation window placement in given m/z range.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="endMZ"></param>
        /// <param name="windowWidth"></param>
        /// <returns>returen a list of m/z placements</returns>
        public static List<double> mzBundaries(double start, double endMZ, double windowWidth)
        {
            
            windowWidth = Math.Ceiling(windowWidth) * OPTIMIZED_WINDOW_WIDTH_MULTIPLE;
            start = Math.Ceiling(start/OPTIMIZED_WINDOW_WIDTH_MULTIPLE)*OPTIMIZED_WINDOW_WIDTH_MULTIPLE + OPTIMIZED_WINDOW_OFFSET;
            
            List<double> bundaries = new List<double>();

            while (start < endMZ)
            {
                bundaries.Add(start);
                start += windowWidth;
            }
            bundaries.Add(start);

            return bundaries;
        }
        
   
    }
}

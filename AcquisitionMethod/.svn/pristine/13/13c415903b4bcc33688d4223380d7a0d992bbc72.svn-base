﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;

// ReSharper disable RedundantUsingDirective
using mHardklor_2;
// ReSharper restore RedundantUsingDirective


namespace AcquisitionMethod
{

    /// <summary>
    /// we analyzed every MS1 scan we acquired, used three or five latest MS1 scans to update the candidate list.
    /// 
    /// </summary>
    public class BinVoting : IAcuqisition
    {
        private ScanAnalyzer _hardklor = new ScanAnalyzer();
        private Queue<Bin> _currentBins = new Queue<Bin>();
        private StreamWriter _log = null;

        private double _lowerMz = 0d;
        private int _noMoreThanThisNumber = 40; // ~2.x second on QE+
        private int _placeAMSoneWhenItCountsToThis = 20; //20 ~1 second on QE+
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _nextAnalysisID = 1000;
        private int _placeAMSoneIfCandidatesLessThanThis = 10;

        bool _readyForNextMS1 = false;
        int _MS2Count = 0;
        
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        private Queue<int> _AccessIDs = new Queue<int>();
        //private bool[] _commonBins = null;

        public BinVoting()
        {
        }

        public BinVoting(string rawfile) : this()
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.3 BinVoting method.");
            _log.WriteLine("# ");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            _hardklor.SetUpLogFilename(filepath);
        }

        // how many previous scans you want keep record

        private int _recordNo = 2;
        // record previous scans, 
        // key is the index of previuos scans
        // value is the list of index of bins

        private Queue<BinRecord> _binRecords = new Queue<BinRecord>();
        

        //private Dictionary<int, List<int>> _binRecords = new Dictionary<int, List<int>>();


        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);

            double bundary = 0d;
            foreach (ICentroid peak in scan.Centroids)
            {
                spectrum.Add(peak.Mz, (float)peak.Intensity);
            }

            if (_log != null)
                _log.WriteLine("S\t" + number);

            //DoAnalysis(spectrum);
            BinsResult binIdx = _hardklor.Analyze(spectrum);
            AssignBins(binIdx);

            if (_log != null)
                _log.Flush();
        }

        /// <summary>
        /// this is for testing 
        /// </summary>
        /// <param name="spectrum"></param>
        public void TestAlgorithm(SpectrumWrapper spectrum)
        {
            BinsResult binIdx = _hardklor.Analyze(spectrum);
            AssignBins(binIdx);
        }

        
        private void AssignBins(BinsResult binIdx)
        {
            // update bin recordes
            List<int> indexList = binIdx.QualifiedBins().Keys.ToList();
            BinRecord binRec = new BinRecord(indexList);  // the list is sorted from low mz to high mz
            if (_binRecords.Count >= _recordNo)
                _binRecords.Dequeue();
            _binRecords.Enqueue(binRec);

            //
            List<int> commonIdx = FindCommonBins();
            commonIdx.Sort();

            //
            int bincount = 0;
            Bin b = null;

            foreach (int idx in commonIdx)
            {
                double checkMZ = Parameter.GetInstance().mzBundaries[idx];

                // this is where we decide which bins we are going to analyze next.
                if (checkMZ < _lowerMz)
                    continue;

                b = new Bin();
                b.lowerBound = Parameter.GetInstance().mzBundaries[idx];
                b.upperBound = Parameter.GetInstance().mzBundaries[idx + 1];

                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());

                bincount++; // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }

            if (bincount < _noMoreThanThisNumber)
                _lowerMz = 0d;
            else
                _lowerMz = b.upperBound; // the bin won't be null, otherwise it will not get into here

            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

            
        }

        // this probably will take many miniseconds.... hmmmm ... 
        private List<int> FindCommonBins()
        {
            List<BinRecord> binRecs = new List<BinRecord>();
            binRecs = _binRecords.ToList();

            Dictionary<int,int> bincounts = new Dictionary<int, int>();

            foreach (BinRecord binrec in binRecs)
            {
                foreach (int idx in binrec.BinIndex)
                {
                    if (!bincounts.ContainsKey(idx))
                        bincounts.Add(idx,0);
                    bincounts[idx]++;
                }
            }
            List<int> commonIdx = new List<int>();

            foreach (int idx in bincounts.Keys)
            {
                if (bincounts[idx] >= _recordNo)
                    commonIdx.Add(idx);
            }
            return commonIdx;
        }

        public int PeriodicScanRunningNumber
        {
            get { return _repeat_runningNumber; }
        }
        
        public Bin NextCandidate 
        {
            get
            {
                Bin nextone = null;
                // the _currentBins may be filled by another thread, 
                lock (lockDequeue)
                {
                    if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }
        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }

        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public NextCustomScan.ScanType NextScanType
        {
            get
            {
                // we need one more MS one if the candidates is less than 10 after analyzing
                if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis && _readyForNextMS1)
                {
                    //_needAMSoneNow = false;
                    ResetCustomCounting();
                    return NextCustomScan.ScanType.CustomMSone;
                }
                else if (CandidateCount == 0)
                {
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.None;
                }
                else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    _MS2Count = 0;
                    if (CandidateCount < _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                    //if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.PeriodicMSone;
                }
                else
                {
                    return NextCustomScan.ScanType.CustomMStwo;
                }
            }
        }


        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }

        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
            //if (_analyzed)
            //{
            //_analyzed = false;
            _nextAnalysisID = _custom_runningNumber + 1;
            //}
        }

        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber;
            }
            
        }


        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == _nextAnalysisID)
            {
                return true;
            }
            else
                return false;
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            _hardklor.CloseLogFile();
        }


    }

    class BinRecord
    {
        public readonly List<int> BinIndex = new List<int>();

        public BinRecord(List<int> indexList)
        {
            indexList.Sort();
            BinIndex = indexList;
        }
    }
}

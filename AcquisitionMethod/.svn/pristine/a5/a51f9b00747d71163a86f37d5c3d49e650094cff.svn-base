﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Scans;

namespace AcquisitionMethod
{
    public class DefineMSScans
    {
        
        
        //static MSScanParameter ms2parameters = Parameter.GetInstance().MStwoSetting;
        public static void SetUpMSTwo(ICustomScan scan,double upper, double lower, int charge)
        {
            MSScanParameter ms2parameters = Parameter.GetInstance().MStwoSetting;
            //scan.SingleProcessingDelay = 0.5d;
            // not sure if the variable is valid
            //, double mzupper, double mzlower
            //scan.Values["IsolationRangeHigh"] = mzupper.ToString(); //50.0-6000.0	Upper m/z edge of isolation range
            //scan.Values["IsolationRangeLow"] = mzlower.ToString();
            //MSScanParameter ms2parameters = Parameter.GetInstance().MStwoSetting;
            double target = (upper + lower)/2d;
            scan.SingleProcessingDelay = ms2parameters.SingleProcessingDelay;
            scan.Values["IsolationRangeLow"] = lower.ToString();
            scan.Values["IsolationRangeHigh"] = upper.ToString();  // 445 is something always in the environment
            scan.Values["FirstMass"] = Parameter.FirstMZ(target, charge);
            scan.Values["LastMass"] = Parameter.LastMZ(target, charge); //50.0-6000.0	First m/z, lower edge of displayed spectral range
            scan.Values["Resolution"] = ms2parameters.Resolution; // resolution
            scan.Values["MaxIT"] = ms2parameters.MaxIT; //0.03-3000.00	Injection time in milliseconds, maximum injection time for AGC operation
            scan.Values["Polarity"] = ms2parameters.Polarity; // 0,1 0=positive ions, 1=negative ions
            scan.Values["NCE"] = ms2parameters.NCE; // collision energy; 0 to 1000

            if (charge < 2)
                charge = 2;
            else if (charge > 5)
                charge = 5;
            scan.Values["NCE_NormCharge"] = charge.ToString();

            scan.Values["AGC_Target"] = ms2parameters.AGC_Target;
        }

        
        public static void SetUpMSOne(ICustomScan scan)
        {
            MSScanParameter ms1parameters = Parameter.GetInstance().MSoneSetting;
            // not sure if the variable is valid
            //MSScanParameter ms1parameters = Parameter.GetInstance().MSoneSetting;
            scan.SingleProcessingDelay = 0.5D;
            scan.Values["IsolationRangeLow"] = ms1parameters.FirstMass;
            scan.Values["IsolationRangeHigh"] = ms1parameters.LastMass;
            scan.Values["FirstMass"] = ms1parameters.FirstMass;
            scan.Values["LastMass"] = ms1parameters.LastMass;
            scan.Values["MaxIT"] = ms1parameters.MaxIT;
            scan.Values["Resolution"] = ms1parameters.Resolution;
            scan.Values["Polarity"] = ms1parameters.Polarity; 
            scan.Values["AGC_Target"] = ms1parameters.AGC_Target;
            scan.Values["NCE"] = ms1parameters.NCE; // make sure it will be MS1

            //scan.Values["NCE"] = "25"; 
            // defualt is 0; it could be 0-1000, Setting this to x>0 will split all HCD injections into 3 parts,
            // one at energy reduced by x%%, one at original energy, one at energy increased by x%%.
            //scan.Values["NCE_SteppedEnergy"] = ""; // don't know
            //scan.Values["NCE_Factors"] = "";
            //scan.Values["AGC_Mode"] = "";
            //scan.Values["AGC_PeriodicPS"] = "";
            //scan.Values["CTCD_Support"] = "1"; 
            //scan.Values["SLensRFLevel"] = ""; 
        }

        public static void SetUpMSOne(ICustomScan scan,string firstMass,string lastMass)
        {
            SetUpMSOne(scan);
            scan.Values["IsolationRangeLow"] = firstMass;
            scan.Values["IsolationRangeHigh"] = lastMass;
            scan.Values["FirstMass"] = firstMass;
            scan.Values["LastMass"] = lastMass;
        }

        public static void SetUpRepeatScan(IRepeatingScan rscan)
        {
            //rscan.Values.Add("FirstMass", "199"); // this is the m/z range of resulting spectrum
            //rscan.Values.Add("LastMass", "1999");
            rscan.Values["FirstMass"] = "200.0";
            rscan.Values["LastMass"] = "1400.0";
            rscan.Values["IsolationRangeLow"] = "200";
            rscan.Values["IsolationRangeHigh"] = "1400";
            rscan.Values["MaxIT"] = "100";
            rscan.Values["Resolution"] = "35000";
            rscan.Values["Polarity"] = "0";
            rscan.Values["AGC_Target"] = "100000";
            rscan.Values["NCE"] = "0"; // this parameter decide if the scan is MS/MS or full MS *****
        }

        public static void SetUpRepeatMS2Scan(IRepeatingScan rscan)
        {
            rscan.Values.Add("FirstMass", "399"); // this is the m/z range of resulting spectrum
            rscan.Values.Add("LastMass", "1399");
            rscan.Values["IsolationRangeHigh"] = "333.8"; //50.0-6000.0	Upper m/z edge of isolation range
            rscan.Values["IsolationRangeLow"] = "334.2";
            rscan.Values["MaxIT"] = "50";
            rscan.Values["Resolution"] = "17500";
            rscan.Values["Polarity"] = "0";
            rscan.Values["AGC_Target"] = "10000";
            rscan.Values["NCE"] = "25"; // this parameter decide if the scan is MS/MS or full MS *****
        }


        public static void TestSetUpMs2(ICustomScan scan)
        {
            scan.SingleProcessingDelay = 0.5D;

            // not sure if the variable is valid
            //scan.Values["MSOrder"] = "MS2";
            
            // what if 
            scan.Values["IsolationRangeHigh"] = "800.1"; //50.0-6000.0	Upper m/z edge of isolation range
            scan.Values["IsolationRangeLow"] = "790.1";

            scan.Values["FirstMass"] = Parameter.GetInstance().FirstMass;
            scan.Values["LastMass"] = Parameter.GetInstance().LastMass; ; //50.0-6000.0	First m/z, lower edge of displayed spectral range
            scan.Values["MaxIT"] = "50"; //0.03-3000.00	Injection time in milliseconds, maximum injection time for AGC operation
            scan.Values["Resolution"] = "17500"; // resolution
            scan.Values["Polarity"] = "0"; // 0,1 0=positive ions, 1=negative ions
            scan.Values["NCE"] = "25"; // collision energy; 0 to 1000
            

            // defualt is 0; it could be 0-1000, Setting this to x>0 will split all HCD injections into 3 parts,
            // one at energy reduced by x%%, one at original energy, one at energy increased by x%%.
            //scan.Values["NCE_SteppedEnergy"] = ""; // don't know
            //scan.Values["NCE_Factors"] = "";

            scan.Values["AGC_Target"] = "10000";
        }

        public static void TestMultiplexedMS2(ICustomScan scan)
        {

            //scan.SingleProcessingDelay = ms2parameters.SingleProcessingDelay;
            //scan.Values["IsolationRangeLow"] = lower.ToString();
            //scan.Values["IsolationRangeHigh"] = upper.ToString();  // 445 is something always in the environment
            //scan.Values["FirstMass"] = ms2parameters.FirstMass;
            //scan.Values["LastMass"] = ms2parameters.LastMass; //50.0-6000.0	First m/z, lower edge of displayed spectral range
            //scan.Values["Resolution"] = ms2parameters.Resolution; // resolution
            //scan.Values["MaxIT"] = ms2parameters.MaxIT; //0.03-3000.00	Injection time in milliseconds, maximum injection time for AGC operation
            //scan.Values["Polarity"] = ms2parameters.Polarity; // 0,1 0=positive ions, 1=negative ions
            //scan.Values["NCE"] = ms2parameters.NCE; // collision energy; 0 to 1000
            //scan.Values["AGC_Target"] = ms2parameters.AGC_Target;

            //MsxInjectRanges
            //MsxInjectTargets - the values will be all the same
            //MsxInjectMaxITs - the values will be all the same 
            //MsxInjectNCEs
        }

// here are the all values from IScan PossibleParameters
// 
//Manual Trigger			Provides event after execution of single custom scan
//FirstMass	120	50.0-6000.0	First m/z, lower edge of displayed spectral range
//LastMass	2000	50.0-6000.0	Last m/z, upper edge of displayed spectral range

//IsolationRangeLow	120	50.0-6000.0	Lower m/z edge of isolation range
//IsolationRangeHigh	2000	50.0-6000.0	Upper m/z edge of isolation range


//MsxInjectRanges	[]	string	MSX m/z-ranges for isolation steps. Format - example:  [ (191.5,194.5), (523.3,525.3), (1117.0,1127.0) ] . Leave empty to use single value. 
//MsxInjectTargets	[]	string	MSX AGC targets for injection steps. Format - example: [20000, 50000, 35000]   
//MsxInjectMaxITs	[]	string	MSX (max) inject time in milliseconds for isolation steps. Format - example:  [80.0, 8.5, 120.0]
//MsxInjectNCEs	[]	string	MSX normalized collision energy for isolation steps. Format - example:  [30.0, 55.5, 17.8]

//MaxIT	10	0.03-3000.00	Injection time in milliseconds, maximum injection time for AGC operation.
//Resolution	35000	8000.0-150000.0	Resolution setting, determines transient data acquisition duration per scan
//Polarity	0	0,1	Ion polarity setting, 0=positive ions, 1=negative ions

//NCE	0	-1000	HCD Collision Energy in normalized units.
// ** NCE decide the resulting scan will be full MS or MS/MS

//NCE_NormCharge	2	5-Jan ( I guess this was 1-5)
//NCE_SteppedEnergy	0	0-1000	Setting this to x>0 will split all HCD injections into 3 parts, one at energy reduced by x%%, one at original energy, one at energy increased by x%%.
//NCE_Factors		string	Factors for stepped CE for a more flexible definition. Example:  [0.9, 1.0, 1.3, 1.8] 
//HCDTrappingTime	3	0.1-3000.0	
//DirectCE	0	-1000	HCD collision energy direct eV setting
//SourceCID	0	-1000	In-source fragmentation energy (eV)
//Microscans	1	Jan-00	Number of microscans per logicel scan.
//AGC_Target	1000000	1-10000000	Target number of trapped ions for automatic control (AGC)
//AGC_Mode	1	0,1	1=Enable automatic injection control for number of trapped ions (AGC), 0=Use fixed injection time
//AGC_PeriodicPS	0.66	0.0-5.0	Setting this !=0 inserts wide-mass-range-AGC prescans at given period [sec] (will be active only if prescans are required by analytical scans). The period is calculated automatically when this is set to -1.
//CTCD_Support	1	0,1	1=Enable/0=disable C-trap charge detector periodic operation (for AGC improvement)
//SLensRFLevel	50	0.0-100.0	S-Lens RF level parameter

    }
}

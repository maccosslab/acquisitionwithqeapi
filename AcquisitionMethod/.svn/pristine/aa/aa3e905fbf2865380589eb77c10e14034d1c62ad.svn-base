﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using mHardklor_2;

namespace AcquisitionMethod
{

    public class mDDAv2 : IAcuqisition
    {
        private ScanAnalyzer _hardklor = new ScanAnalyzer();
        private Queue<Bin> _currentBins = new Queue<Bin>();
        private bool _readyForNextMS1 = false;
        private int _MS2Count = 0;
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _placeAMSoneWhenItCountsToThis = 20;

        private int _topN = 20;
        private StreamWriter _log = null;
        private int _lifecycle = 10000; // 10,000 milliseconds = 10 seconds


        private StreamWriter _exclution = null;
        
        private List<PepIsotopes> _excludedPepIons = new List<PepIsotopes>();
        private StreamWriter _peakNumlog = null;
        private Stopwatch _watch = new Stopwatch();

        public mDDAv2(string rawfile)
        {
            _hardklor.UseMonoMz = false;
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _exclution = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile +".ex");
            _peakNumlog = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".np");
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDA v0.2");
            _log.WriteLine("# Because there are some overhead when doing Hardklor and sorting, so I try to avoid that in this version");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#T	Running Time");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");

            _peakNumlog.WriteLine("# Peak Number log");
            _peakNumlog.WriteLine("#S Scan AccessID");
            _peakNumlog.WriteLine("#P #Peak In Scan\t# Excluded Peak");
            _peakNumlog.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            _hardklor.SetUpLogFilename(filepath);
        }


        public void DoAnalysis(IMsScan scan)
        {
            if (!_watch.IsRunning)
                _watch.Start();

            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);

            SpectrumWrapper spectrum = new SpectrumWrapper();
            foreach (ICentroid peak in scan.Centroids)
            {
                spectrum.Add(peak.Mz, (float) peak.Intensity);
            }
            // hardklor log
            string time = TimeString(_watch.Elapsed);
            
            if (_log != null)
                _log.WriteLine("S\t" + number+"\nT\t"+time);
            
            if (_exclution != null)
                _exclution.WriteLine("S\t" + number + "\nT\t" + time);
            
            if (_peakNumlog != null)
                _peakNumlog.WriteLine("S\t"+ number);


            // it's doing analysis now, I will get candidate list soon
            _readyForNextMS1 = false;

            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            List<Bin> candidates = GetTopNCandidates(peptides);
            _log.WriteLine("N\t" + candidates.Count.ToString());
            
            // put all candidates in the queue
            lock (_currentBins)
            {
                foreach (Bin bin in candidates)
                {
                    _log.WriteLine("B\t" + bin.lowerBound.ToString() + "\t" + bin.upperBound.ToString());
                    _currentBins.Enqueue(bin);
                }
            }
            _readyForNextMS1 = true;
            if (_log != null)
                _log.Flush();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pepions"></param>
        /// <returns>from higher intensity to lower intensity</returns>
        private List<Bin> GetTopNCandidates(List<PeptideMatch> matches)
        {
            List<double> mzs = GetExclutedMZs();
            mzs.Sort();
            int count = 0;

            // write exclution list log
            if (_exclution != null)
            {
                foreach (double mz in mzs)
                {
                    count++;
                    int res = count%5;
                    if (res == 1)
                        _exclution.Write("E\t" + mz);
                    else if (count%5 == 0)
                        _exclution.WriteLine("; " + mz);
                    else
                        _exclution.Write("; " + mz);
                }
            }
            

            // only consider multiple charged isotope evenlopes
            List<PeptideMatch> pepions = new List<PeptideMatch>();
            foreach (PeptideMatch isotope in matches)
            {
                if (isotope.charge >= Parameter.GetInstance().minCharge && isotope.charge <= Parameter.GetInstance().maxCharge)
                    pepions.Add(isotope);
            }

            if (_peakNumlog != null)
            {
                int no = mzs.Count/5;
                _peakNumlog.WriteLine("#P\t"+pepions.Count.ToString()+"\t"+no.ToString());
            }

            // analyze and get the top N peak.
            pepions.Sort(new PepMatchIntensityComparer());
            List<Bin> candidates = new List<Bin>();
            
            double offset = Parameter.GetInstance().windowWidth/2;
            for (int i = pepions.Count - 1; i >= 0; i --) // from high intensity to low intensity
            {
                bool added = false;
                double tolerance = Function.PPMToDa(pepions[i].baseMZ, Parameter.GetInstance().FragmentTolerance);
                for (int idx = 0; idx < mzs.Count; idx++)
                {
                    double dif = mzs[idx] - pepions[i].baseMZ;
                    dif = Math.Round(dif);
                    if (dif > tolerance)
                    {
                        if (mzs[idx] > pepions[i].baseMZ)
                            break;
                        else
                            continue;
                    }
                    else
                    {
                        added = true;
                        break;
                    }
                }
                if (!added)
                {
                    Bin b = new Bin();
                    b.upperBound = pepions[i].baseMZ + offset;
                    b.lowerBound = pepions[i].baseMZ - offset;
                    candidates.Add(b);
                    // put into exclusion list
                    PutIntoExclusionList(pepions[i]);
                }
                // only select N ions 
                if (candidates.Count >= _topN)
                    break;
            }
            return candidates;
        }

        private List<double> GetExclutedMZs()
        {
            List<double> mzpoisitions = new List<double>();
            lock (_excludedPepIons)
            {
                DateTime now = DateTime.Now;
                for (int i = _excludedPepIons.Count-1; i >= 0; i--)
                {
                    if (_excludedPepIons[i].Deadline < now)
                        _excludedPepIons.RemoveAt(i);
                    else
                    {
                        foreach (double mz in _excludedPepIons[i].IsotopeMZs)
                            mzpoisitions.Add(mz);
                    }
                }
            }
            return mzpoisitions;
        }

        private void PutIntoExclusionList(PeptideMatch pepion)
        {
            PepIsotopes pepIsotope = new PepIsotopes(pepion, _lifecycle);
            lock (_excludedPepIons)
            {
                pepIsotope.StartCount();
                _excludedPepIons.Add(pepIsotope);
            }
        }


        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            return false;
        }

        public int TopN
        {
            get { return _topN; }
            set { _topN = value; }
        }


        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid >= 1000)
                return true;
            else 
                return false;
        }

        public  Bin NextCandidate 
        {
            get
            {
                Bin b = null;
                lock (_currentBins)
                {
                    if (_currentBins.Count > 0)
                    {
                        b = _currentBins.Dequeue();
                        _MS2Count ++;
                    }
                }
                return b;
            }
        }



        // check here on Friday*********************
        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public NextCustomScan.ScanType NextScanType
        {
            get
            {
                if (CandidateCount <= _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                {
                    ResetCustomCounting();
                    return NextCustomScan.ScanType.CustomMSone;
                }
                if (_MS2Count >= _topN || CandidateCount == 0)
                {
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    else
                        return NextCustomScan.ScanType.None;
                }
                else if (CandidateCount > 0)
                {
                    return NextCustomScan.ScanType.CustomMStwo;
                }
                else
                    return NextCustomScan.ScanType.PeriodicMSone;
            }
        }

        private int CandidateCount 
        {
            get { return _currentBins.Count; }
        }

        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
        }
        public int NextCustomScanRunningNumber
        {
            get
            {
                // start from 1,000
                _custom_runningNumber++;
                return _custom_runningNumber;
            } 
            
        }
        public int PeriodicScanRunningNumber { get { return _repeat_runningNumber; } }
        public bool DoneAnalysis { get { return false; } }

        public void Close() 
        {
            if (_log != null)
                _log.Close();
            if (_exclution != null)
                _exclution.Close();
            if (_peakNumlog != null)
                _peakNumlog.Close();
            _hardklor.CloseLogFile();
        }

        private string TimeString(TimeSpan ts)
        {
            return String.Format("H:{0:00},Min:{1:00},Sec:{2:00},MS:{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            //string elapsedTime = String.Format("Sec:{0:00},MS:{1:00}", ts.Seconds, ts.Milliseconds);
        }
        

    }



   

}

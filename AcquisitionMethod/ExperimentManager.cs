﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class ExperimentManager
    {
        Dictionary<string,Parameter> _experimentSetting = new Dictionary<string, Parameter>();

        private static ExperimentManager _mamager;
        private string _filefolder = "";
        

        public static ExperimentManager GetInstance()
        {
            if (_mamager == null)
                _mamager = new ExperimentManager();
            return _mamager;
        }

        private ExperimentManager()
        {

        }

        public IEnumerable<string> Projects()
        {
            foreach (string name in _experimentSetting.Keys)
            {
                yield return name;
            }
        }

        /// <summary>
        /// the experiment name has to be unique. 
        /// The old parameter setting will be replaced if you add the same rawfilename twice.
        /// </summary>
        /// <param name="rawfilename"></param>
        //public void AddExperiment(string rawfilename)
        //{
        //    if (!_experimentSetting.ContainsKey(rawfilename))
        //        _experimentSetting.Add(rawfilename, new Parameter());
        //    else
        //        _experimentSetting[rawfilename] = new Parameter();
        //}

        private string _temprawfile = "";
        private string _tempset = "";
        private Parameter _tempParameter;

        public void AddTempExperiment(string rawfilename, string settingfile)
        {
            _temprawfile = rawfilename;
            _tempset = settingfile;
            _tempParameter = ParameterFileIO.GetParameterSetting(_tempset);
            
            //_currentProject = _temprawfile;

        }

        public string TempRawfilename 
        {
            get { return _temprawfile; }
        }
        public string TempParameterFilepath
        {
            get { return _tempset; }
        }
        public void UpdateTempParameterFilepath(string newpath)
        {
            _tempset = newpath;
        }

     


        public void AddExperiment(string rawfilename, Parameter parameter)
        {
            if (!_experimentSetting.ContainsKey(rawfilename))
            {
                _experimentSetting.Add(rawfilename,parameter);
            }
        }

        public Parameter GetParameter(string rawfilename)
        {
            if (_experimentSetting.ContainsKey(rawfilename))
                return _experimentSetting[rawfilename];
            else
            {
                if (rawfilename == _temprawfile)
                    return _tempParameter;
                else 
                    return null;
            }
        }

        public bool ContainExperimentSetting(string rawfilename)
        {
            return _experimentSetting.ContainsKey(rawfilename);
        }

        public string ExperimentSettubgSummary(string rawfilename)
        {
            Parameter para = GetParameter(rawfilename);
            return ExperimentSettubgSummary(para);
        }

        public string ExperimentSettubgSummary(Parameter parameter)
        {
            StringBuilder sb = new StringBuilder();
            if (parameter == null)
                sb.AppendLine("The parameter setting is null");
            else
            {
                
            }
            return sb.ToString();
        }

        
        public void Setfilefolder(string filefolder)
        {
            _filefolder = filefolder;
        }

        public string Filefolder
        {
            get { return _filefolder; }
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class ScanBase
    {
        // the ion stores the results from Hardklor
        public List<Ion> Ionlist = new List<Ion>();
        public int ScanNo;
        public double RT = 0d;
        public ScanBase(){}
        public ScanBase(int scanNo)
        {
            ScanNo = scanNo;
        }

        public void SortIonsBasedOnMz()
        {
            Ionlist.Sort(new IonMZComparer());
        }
    
    }
    
}

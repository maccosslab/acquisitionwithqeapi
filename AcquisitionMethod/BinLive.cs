﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class BinLive
    {
        public readonly int BinIdx = -1;
        private int _lifecycle = 0;
        private DateTime _deadline = DateTime.Now;

        public BinLive(int binIdx, int lifecycle)
        {
            BinIdx = binIdx;
            _lifecycle = lifecycle;
        }


        public DateTime Deadline
        {
            get { return _deadline; }
        }
        public void StartCount()
        {
            _deadline = DateTime.Now + new TimeSpan(0, 0, 0, 0, _lifecycle);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    internal class PersistPep
    {
        public int Charge = 0;
        //public double Max = 0d;
        //public double Min = 0d;
        List<Ion> ions = new List<Ion>();
        public float BaseIntensityInRange = 0f;

        public PersistPep(Ion ion)
        {
            Charge = ion.Charge;
            AddIon(ion);
        }

        public IEnumerable<Ion> PepIons()
        {
            return ions;
        }

        public Ion LastIon
        {
            get { return ions[ions.Count - 1]; }
        }

        public void AddIon(Ion ion)
        {
            ions.Add(ion);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class Bin
    {
        public double upperBound;
        public double lowerBound;
        
        public int targetCharge = 0; // default is 2
        public double targetIntensity = 0d;
        public Types.ScanType ScanType = Types.ScanType.None;
        public int parentScan = -999;

        // this is for ms2
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class MSScanParameter
    {
        public string IsolationLow = "400";
        public string IsolationHigh = "1400";
        public string FirstMass = "400";
        public string LastMass = "1400";
        public string Resolution = "70000";
        public string MaxIT = "30";
        public string Polarity = "0";
        public string NCE = "0";
        public string AGC_Target = "1000000";
        public string IsolationWindowWidth = "10";
        public double SingleProcessingDelay = 0.5d;
        public int Charge = 0;
        public Types.ScanOrder Order = Types.ScanOrder.MS;
    }
}

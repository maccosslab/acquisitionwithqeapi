﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AcquisitionMethod
{
    public class Function
    {

        public static double PPMToDa(double mz, double ppm)
        {
            double tolerance = 0d;
            tolerance = mz * ppm * 0.000001d;
            return tolerance;
        }
        public static string TimeString(TimeSpan ts)
        {
            return String.Format("H:{0:00},Min:{1:00},Sec:{2:00},MS:{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
        }

        public static List<Ion> ParseTargetList(string filename)
        {
            StreamReader sr = new StreamReader(filename);
            List<Ion> targetList = new List<Ion>();
            Dictionary<double,int> mzs = new Dictionary<double, int>();
            while (sr.Peek() != -1)
            {
                string oneline = sr.ReadLine();
                double mz = double.Parse(oneline);
                if (!mzs.ContainsKey(mz))
                {
                    mzs.Add(mz, 0);
                    Ion ion = new Ion();
                    ion.Mz = mz;
                    targetList.Add(ion);
                }
            }
            return targetList;
        }

        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;

namespace AcquisitionMethod
{
    public class Parameter
    {
        public delegate void IsolationWindowUpdate(List<double> newBundaries);
        /// <summary>
        /// when isolation window setting changes .....
        /// </summary>
        public event IsolationWindowUpdate IsolationWindowChanged;
        public double Threshold = 1d;
        public int minCharge = 2;
        public int maxCharge = 3;
        public double Speed = 10d; //(Hz) 
        
        private double _start = 400d;
        private double _end = 1400;
        private double _binsize = 2d;
        private List<double> _mzBundaries = null;
        private Dictionary<double, int> _bundaryToIdx = new Dictionary<double, int>();

        public double PepTolerance = 10d;
        public ToleranceUnit pepToleranceUnit = ToleranceUnit.ppm;

        public double FragmentTolerance = 20d;
        public ToleranceUnit fragToleranceUnit = ToleranceUnit.ppm;
        public float IntensityThreshold = 10000;
        public float FragmentIntThreshold = 10000;
        //_hardklorAnalysis = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"/"+filename);
        //_hardklorAnalysis.WriteLine("#S\tAccessID\tPeak Number\tBin Number");
        //_hardklorAnalysis.WriteLine("#B\tLower MZ\t Upper MZ");
        //_hardklorAnalysis.WriteLine("#----------------------------------------");
        //_hardklorAnalysis.Flush();

        //this might be a redundant parameter, check PepTolerance 
        public double TargetPepTolerance = 10d;
        public bool MatchAllRepoeters = false;
        public bool MatchChargeState = false;

        public string ParameterFileName = "";
        
        public Types.AcuqisitionMethodType AcquisitionMethod = Types.AcuqisitionMethodType.FeatureVotes;
        //public Types.AcuqisitionMethodType experimentType = Types.AcuqisitionMethodType.None;
        public enum ToleranceUnit
        {
            ppm,
            Da,
        }

        #region DDAp parameters 
        /// <summary>
        /// force to update candidate list if it hasn't updated for 20 MS2 scans.
        /// </summary>
        public int UpdateFrequency = 20;
        /// <summary>
        /// only condier bin candidate when there are peptide ions observed in N continuous MS1 scans.
        /// </summary>
        public int PeptideInNMSone = 3;
        public int PlaceAMSoneAfterNMS2 = 20;
        #endregion

        #region mDDA parameters
        public int TopN = 20;
        public int DynamicExclusion = 20;
        //public int DynamicExclusionPPM = 10;  // use PepTolerance instead
        public bool ExcludeIsotopes = true;
        #endregion


        #region DIADA parameters
        public int DIACharge = 2;
        public List<double> ReportIons = new List<double>();
        public int ReporterIonInNscans = 1;
        #endregion

        public bool UseMonoMZToDecideBins = true;

        //public float IntThreshold = 5;
        public float UnderFillRatio = 0.1f;

        // the default parameters are for QE+
        private  MSScanParameter _msOne;
        private  MSScanParameter _msTwo;
        private MSScanParameter _sDIA;
        private MSScanParameter _aDIA;
        private MSScanParameter _SIM;
        


        public MSScanParameter MSoneSetting
        {
            get
            {
                if (_msOne == null)
                {
                    _msOne = new MSScanParameter();
                    _msOne.Order = Types.ScanOrder.MS;
                    _msOne.LastMass = "800";
                    _msOne.FirstMass = "400";
                    _msOne.NCE = "0";
                    _msOne.Polarity = "0";
                    _msOne.MaxIT = "50";
                    _msOne.Resolution = "30000";
                    _msOne.AGC_Target = "3000000";
                    _msOne.IsolationWindowWidth = "1000";
                    
                    _msOne.SingleProcessingDelay = 0.5d;
                }
                return _msOne;
            }
        }

        public MSScanParameter SIMSetting
        {
            get
            {
                if (_SIM == null)
                {
                    _SIM = new MSScanParameter();
                    _SIM.Order = Types.ScanOrder.MS;
                    _SIM.LastMass = "800";
                    _SIM.FirstMass = "400";
                    _SIM.NCE = "0";
                    _SIM.Polarity = "0";
                    _SIM.MaxIT = "50";
                    _SIM.Resolution = "120000";
                    _SIM.AGC_Target = "3000000";
                    _SIM.IsolationWindowWidth = "1000";

                    _SIM.SingleProcessingDelay = 0.5d;
                }
                return _SIM;
            }
        }

        // DIAIA parameter is alos saved here
        public MSScanParameter MStwoSetting
        {
            get
            {
                if (_msTwo == null)
                {
                    _msTwo = new MSScanParameter();
                    _msTwo.Order = Types.ScanOrder.MS2;
                    _msTwo.NCE = "27";
                    _msTwo.Polarity = "0";
                    _msTwo.MaxIT = "25";
                    _msTwo.Resolution = "15000";
                    _msTwo.AGC_Target = "100000";
                    _msTwo.IsolationWindowWidth = "1.5";
                    _msTwo.SingleProcessingDelay = 0.5d;
                }
                return _msTwo;
            }
        }

        public MSScanParameter sDIASetting
        {
            get
            {
                if (_sDIA == null)
                {
                    _sDIA = new MSScanParameter();
                    _sDIA.Order = Types.ScanOrder.MS2;
                    _sDIA.NCE = "30";
                    _sDIA.Polarity = "0";
                    _sDIA.MaxIT = "25";
                    _sDIA.Resolution = "15000";
                    _sDIA.AGC_Target = "100000";
                    _sDIA.IsolationWindowWidth = "10";
                    _sDIA.SingleProcessingDelay = 1d;
                }
                return _sDIA;
            }
        }

        public MSScanParameter aDIASetting
        {
            get
            {
                if (_aDIA == null)
                {
                    _aDIA = new MSScanParameter();
                    _aDIA.Order = Types.ScanOrder.MS2;
                    _aDIA.NCE = "30";
                    _aDIA.Polarity = "0";
                    _aDIA.MaxIT = "25";
                    _aDIA.Resolution = "15000";
                    _aDIA.AGC_Target = "100000";
                    _aDIA.IsolationWindowWidth = "2";
                    _aDIA.SingleProcessingDelay = 1d;
                }
                return _aDIA;
            }
        }



        public Parameter()
        {
            //ReportIons.Add(159.0922d); //Tryptophan Immonium ion mass
            ReportIons.Add(120.0813d); // Phenylalanine Immonium ion mass
            //ReportIons.Add(308d);
            //ReportIons.Add(304d);
            //ReportIons.Add(203d);

        }

        public static string FirstMZ(double targetMz, int charge)
        {
            double first = 0d;
            if (charge == 2)
            {
                //y=0.136x+1.3022
                first = targetMz * 0.136d + 1.3022d;
            }
            else if (charge == 3)
            {
                //y=0.204x+1.3728
                first = targetMz * 0.201d + 1.3728;
            }
            else if (charge == 4)
            {
                //y = 0.272x+1.4213
                first = targetMz * 0.272 + 1.4213;
            }
            else
            {
                first = targetMz * 0.1 + 1.35; // I just give a random number
                if (first < 50)
                    first = 50;

            }

            return first.ToString();
        }

        public static string LastMZ(double targetMz, int charge)
        {
            double last = 0d;
            if (charge == 2)
            {
                //y=2.04x+19.532
                last = targetMz * 2.04 + 19.532;
            }
            else if (charge == 3)
            {
                //y=3.0599x+20.592
                last = targetMz * 3.0599d + 20.592;
            }
            else if (charge == 4)
            {
                //y=4.0804x+21.319
                last = targetMz * 4.0804 + 21.319;
            }
            else
            {
                last = targetMz * 4 + 20; // I just give a random number
                if (last > 6000)
                    last = 6000;

            }
            return last.ToString();
        }

        public double startMZ
        {
            get { return _start; }
            set
            {
                if (value != _start)
                {
                    _start = value;
                    ComputeNewBundaries();
                }
            }
        }

        public double endMZ
        {
            get { return _end; }
            set
            {
                if (value != _end)
                {
                    _end = value;
                    ComputeNewBundaries();
                }
            }
        }

        public double windowWidth
        {
            get { return _binsize; }
            set
            {
                if (value != _binsize)
                {
                    _binsize = value;
                    ComputeNewBundaries();
                }
            }
        }

        public List<double> mzBundaries
        {
            get
            {
                if (_mzBundaries == null)
                    ComputeNewBundaries();
                return _mzBundaries;
            }
        }

        public int mzBinIndex(double binlowerboundary)
        {
            return _bundaryToIdx[binlowerboundary];
        }


        private void ComputeNewBundaries()
        {
            _mzBundaries = IsoloationWindowCalculator.mzBundaries(startMZ, endMZ, windowWidth);

            _bundaryToIdx.Clear();
            for (int i = 0; i < _mzBundaries.Count; i++)
                _bundaryToIdx.Add(_mzBundaries[i],i);


            if (IsolationWindowChanged != null)
                IsolationWindowChanged(_mzBundaries);
        }


    }
}

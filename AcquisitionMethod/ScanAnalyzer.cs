﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

using mHardklor_2;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;

namespace AcquisitionMethod
{
    /// <summary>
    /// hardklor result wrapper
    /// </summary>
    public class ScanAnalyzer
    {
        
        //private readonly 

        // scan; bin index; ions in one bin
        //Dictionary<ScanBase, Dictionary<int, List<Ion>>> _scanToBinResult = new Dictionary<ScanBase, Dictionary<int, List<Ion>>>();
        
        //private void AssignBins(ScanBase scan, List<double> mzBundaries)

        private readonly HardklorWrapper hardklor;

        private TextWriter _log = null;
        private bool _useMonoMZ = true;
        private readonly int _minCharge ;
        private readonly int _maxCharge;
        private readonly List<double> _mzBundaries; 
        private readonly double _threshold;
        private readonly double _resolution;

        public ScanAnalyzer(List<double> mzBundaries,int minCharge,int maxCharge,double threshold,double resolution)
        {
            _mzBundaries = mzBundaries;
            _minCharge = minCharge;
            _maxCharge = maxCharge;
            _threshold = threshold;
            _resolution = resolution;
            // change hardklor parameter here.
            // it goes wrong here on the computer next to QE, which means it fails when we attempt to use mHardklor.dll

            ParameterWrapper hardklorParameter = new ParameterWrapper();
            hardklorParameter.resolution = resolution;
            hardklor = new HardklorWrapper(hardklorParameter);
            
        }
        public ScanAnalyzer(List<double> mzBundaries, int minCharge, int maxCharge,double resolution) : this(mzBundaries, minCharge, maxCharge, 0d, resolution) { }
        
        public ScanAnalyzer(List<double> mzBundaries, int minCharge, int maxCharge) : this(mzBundaries, minCharge, maxCharge, 0d, 84853d) { }


        public bool UseMonoMz
        {
            get { return _useMonoMZ; }
            set { _useMonoMZ = value; }
        }
        public void SetUpLogFilename(string filename)
        {
            _log = TextWriter.Synchronized(new StreamWriter(filename));
            AddMessage("# Hardklor real-time output.");
            AddMessage("# the format of document is very similar to result from standalone hardklor, but ");
            AddMessage("# this is in DIFFERENT format");
            AddMessage("# Rosultion: " + _resolution);
            AddMessage("#----------------------------------------");
            AddMessage("# S Access ID   Retention Time");
            AddMessage("# P Mass    Charge State    intensity of Base Peak  m/z of Base Peak    Corrlation  m/z of Monoisotopic Peak");
            AddMessage("#----------------------------------------");
        }

        private static readonly object _syncObject = new object();
        private void AddMessage(string message)
        {
            if (_log != null)
            {
                lock (_syncObject)
                {
                    _log.WriteLine(message);
                    _log.Flush();
                }
            }
        }
        public void CloseLogFile()
        {
            if (_log != null)
            {
                lock (_syncObject)
                {
                    _log.Close();
                }
            }
        }
        /// <summary>
        /// this method will call DetectPeptideIons and AssignBins
        /// </summary>
        /// <param name="spectrum"></param>
        /// <returns></returns>
        public BinsResult Analyze(SpectrumWrapper spectrum)
        {
            // do hardklor here
            List<Ion> peptideIons = DetectPeptideIons(spectrum);
            BinsResult result = AssignBins(peptideIons);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spectrum"></param>
        /// <returns>The result is sorted base on m/z value, from low to high</returns>
        public List<Ion> DetectPeptideIons(SpectrumWrapper spectrum)
        {
            List<PeptideMatch> matches = hardklor.AnalyzeSpectrum(spectrum);

            
            AddMessage("S\t" + spectrum.Index.ToString() + "\t" + spectrum.RetentionTime.ToString());

            ScanBase scanb = new ScanBase();
            
            foreach (PeptideMatch isotope in matches)
            {
                Ion ion = new Ion();
                ion.Charge = isotope.charge;
                ion.Intensity = isotope.intensity; // this will affect the result of DDABin, mDDA

                double mz = 0d;
                if (_useMonoMZ)
                    mz = (isotope.monoMass + Molecules.Proton * ion.Charge) / ion.Charge;
                else
                    mz = isotope.baseMZ;
                //double mz = isotope.baseMZ;
            
                ion.Mz = mz;
                scanb.Ionlist.Add(ion);
                
                AddMessage("P\t" + isotope.monoMass.ToString() + "\t" + isotope.charge.ToString() + "\t" + isotope.intensity + "\t" + isotope.baseMZ + "\t" + isotope.corr + "\t" + mz.ToString());
                    
            }
            // this is important
            scanb.SortIonsBasedOnMz();
            return scanb.Ionlist;

        }

        // this is the meathod called by DDA
        public List<PeptideMatch> DetectPepMatches(SpectrumWrapper spectrum)
        {
            List<PeptideMatch> matches = hardklor.AnalyzeSpectrum(spectrum);

            // log info
            AddMessage("S\t" + spectrum.Index.ToString() + "\t" + spectrum.RetentionTime.ToString());
            foreach (PeptideMatch isotope in matches)
            {
                double mz = (isotope.monoMass + Molecules.Proton * isotope.charge) / isotope.charge;
                AddMessage("P\t" + isotope.monoMass.ToString() + "\t" + isotope.charge.ToString() + "\t" + isotope.intensity + "\t" + isotope.baseMZ + "\t" + isotope.corr + "\t" + mz.ToString());
            }

            return matches;
        }

        public BinsResult AssignBins(List<PeptideMatch> pepIsotopes)
        {

            List<double> mzBundaries = null;
            int currentIdx = 0;



            Dictionary<Ion, PeptideMatch> mzToMatches = new Dictionary<Ion, PeptideMatch>();
            foreach (PeptideMatch isotope in pepIsotopes)
            {
                Ion ion = new Ion();
                ion.Charge = isotope.charge;

                if (_useMonoMZ)
                    ion.Mz = (isotope.monoMass + Molecules.Proton*isotope.charge)/isotope.charge; // this intensity will be assign later
                else
                {
                    ion.Mz = isotope.baseMZ;
                    ion.Intensity = isotope.intensity;
                }
                mzToMatches.Add(ion,isotope);
            }
            var sorted = mzToMatches.OrderBy(Ions => Ions.Key.Mz);
            var sortedDic = sorted.ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);


            Dictionary<int, List<Ion>> binIdxToIons = new Dictionary<int, List<Ion>>();
            try
            {
                foreach (Ion ion in sortedDic.Keys)
                {
                    int assignIdx = -1;

                    
                    for (int binidx = currentIdx; binidx < _mzBundaries.Count; binidx++)
                    {
                        if (ion.Mz < _mzBundaries[binidx])
                            continue;
                        else if (binidx + 1 >= _mzBundaries.Count) // out of mz ragne of interest
                        {
                            currentIdx = binidx + 1;
                            break;
                        }
                        else if (ion.Mz < _mzBundaries[binidx + 1])
                        {
                            assignIdx = binidx;
                            currentIdx = assignIdx;

                            if (_useMonoMZ)
                            {
                                PeptideMatch isotope = sortedDic[ion];
                                if (isotope.baseMZ < _mzBundaries[binidx + 1])
                                {
                                    ion.Intensity = isotope.intensity;
                                }
                                else // this is the case when the base peak locates outside of bin
                                {
                                    double maxInt = double.MinValue;
                                    double distance = (Molecules.Proton/ion.Charge);
                                    for (int i = 0; i < isotope.distribution.Count; i++)
                                    {
                                        double currentMz = ion.Mz + (distance*i);
                                        if (currentMz >= _mzBundaries[binidx + 1])
                                            break;
                                        double currentInt = isotope.intensity*(isotope.distribution[i]/100);
                                        if (currentInt > maxInt)
                                            maxInt = currentInt;
                                    }
                                    ion.Intensity = maxInt;
                                }
                            }
                            break;
                        }
                    }

                    //if (_useMonoMZ)
                    //    mz = (isotope.monoMass + Molecules.Proton * isotope.charge) / isotope.charge;
                    //else
                    //    mz = isotope.baseMZ;

                    if (assignIdx != -1)
                    {
                        if (!binIdxToIons.ContainsKey(assignIdx))
                            binIdxToIons.Add(assignIdx, new List<Ion>());
                        binIdxToIons[assignIdx].Add(ion);
                    }
                    else // out of mz ragne of interest
                    {
                        //Console.WriteLine("opps... just skipped some peaks");
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Wrong in bin assignment - " + e.Message);
            }
            BinsResult bins = new BinsResult(binIdxToIons, _minCharge, _maxCharge, _threshold);

            return bins;

        }

        /// <summary>
        /// Be aware that the input ion list has to be SORTED from LOW m/z to HIGH m/z
        /// </summary>
        /// <param name="ionlist">sort it, please</param>
        /// <returns></returns>
        private BinsResult AssignBins(List<Ion> ionlist)
        {
            List<double> mzBundaries = null;
            int currentIdx = 0;
            
            Dictionary<int, List<Ion>> binIdxToIons = new Dictionary<int, List<Ion>>();

            try
            {
                foreach (Ion ion in ionlist)
                {
                    int assignIdx = -1;

                    for (int binidx = currentIdx; binidx < _mzBundaries.Count; binidx++)
                    {
                        if (ion.Mz < _mzBundaries[binidx])
                            continue;
                        else if (binidx + 1 >= _mzBundaries.Count) // out of mz ragne of interest
                        {
                            currentIdx = binidx + 1;
                            break;
                        }
                        else if (ion.Mz < _mzBundaries[binidx + 1])
                        {
                            assignIdx = binidx;
                            currentIdx = assignIdx;
                            break;
                        }
                    }

                    if (assignIdx != -1)
                    {
                        if (!binIdxToIons.ContainsKey(assignIdx))
                            binIdxToIons.Add(assignIdx, new List<Ion>());
                        binIdxToIons[assignIdx].Add(ion);
                    }
                    else // out of mz ragne of interest
                    {
                        //Console.WriteLine("opps... just skipped some peaks");
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Wrong in bin assignment - " + e.Message);
            }
            BinsResult bins = new BinsResult(binIdxToIons,_minCharge,_maxCharge,_threshold);

            return bins;
        }
    }
}


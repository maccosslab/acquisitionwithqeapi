﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mHardklor_2;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;


namespace AcquisitionMethod
{

    public class PepMatchIntensityComparer : IComparer<PeptideMatch>
    {
        public int Compare(PeptideMatch f1, PeptideMatch f2)
        {
            if (f1.intensity > f2.intensity)
                return 1;
            else if (f1.intensity < f2.intensity)
                return -1;
            else
                return 0;
        }
    }



    public class BinIntComparer : IComparer<Bin>
    {
        public int Compare(Bin b1, Bin b2)
        {
            if (b1.targetIntensity > b2.targetIntensity)
                return 1;
            else if (b1.targetIntensity < b2.targetIntensity)
                return -1;
            else
                return 0;
        }
    }

    public class BinMzComparer : IComparer<Bin>
    {
        public int Compare(Bin b1, Bin b2)
        {
            if (b1.lowerBound > b2.lowerBound)
                return 1;
            else if (b1.lowerBound < b2.lowerBound)
                return -1;
            else
                return 0;
        }
    }


    public class IonMZComparer : IComparer<Ion>
    {
        public int Compare(Ion i1, Ion i2)
        {
            if (i1.Mz > i2.Mz)
                return 1;
            else if (i1.Mz < i2.Mz)
                return -1;
            else
                return 0;
        }
    }

    public class ScanBaseSortComparer : IComparer<ScanBase>
    {
        public int Compare(ScanBase s1, ScanBase s2)
        {
            if (s1.ScanNo > s2.ScanNo)
                return 1;
            else if (s1.ScanNo < s2.ScanNo)
                return -1;
            else
                return 0;
        }
    }

    public class PepMZComparer : IComparer<ICentroid>
    {
        public int Compare(ICentroid p1, ICentroid p2)
        {
            if (p1.Mz > p2.Mz)
                return 1;
            else if (p1.Mz < p2.Mz)
                return -1;
            else
                return 0;
        }
    }
    
}

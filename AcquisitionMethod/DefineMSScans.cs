﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Scans;

namespace AcquisitionMethod
{
    public class DefineMSScans
    {


        public static void SetUpScanParameter(ICustomScan scan, MSScanParameter setting)
        {
            if (setting.Order == Types.ScanOrder.MS2)
            {
                //double low = double.Parse(setting.IsolationLow);
                //double high = double.Parse(setting.IsolationHigh);
                //double target = (low + high)/2d;
                int charge = 2;
                if (setting.Charge < 2)
                    charge = 2;
                else if (setting.Charge > 5)
                    charge = 5;
                scan.Values["NCE_NormCharge"] = charge.ToString();
                scan.Values["NCE"] = setting.NCE;
                scan.Values["FirstMass"] = setting.FirstMass;
                scan.Values["LastMass"] = setting.LastMass;
                //scan.Values["FirstMass"] = Parameter.FirstMZ(target, charge);
                //scan.Values["LastMass"] = Parameter.LastMZ(target, charge);
                //50.0-6000.0	First m/z, lower edge of displayed spectral range
            }
            else
            {
                scan.Values["NCE"] = "0";
                scan.Values["FirstMass"] = setting.IsolationLow;
                scan.Values["LastMass"] = setting.IsolationHigh;    
            }
            //scan.Values["FirstMass"] =
            //scan.Values["LastMass"] = Parameter.LastMZ(target, charge);
            scan.Values["IsolationRangeLow"] = setting.IsolationLow;
            scan.Values["IsolationRangeHigh"] = setting.IsolationHigh;
            scan.Values["MaxIT"] = setting.MaxIT;
            scan.Values["Resolution"] = setting.Resolution;
            //scan.Values["Polarity"] = setting.Polarity;
            // some problem with assigning setting 
            scan.Values["Polarity"] = "0";
            scan.Values["AGC_Target"] = setting.AGC_Target;
            //scan.Values["NCE"] = setting.NCE;
            scan.SingleProcessingDelay = setting.SingleProcessingDelay;
        }
        






// here are the all values from IScan PossibleParameters
// 
//Manual Trigger			Provides event after execution of single custom scan
//FirstMass	120	50.0-6000.0	First m/z, lower edge of displayed spectral range
//LastMass	2000	50.0-6000.0	Last m/z, upper edge of displayed spectral range

//IsolationRangeLow	120	50.0-6000.0	Lower m/z edge of isolation range
//IsolationRangeHigh	2000	50.0-6000.0	Upper m/z edge of isolation range


//MsxInjectRanges	[]	string	MSX m/z-ranges for isolation steps. Format - example:  [ (191.5,194.5), (523.3,525.3), (1117.0,1127.0) ] . Leave empty to use single value. 
//MsxInjectTargets	[]	string	MSX AGC targets for injection steps. Format - example: [20000, 50000, 35000]   
//MsxInjectMaxITs	[]	string	MSX (max) inject time in milliseconds for isolation steps. Format - example:  [80.0, 8.5, 120.0]
//MsxInjectNCEs	[]	string	MSX normalized collision energy for isolation steps. Format - example:  [30.0, 55.5, 17.8]

//MaxIT	10	0.03-3000.00	Injection time in milliseconds, maximum injection time for AGC operation.
//Resolution	35000	8000.0-150000.0	Resolution setting, determines transient data acquisition duration per scan
//Polarity	0	0,1	Ion polarity setting, 0=positive ions, 1=negative ions

//NCE	0	-1000	HCD Collision Energy in normalized units.
// ** NCE decide the resulting scan will be full MS or MS/MS

//NCE_NormCharge	2	5-Jan ( I guess this was 1-5)
//NCE_SteppedEnergy	0	0-1000	Setting this to x>0 will split all HCD injections into 3 parts, one at energy reduced by x%%, one at original energy, one at energy increased by x%%.
//NCE_Factors		string	Factors for stepped CE for a more flexible definition. Example:  [0.9, 1.0, 1.3, 1.8] 
//HCDTrappingTime	3	0.1-3000.0	
//DirectCE	0	-1000	HCD collision energy direct eV setting
//SourceCID	0	-1000	In-source fragmentation energy (eV)
//Microscans	1	Jan-00	Number of microscans per logicel scan.
//AGC_Target	1000000	1-10000000	Target number of trapped ions for automatic control (AGC)
//AGC_Mode	1	0,1	1=Enable automatic injection control for number of trapped ions (AGC), 0=Use fixed injection time
//AGC_PeriodicPS	0.66	0.0-5.0	Setting this !=0 inserts wide-mass-range-AGC prescans at given period [sec] (will be active only if prescans are required by analytical scans). The period is calculated automatically when this is set to -1.
//CTCD_Support	1	0,1	1=Enable/0=disable C-trap charge detector periodic operation (for AGC improvement)
//SLensRFLevel	50	0.0-100.0	S-Lens RF level parameter

    }
}

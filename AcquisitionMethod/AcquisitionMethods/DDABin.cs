﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using mHardklor_2;


namespace AcquisitionMethod
{
    public class DDABin : IAcuqisition
    {
        private ScanAnalyzer _hardklor;
        private Queue<Bin> _currentBins = new Queue<Bin>();

        private Parameter _parameter;
        private double _lowerMz = 0d;
        private int _noMoreThanThisNumber;
        private int _placeAMSoneWhenItCountsToThis;
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _nextAnalysisID = 1000;
        private bool _readyForNextMS1 = false;
        private int _MS2Count = 0;
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        private StreamWriter _log = null;
        private int _placeAMSoneIfCandidatesLessThanThis = 10;
        private List<BinLive> _excludedbins = new List<BinLive>();
        private int _lifecycle = 10000;

        private int _reocrdNo;
        //private double _peakTolerance = 0.01d;
        private double _widthTolerance = 0.02d;
        private Queue<Record> _binRecords = new Queue<Record>();
        private StreamWriter _peakNumlog = null;
        private Stopwatch _watch = new Stopwatch();
        private StreamWriter _exclution = null;


        //private double _intThreshold = 0d;
        private double _targetIonCount = 0d;
        private string _hardklorlogfilename = "";
        
        public DDABin(Parameter parameter)
        {

            _parameter = parameter;
            
            _parameter.UseMonoMZToDecideBins = false; // chech this 
            _reocrdNo = _parameter.PeptideInNMSone;
            _noMoreThanThisNumber = _parameter.UpDataFrequency; // ~2.x second on QE+
            _placeAMSoneWhenItCountsToThis = _parameter.PlaceAMSoneAfterNMS2; //20 ~1 second on QE+
            _targetIonCount = _parameter.UnderFillRatio * double.Parse(_parameter.MStwoSetting.AGC_Target);

            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            _hardklor.UseMonoMz = false;
            if (_hardklorlogfilename != "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);

        }
        public Bin NextMSone { get { return new Bin(); } }
        public DDABin(string rawfile,Parameter parameter) :this(parameter)
        {
            
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _peakNumlog = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".np");
            _exclution = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".ex");
            _log = new StreamWriter(filepath);
            _log.WriteLine("# mDDA v0.3 DDABin method.");
            _log.WriteLine("# I am trying to figure why we get less identification result, so I decided to implement something like DDA,");
            _log.WriteLine("# but we are not placing window center to the ion");
            _log.WriteLine("# ");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");


            _peakNumlog.WriteLine("# Peak Number log");
            _peakNumlog.WriteLine("#S Scan AccessID");
            _peakNumlog.WriteLine("#");
            _peakNumlog.WriteLine("#----------------------------------------");
            _exclution.WriteLine("#");
            _exclution.WriteLine("#");
            _exclution.WriteLine("#");

            // set up hardklor analysis log filename
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            
            
            //_intThreshold = Math.Pow(10d, _parameter.IntThreshold);

        }

        public int CheckPersistentInNScans
        {
            get { return _reocrdNo; }
            set { _reocrdNo = value; }
        }

        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);

            // have to analyze the whole scan
            foreach (ICentroid peak in scan.Centroids)
                spectrum.Add(peak.Mz, (float) peak.Intensity);

            string time = TimeString(_watch.Elapsed);
            if (_log != null)
                _log.WriteLine("S\t" + number + "\nT\t" + time);
            if (_peakNumlog != null)
                _peakNumlog.WriteLine("S\t" + number + "\nT\t" + time);

            if (_exclution != null)
                _exclution.WriteLine("\nS\t" + number + "\nT\t" + time);

            // this is different from previous versions, 
            // In this version, I have to record the isotope envelopes from previous scans not bin results
            //PeptideRecord record = new PeptideRecord(peptideIons);

            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            BinsResult binIdx = _hardklor.AssignBins(peptides);
            //FindCommonFeature
            AssignBins(binIdx);

            if (_log != null)
                _log.Flush();
        }

        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            BinsResult binIdx = _hardklor.AssignBins(peptides);
            AssignBins(binIdx);
            return true;
        }


        private List<Bin> FindIntensityForEachBin(BinsResult binIdx)
        {
            List<Bin> bins = new List<Bin>();
            foreach (int idx in binIdx.QualifiedBins().Keys)
            {
                Ion intensOne = new Ion();
                foreach (Ion ion in binIdx.QualifiedIons(idx))
                {
                    if (ion.Intensity > intensOne.Intensity)
                        intensOne = ion;
                }
                Bin b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];
                b.targetIntensity = intensOne.Intensity;
                b.targetCharge = intensOne.Charge;

                double predictIon = intensOne.Intensity * double.Parse(_parameter.MStwoSetting.MaxIT) / 1000d;
                if (predictIon < _targetIonCount)
                {
                    _peakNumlog.Write(_parameter.mzBundaries[idx].ToString() + "-" + _parameter.mzBundaries[idx + 1].ToString() + "\t" + intensOne.Mz + "\t" +
                                  intensOne.Intensity + "\t" + predictIon);
                    _peakNumlog.WriteLine("\tThe intensity is too low, skip this peak");
                    continue;
                }
                bins.Add(b);
            }
            return bins;
        }

        private void AssignBins(BinsResult binIdx)
        {
            _readyForNextMS1 = false;
            Dictionary<int,int> excluded = GetExclutedBins();

            int count = 0;

            // write exclution list log
            #region write log
            if (_exclution != null)
            {
                foreach (int idx in excluded.Keys)
                {
                    double mz = _parameter.mzBundaries[idx];
                    count++;
                    int res = count % 5;
                    if (res == 1)
                        _exclution.Write("E\t" + mz);
                    else if (count % 5 == 0)
                        _exclution.WriteLine("; " + mz);
                    else
                        _exclution.Write("; " + mz);
                }
            }
            #endregion


            List<Bin> bins = FindIntensityForEachBin(binIdx);
            bins.Sort(new BinIntComparer());
            int bincount = 0;
            Bin b = null;

            for (int i=bins.Count-1; i>=0; i--)
            {
                int bidx = _parameter.mzBinIndex(bins[i].lowerBound);
                if (excluded.ContainsKey(bidx))
                {
                    if (_peakNumlog != null)
                    {
                        _peakNumlog.Write(_parameter.mzBundaries[bidx].ToString() + "-" + _parameter.mzBundaries[bidx + 1].ToString() + "\t");
                        _peakNumlog.WriteLine("\tNot A candidate");
                    }
                    continue;
                }

                b = bins[i];

                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                PutIntoExclusionList(bidx);
                bincount++;


                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString()+"\t"+b.targetCharge.ToString());
                if (_peakNumlog != null)
                {
                    _peakNumlog.Write(_parameter.mzBundaries[bidx].ToString() + "-" + _parameter.mzBundaries[bidx + 1].ToString() + "\t");
                    _peakNumlog.WriteLine("\tAdded to candidate list");
                }

                // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }
            if (_peakNumlog != null)
                _peakNumlog.WriteLine("# Qualified bin: " + bincount.ToString());



            // the bin won't be null, otherwise it will not get into here
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

            _readyForNextMS1 = true;
         
        }
        

        private Dictionary<int, int> GetExclutedBins()
        {
            //List<int> excludedIndex = new List<int>();
            Dictionary<int,int> excludedIndex = new Dictionary<int, int>();
            lock (_excludedbins)
            {
                DateTime now = DateTime.Now;
                for (int i = _excludedbins.Count - 1; i >= 0; i--)
                {
                    if (_excludedbins[i].Deadline < now)
                        _excludedbins.RemoveAt(i);
                    else
                    {
                        excludedIndex.Add(_excludedbins[i].BinIdx,0);
                    }
                }
            }
            return excludedIndex;
        }
        private object lock1 = new object();
        private void PutIntoExclusionList(int binIdx)
        {
            BinLive binEx = new BinLive(binIdx, _lifecycle);
            binEx.StartCount();
            lock (_excludedbins)
            {
                _excludedbins.Add(binEx);
            }
        }

        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }

        public Bin NextCandidate
        {
            get
            {
                Bin nextone = null;
                // the _currentBins may be filled by another thread, but it's fine. 
                lock (lockDequeue)
                {
                    if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }

        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber;
            }
        }

        public int PeriodicScanRunningNumber
        {
            get { return _repeat_runningNumber; }
        }

        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }

        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public NextCustomScan.ScanType NextScanType
        {
            get
            {
                // we need one more MS one if the candidates is less than 10 after analyzing
                if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis && _readyForNextMS1)
                {
                    //_needAMSoneNow = false;
                    ResetCustomCounting();
                    return NextCustomScan.ScanType.CustomMSone;
                }
                else if (CandidateCount == 0)
                {
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.None;
                }
                else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    _MS2Count = 0;
                    if (CandidateCount < _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.PeriodicMSone;
                }
                else
                {
                    return NextCustomScan.ScanType.CustomMStwo;
                }
            }
        }

        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == _nextAnalysisID)
            {
                return true;
            }
            else
                return false;
        }


        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
            //if (_analyzed)
            //{
            //_analyzed = false;
            _nextAnalysisID = _custom_runningNumber + 1;
            //}
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            if (_peakNumlog != null)
                _peakNumlog.Close();
            if (_exclution != null)
                _exclution.Close();
            _hardklor.CloseLogFile();
        }

        private string TimeString(TimeSpan ts)
        {
            return String.Format("H:{0:00},Min:{1:00},Sec:{2:00},MS:{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            //string elapsedTime = String.Format("Sec:{0:00},MS:{1:00}", ts.Seconds, ts.Milliseconds);
        }

    }





    


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using System.IO;
using mHardklor_2;
namespace AcquisitionMethod
{
    public class SimpleN:IAcuqisition
    {
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101; 
        private int _MSCount = 0;
        private int _placeAMSoneWhenItCountsToThis = 20;
        private bool _readyForNextMS1 = false;
        private Queue<Bin> _currentBins = new Queue<Bin>();

        private StreamWriter _log = null;

        public SimpleN(string rawfile)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.01 Simple N.");
            _log.WriteLine("# this method is used for testing");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");

        }

        public Bin NextMSone { get { return new Bin(); } }
        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid > 999)
                return true;
            else
                return false;
        }
        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber;
            }
        }
        public int PeriodicScanRunningNumber
        {
            get
            {
                return _repeat_runningNumber;
            }
        }
        public Bin NextCandidate
        {
            get
            {

                if (_currentBins.Count > 0)
                    return _currentBins.Dequeue();
                else
                {
                    Bin b = new Bin();
                    b.upperBound = 397d;
                    b.lowerBound = 395d;
                    return b;
                }

            }
        }
        private int _precursorID = 0;
        private int _MS1Count = 0;

        public void DoAnalysis(IMsScan scan)
        {

            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            _precursorID = int.Parse(number);
            if (_log != null)
            {
                _log.WriteLine("S\t" + number);
                _log.WriteLine("N\t" + _placeAMSoneWhenItCountsToThis.ToString());
            }

            double mzu = 400d + _MS1Count + 1d;
            double mzl = 400d + _MS1Count - 1d;

            for (int i = 0; i < _placeAMSoneWhenItCountsToThis; i++)
            {
                Bin b = new Bin();
                b.upperBound = mzu;
                b.lowerBound = mzl;
                _currentBins.Enqueue(b);
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());
            }

            _readyForNextMS1 = true;
            if (_log != null)
                _log.Flush();
        }

        public bool DoneAnalysis 
        {
            get { return _readyForNextMS1; } 
        }

        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            return false;
        }

        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>

        public NextCustomScan.ScanType NextScanType
        {
            get 
            {
                if (_currentBins.Count == 0)
                {
                    if (_readyForNextMS1)
                    {
                        _MSCount = 0;
                        _MS1Count++;
                        _readyForNextMS1 = false;
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.None;
                }
                else
                {
                    _MSCount++;
                    _readyForNextMS1 = true;
                    return NextCustomScan.ScanType.CustomMStwo;

                }
            }
        }
        
        public void Close() 
        {
            _log.Close();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;

using mHardklor_2;


using System.IO;

namespace AcquisitionMethod
{

    public class UpdateList :  IAcuqisition
    {
        private ScanAnalyzer _hardklor ;
        private Queue<Bin> _currentBins = new Queue<Bin>();

        private Parameter _parameter;
        private double _lowerMz = 0d;
        private int _noMoreThanThisNumber;
        private int _placeAMSoneWhenItCountsToThis;
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _nextAnalysisID = 1000;
        bool _readyForNextMS1 = false;
        int _MS2Count = 0;
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        private Queue<int> _AccessIDs = new Queue<int>();
        private readonly bool _reverseOrder = false;
        private StreamWriter _log = null;
        private int _placeAMSoneIfCandidatesLessThanThis = 10;

        #region test
        
        Random _random = new Random();
        Random _randomCount = new Random();

        #endregion


        public Bin NextMSone { get { return null; } }
        private string _hardklorlogfilename = "";

        public UpdateList(string rawfile, Parameter parameter) : this(rawfile, false, parameter ) { }
        public UpdateList(string rawfile, bool reverseOrder, Parameter parameter):this (parameter)
        {
            
            _reverseOrder = reverseOrder;
            if (_reverseOrder)
                _lowerMz = double.MaxValue;
            else
                _lowerMz = 0d;

            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.2 Update list.");
            _log.WriteLine("# update list after ~2.x seconds");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            
        }
        public UpdateList(Parameter parameter)
        {
            
            _parameter = parameter;
            _noMoreThanThisNumber = _parameter.UpDataFrequency; // ~2.x second on QE+
            _placeAMSoneWhenItCountsToThis = _parameter.PlaceAMSoneAfterNMS2; //20 ~1 second on QE+
            
            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
            if (_hardklorlogfilename!= "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);
        }
        

        // this should be accessed by one thread one time
        public Bin NextCandidate
        {
            get
            {
                Bin nextone = null;
                // the _currentBins may be filled by another thread, but it's fine. 
                lock (lockDequeue)
                {
                    if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }

        /// <summary>
        /// this starts from 1000
        /// </summary>
        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber;
            }
        }

        public int PeriodicScanRunningNumber
        {
            get { return _repeat_runningNumber; }
        }
        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }
        
        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);

            double bundary = 0d;
            //int peakNo = 0;
            if (_reverseOrder)
            {
                if (_lowerMz <= 2000d)
                    bundary = _lowerMz + 4.2;
                else
                    bundary = double.MaxValue;
                foreach (ICentroid peak in scan.Centroids)
                {
                    if (peak.Mz > bundary)
                        break;
                    spectrum.Add(peak.Mz, (float)peak.Intensity);
                }
            }
            else
            {
                bundary = _lowerMz - 4.2; // 4.2 is an arbitray number

                if (bundary < 0)
                    bundary = 0;
                foreach (ICentroid peak in scan.Centroids)
                {
                    // skip the region we have already analyzed.
                    if (peak.Mz < bundary)
                        continue;
                    spectrum.Add(peak.Mz, (float)peak.Intensity);
                    //peakNo++;
                }
            }

            // do hardklor here
            if (_log != null)
                _log.WriteLine("S\t" + number);


            BinsResult binIdx = _hardklor.Analyze(spectrum);

            if (_reverseOrder)
                AssignBinsReverse(binIdx);
            else 
                AssignBins(binIdx);
            
            //AssignBinsRandom();

            if (_log != null)
                _log.Flush();
        }


        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }

        /// <summary>
        /// this is for testing 
        /// </summary>
        /// <param name="spectrum"></param>
        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            BinsResult binIdx = _hardklor.Analyze(spectrum);
            AssignBins(binIdx);
            return true;
        }
    

        /// <summary>
        /// this is only for testing !!!!
        /// </summary>
        private void AssignBinsRandom()
        {
            _readyForNextMS1 = false;
            int numberOfbin = _randomCount.Next(0, 50);
            List<int> mzs = new List<int>();
            for (int i = 0; i < numberOfbin; i++)
            {
                mzs.Add(_random.Next(400, 1400));
            }

            mzs.Sort();
            //_hardklorAnalysis.WriteLine(keys.Count.ToString());
            int bincount = 0;
            Bin b = null;
            foreach (int mz in mzs)
            {
                b = new Bin();
                b.lowerBound = mz - 1d;
                b.upperBound = mz + 1d;

                // this is where we decide which bins we are going to analyze next.
                if (b.lowerBound < _lowerMz)
                    continue;
                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());

                bincount++; // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }
            // this means the number of candidates from the selected region is less than what we want, so we should reset
            // the lower boundary to 0 
            if (bincount < _noMoreThanThisNumber)
                _lowerMz = 0d;
            else
                _lowerMz = b.upperBound; // this should be the last bin in the queue
            // the bin won't not be null, otherwise it will not get into here
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());
            //if (bincount == 0)
            _readyForNextMS1 = true;
            //_needAMSoneNow = true;

            //_analyzed = true;
        }


        private void AssignBins(BinsResult binIdx)
        {
            _readyForNextMS1 = false;
            List<int> keys = binIdx.QualifiedBins().Keys.ToList<int>();
            keys.Sort();
            
            //_hardklorAnalysis.WriteLine(keys.Count.ToString());
            int bincount = 0;
            Bin b = null;
            foreach (int idx in keys)
            {
                b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];

                // this is where we decide which bins we are going to analyze next.
                if (b.lowerBound < _lowerMz) 
                    continue;
                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());
                
                bincount++; // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }
            
            // this means the number of candidates from the selected region is less than what we want, so we should reset
            // the lower boundary to 0 
            if (bincount < _noMoreThanThisNumber)
                _lowerMz = 0d;
            else
                _lowerMz = b.upperBound; // this should be the last bin in the queue
                                         // the bin won't be null, otherwise it will not get into here
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

            //if (bincount == 0)
            _readyForNextMS1 = true;
            //_needAMSoneNow = true;

            //_analyzed = true;
        }

        private void AssignBinsReverse(BinsResult binIdx)
        {
            _readyForNextMS1 = false;
            List<int> keys = binIdx.QualifiedBins().Keys.ToList<int>();
            keys.Sort();
            keys.Reverse(); // reverse
            
            int bincount = 0;
            Bin b = null;
            foreach (int idx in keys) // already reversed
            {
                b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];

                if (b.upperBound > _lowerMz) // reverse
                    continue;
                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());

                bincount++; 
                if (bincount == _noMoreThanThisNumber)
                    break;
            }

            if (bincount < _noMoreThanThisNumber)
                _lowerMz = double.MaxValue; // reverse
            else
                _lowerMz = b.lowerBound; // reverse
            
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

            _readyForNextMS1 = true;
        }

        
        public NextCustomScan.ScanType NextScanType 
        {
            get 
            {
                // we need one more MS one if the candidates is less than 10 after analyzing
                if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis && _readyForNextMS1) 
                {
                    //_needAMSoneNow = false;
                    ResetCustomCounting();
                    return NextCustomScan.ScanType.CustomMSone;
                }
                else if (CandidateCount == 0) 
                {
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.None;
                }
                else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    _MS2Count = 0;
                    if (CandidateCount < _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                    //if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.PeriodicMSone;
                }
                else
                {
                    return NextCustomScan.ScanType.CustomMStwo;
                }
            }
        }



        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
            //if (_analyzed)
            //{
                //_analyzed = false;
                _nextAnalysisID = _custom_runningNumber + 1;
            //}
        }


        //bool _analyzed = false;
        // if there is nothing wrong, i should be able to set up the Access ID currectly.
        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == _nextAnalysisID)
            {
                return true;
            }
            else
                return false;
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            _hardklor.CloseLogFile();
        }
    }
}

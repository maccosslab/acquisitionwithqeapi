﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using mHardklor_2;


namespace AcquisitionMethod
{
    public class BinCharge : IAcuqisition
    {
        private ScanAnalyzer _hardklor;
        private Queue<Bin> _currentBins = new Queue<Bin>();

        private Parameter _parameter;
        private double _lowerMz = 0d;
        private int _noMoreThanThisNumber;
        private int _placeAMSoneWhenItCountsToThis;
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _nextAnalysisID = 1000;
        private bool _readyForNextMS1 = false;
        private int _MS2Count = 0;
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        private StreamWriter _log = null;
        private int _placeAMSoneIfCandidatesLessThanThis = 10;

        private int _reocrdNo;
        //private double _peakTolerance = 0.01d;
        private double _widthTolerance = 0.02d;
        private Queue<Record> _binRecords = new Queue<Record>();
        private StreamWriter _peakNumlog = null;

        //private double _intThreshold = 0d;
        private double _targetIonCount = 0d;
        private string _hardklorlogfilename = "";
        public BinCharge(Parameter parameter)
        {
            
            _parameter = parameter;
            
            _reocrdNo = _parameter.PeptideInNMSone;
            _noMoreThanThisNumber = _parameter.UpDataFrequency; // ~2.x second on QE+
            _placeAMSoneWhenItCountsToThis = _parameter.PlaceAMSoneAfterNMS2; //20 ~1 second on QE+
            _targetIonCount = _parameter.UnderFillRatio * double.Parse(_parameter.MStwoSetting.AGC_Target);

            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
            if (_hardklorlogfilename!= "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);

        }
        public Bin NextMSone { get { return null; } }
        public BinCharge(string rawfile, Parameter parameter) : this(parameter)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _peakNumlog = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".np");
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.55 BinCharge method.");
            _log.WriteLine("# in this version, the ms2 m/z range is changed based on the bin charge... ");
            _log.WriteLine("# the bin charge is the charge state of dominat peak in the bin");
            _log.WriteLine("# in addition, I also change the NCENormCharge parameter when setting up a ms2 scan");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");


            _peakNumlog.WriteLine("# Peak Number log");
            _peakNumlog.WriteLine("#S Scan AccessID");
            _peakNumlog.WriteLine("#");
            _peakNumlog.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            
            
            //_intThreshold = Math.Pow(10d, Parameter.GetInstance().IntThreshold);

        }

        public int CheckPersistentInNScans
        {
            get { return _reocrdNo; }
            set { _reocrdNo = value; }
        }

        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);

            // have to analyze the whole scan
            foreach (ICentroid peak in scan.Centroids)
                spectrum.Add(peak.Mz, (float) peak.Intensity);

            if (_log != null)
                _log.WriteLine("S\t" + number);
            if (_peakNumlog != null)
                _peakNumlog.WriteLine("S\t" + number);

            // this is different from previous versions, 
            // In this version, I have to record the isotope envelopes from previous scans not bin results
            //PeptideRecord record = new PeptideRecord(peptideIons);

            //BinsResult binIdx = _hardklor.Analyze(spectrum);
            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            BinsResult binIdx = _hardklor.AssignBins(peptides);

            //FindCommonFeature
            AssignBins(binIdx);

            if (_log != null)
                _log.Flush();
        }

        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            BinsResult binIdx = _hardklor.AssignBins(peptides);
            AssignBins(binIdx);
            return true;
        }


        private void AssignBins(BinsResult binIdx)
        {
            _readyForNextMS1 = false;
            //List<int> keys = binIdx.QualifiedBins().Keys.ToList<int>();
            //keys.Sort();

            if (_binRecords.Count >= _reocrdNo)
                _binRecords.Dequeue();

            Record r = new Record(binIdx);
            _binRecords.Enqueue(r);
            //List<int> commonIdx = FindCommonFeature(_binRecords.ToList());
            List<Bin> commonBins = FindCommonFeature(_binRecords.ToList());


            //_hardklorAnalysis.WriteLine(keys.Count.ToString());
            int bincount = 0;
            Bin b = null;
            for (int i=0;i<commonBins.Count;i++)
            {
                b =commonBins[i];

                // m/z checking has been movded to FindCommonFeature function. 
                //if (b.lowerBound < _lowerMz)
                //    continue;

                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString()+"\t"+b.targetCharge.ToString());

                bincount++;
                // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }

            // this means the number of candidates from the selected region is less than what we want, so we should reset
            // the lower boundary to 0 
            if (bincount < _noMoreThanThisNumber)
                _lowerMz = 0d;
            else
                _lowerMz = b.upperBound; // this should be the last bin in the queue
            // the bin won't be null, otherwise it will not get into here
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

            //if (bincount == 0)
            _readyForNextMS1 = true;
            //_needAMSoneNow = true;
            //_analyzed = true;
        }


        private List<Bin> FindCommonFeature(List<Record> records)
        {
            List<int> commonIdx = new List<int>();
            List<Bin> commonBins = new List<Bin>();


            Dictionary<int, List<PersistPep>> commonDic = new Dictionary<int, List<PersistPep>>();

            foreach (int idx in records[0].IndexList)
            {
                // checking mz
                double checkMZ = _parameter.mzBundaries[idx];
                if (checkMZ < _lowerMz)
                    continue;
                commonDic.Add(idx, new List<PersistPep>());
                foreach (Ion ion in records[0].BinIdx.QualifiedIons(idx))
                    commonDic[idx].Add(new PersistPep(ion));
            }

            int startIdx = 1;
            while (startIdx < records.Count)
            {
                commonDic = FindCommon(commonDic, records[startIdx]);
                startIdx++;
            }

            commonIdx = commonDic.Keys.ToList();
            commonIdx.Sort();

            Bin b;
            foreach (int idx in commonIdx)
            {
                Ion intensOne = new Ion();
                foreach (PersistPep pep in commonDic[idx])
                {
                    if (pep.LastIon.Intensity > intensOne.Intensity)
                        intensOne = pep.LastIon;
                }

                double predictIon = intensOne.Intensity * double.Parse(_parameter.MStwoSetting.MaxIT) / 1000d;
                _peakNumlog.Write(_parameter.mzBundaries[idx].ToString() + "-" + _parameter.mzBundaries[idx + 1].ToString() + "\t" + intensOne.Mz + "\t" +
                                  intensOne.Intensity + "\t" + predictIon);
       
                if (predictIon < _targetIonCount)
                {
                    _peakNumlog.WriteLine("\tThe intensity is too low, skip this peak");
                    continue;
                }
                b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];
                b.targetCharge = intensOne.Charge;
                commonBins.Add(b);
                _peakNumlog.WriteLine("\tAdd to candidate list");
            }
            _peakNumlog.WriteLine("# Qualified bin: " + commonBins.Count.ToString());
            return commonBins;
        }



        private Dictionary<int, List<PersistPep>> FindCommon(Dictionary<int, List<PersistPep>> common, Record currentRecord)
        {
            Dictionary<int, List<PersistPep>> result = new Dictionary<int, List<PersistPep>>();
            //foreach ()
            foreach (int idx in currentRecord.IndexList)
            {
                if (common.ContainsKey(idx))
                {
                    // I think it's only two to three ions in a bin so... I could just use double loops.
                    foreach (PersistPep pep in common[idx])
                    {
                        Ion closetOne = null;
                        double minDif = double.MaxValue;
                        
                        foreach (Ion ion in currentRecord.BinIdx.QualifiedIons(idx))
                        {
                            bool pass = true;
                            if (pep.Charge != ion.Charge)
                                continue;
                            else
                            {
                                double dif = 0d;
                                foreach (Ion pion in pep.PepIons())
                                {
                                    double error = Math.Round(pion.Mz - ion.Mz);
                                    if (error <= _widthTolerance)
                                        dif += error;
                                    else
                                    {
                                        pass = false;
                                        break;
                                    }
                                }
                                if (pass)
                                {
                                    if (dif < minDif)
                                        closetOne = ion;
                                }
                            }
                        }
                        if (closetOne != null)
                        {
                            pep.AddIon(closetOne);
                            if (!result.ContainsKey(idx))
                                result.Add(idx,new List<PersistPep>());
                            result[idx].Add(pep);
                        }
                    }
                }
            }
            return result;
        }




        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }



        public Bin NextCandidate
        {
            get
            {
                Bin nextone = null;
                // the _currentBins may be filled by another thread, but it's fine. 
                lock (lockDequeue)
                {
                    if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }

        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber;
            }
        }

        public int PeriodicScanRunningNumber
        {
            get { return _repeat_runningNumber; }
        }

        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }

        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public NextCustomScan.ScanType NextScanType
        {
            get
            {
                // we need one more MS one if the candidates is less than 10 after analyzing
                if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis && _readyForNextMS1)
                {
                    //_needAMSoneNow = false;
                    ResetCustomCounting();
                    return NextCustomScan.ScanType.CustomMSone;
                }
                else if (CandidateCount == 0)
                {
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.None;
                }
                else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    _MS2Count = 0;
                    if (CandidateCount < _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                        //if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.PeriodicMSone;
                }
                else
                {
                    return NextCustomScan.ScanType.CustomMStwo;
                }
            }
        }

        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == _nextAnalysisID)
            {
                return true;
            }
            else
                return false;
        }


        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
            //if (_analyzed)
            //{
            //_analyzed = false;
            _nextAnalysisID = _custom_runningNumber + 1;
            //}
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            if (_peakNumlog != null)
                _peakNumlog.Close();
            _hardklor.CloseLogFile();
        }

    }





    


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using mHardklor_2;
using System.Diagnostics;
using System.IO;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;

namespace AcquisitionMethod
{
    public class mDDAv3 : IAcuqisition
    {
        private ScanAnalyzer _hardklor;
        private Queue<Bin> _currentBins = new Queue<Bin>();
        //private bool _readyForNextMS1 = false;

        private TextWriter _log = null;
        private TextWriter _exclution = null;
        private int _lifecycle = 10000; // 10,000 milliseconds = 10 seconds
        private Bin _msone = new Bin();

        private List<PepIsotopes> _excludedPepIons = new List<PepIsotopes>();
        //private StreamWriter _peakNumlog = null;
        private Stopwatch _watch = new Stopwatch();
        private Parameter _parameter;

        //
        // adpate mDDAv2 to latest version UI and parameter setting. 2017/05/08
        //
        public mDDAv3(string rawfile, Parameter parameter)
        {
            try
            {
                _parameter = parameter;
                _lifecycle = parameter.DynamicExclusion * 1000; // convert to millisecond 
                WriteLogHeader(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".log");
                WriteExclusionHeader(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".ex");

                double reso = double.Parse(_parameter.MSoneSetting.Resolution) / Math.Sqrt(2); // resolution for hardklor 
                _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge,reso);
                _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
                

                //string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
                //_hardklor.SetUpLogFilename(filepath);
                

                
                _msone.ScanType = Types.ScanType.RegularMSone;
                _msone.lowerBound = double.Parse(_parameter.MSoneSetting.IsolationLow);
                _msone.upperBound = double.Parse(_parameter.MSoneSetting.IsolationHigh);

                //_peakNumlog = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".np");
                //_peakNumlog.WriteLine("# Peak Number log");
                //_peakNumlog.WriteLine("#S Scan AccessID");
                //_peakNumlog.WriteLine("#P #Peak In Scan\t# Excluded Peak");
                //_peakNumlog.WriteLine("#----------------------------------------");

            }
            catch (Exception e)
            {
                AddMessage(e.Message);
                AddMessage(e.Source);
                AddMessage(e.StackTrace);
            }
        }

        public void DoAnalysis(IMsScan scan)
        {
            if (!_watch.IsRunning)
                _watch.Start();

            // get scan info
            string number = "";
            string scanNo = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            MsScanInformationSource common = MsScanInformationSource.Common;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            scan.CommonInformation.TryGetValue("Scan", out scanNo, ref common);

            // centroid peaks
            SpectrumWrapper spectrum = new SpectrumWrapper();
            foreach (ICentroid peak in scan.Centroids)
            {
                spectrum.Add(peak.Mz, (float)peak.Intensity);
            }
            // hardklor log
            string time = TimeString(_watch.Elapsed);


            AddMessage("S\t" + scanNo +'\t'+number+ "\nT\t" + time);
            AddExclusionMessage("S\t" + scanNo + "\nT\t" + time);


            //if (_peakNumlog != null)
            //    _peakNumlog.WriteLine("S\t" + number);


            // it's doing analysis now, I will get candidate list soon
            //_readyForNextMS1 = false;

            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            
            List<Bin> candidates = GetTopNCandidates(peptides);
            AddMessage("N\t" + candidates.Count.ToString());

            // put all candidates in the queue
            lock (_currentBins)
            {
                foreach (Bin bin in candidates)
                {
                    AddMessage("B\t" + bin.lowerBound.ToString() + "\t" + bin.upperBound.ToString() + "\t" + bin.targetCharge.ToString());
                    _currentBins.Enqueue(bin);
                }
                //add a MSone after MS2 scans
                _currentBins.Enqueue(_msone);

            }
            //_readyForNextMS1 = true;
            
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pepions"></param>
        /// <returns>from higher intensity to lower intensity</returns>
        private List<Bin> GetTopNCandidates(List<PeptideMatch> matches)
        {
            List<double> mzs = GetExclutedMZs();
            mzs.Sort();

            AddExclusionMessage("total # of excluded ions: " + mzs.Count);
            StringBuilder sb = new StringBuilder();
            for (int idx = 0; idx < mzs.Count; idx ++)
            {   
                int res = (idx+1) % 5;
                if (res == 1)
                    sb.Append("E\t" + mzs[idx]);
                else if (res == 0)
                    sb.AppendLine("; " + mzs[idx]);
                else
                    sb.Append("; " + mzs[idx]);
            }
            if( sb.Length > 0)
                AddExclusionMessage(sb.ToString());


            // only consider multiple charged isotope evenlopes
            List<PeptideMatch> pepions = new List<PeptideMatch>();
            foreach (PeptideMatch isotope in matches)
            {
                if (isotope.charge >= _parameter.minCharge && isotope.charge <= _parameter.maxCharge)
                    pepions.Add(isotope);
            }

            //if (_peakNumlog != null)
            //{
            //    int no = mzs.Count / 5;
            //    _peakNumlog.WriteLine("#P\t" + pepions.Count.ToString() + "\t" + no.ToString());
            //}

            // analyze and get the top N peak.
            pepions.Sort(new PepMatchIntensityComparer());
            List<Bin> candidates = new List<Bin>();

            double offset = _parameter.windowWidth / 2;
            for (int i = pepions.Count - 1; i >= 0; i--) // from high intensity to low intensity
            {

                if (pepions[i].intensity < _parameter.IntensityThreshold) //peptide intensity is base peak intensity
                {
                    //_peakNumlog.WriteLine(pepions[i].baseMZ + "\t" + pepions[i].intensity + "\tThe intensity is too low, break the loop");
                    int nopeak = pepions.Count - 1 - i;
                    //AddMessage("Top "+nopeak+" peaks will be analyzed with MS2");
                    int nextone = (nopeak+1);
                    //AddMessage("The intensity of "+nextone+"(th) peak is too low: mz " + pepions[i].baseMZ + "\t" + pepions[i].intensity + "  -- skip");
                    break;
                }

                bool added = false;

                // isolate on mono isotope peak or base peak
                double isolatedmz = 0d;
                if (_parameter.UseMonoMZToDecideBins)
                    isolatedmz = (pepions[i].monoMass + Molecules.Proton * pepions[i].charge) / pepions[i].charge; // monoMZ
                else
                    isolatedmz = pepions[i].baseMZ;

                double tolerance = Function.PPMToDa(isolatedmz, _parameter.PepTolerance);

                for (int idx = 0; idx < mzs.Count; idx++)
                {
                    double dif = Math.Abs(mzs[idx] - isolatedmz); 
                    if (dif > tolerance)
                    {
                        if (mzs[idx] > isolatedmz)
                            break;
                    }
                    else // within tolerance 
                    {
                        added = true;
                        //_peakNumlog.WriteLine("Match to excluded mz: " + mzs[idx].ToString() + "\tPepMass: " + pepions[i].baseMZ + "\ttolerance:" + tolerance.ToString()+"\tError"+dif.ToString());
                        break;
                    }
                }
                if (!added)
                {
                    Bin b = new Bin();
                    b.upperBound = isolatedmz + offset;
                    b.lowerBound = isolatedmz - offset;
                    b.targetCharge = pepions[i].charge;
                    b.ScanType = Types.ScanType.RegularMStwo;
                    candidates.Add(b);
                    // put into exclusion list
                    PutIntoExclusionList(pepions[i]);
                    //_peakNumlog.WriteLine(pepions[i].baseMZ + "\t" + pepions[i].intensity + "\tAdded to candidate list");
                }
                //else
                //    _peakNumlog.WriteLine(pepions[i].baseMZ + "\t" + pepions[i].intensity + "\tNot a candidate");
                // only select N ions 
                if (candidates.Count >= _parameter.TopN)
                    break;
            }
            //_peakNumlog.WriteLine("# Qualified candidate: " + candidates.Count.ToString());
            //_peakNumlog.Flush();

            return candidates;

        }

        private List<double> GetExclutedMZs()
        {
            List<double> mzpoisitions = new List<double>();
            lock (_excludedPepIons)
            {
                DateTime now = DateTime.Now;
                for (int i = _excludedPepIons.Count - 1; i >= 0; i--)
                {
                    if (_excludedPepIons[i].Deadline < now)
                        _excludedPepIons.RemoveAt(i);
                    else
                    {
                        // exclude all isotope peaks
                        if (_parameter.ExcludeIsotopes)
                        {
                            foreach (double mz in _excludedPepIons[i].IsotopeMZs)
                                mzpoisitions.Add(mz);
                        }
                        else // only exclude one of the peak
                        {
                            if (_parameter.UseMonoMZToDecideBins)
                                mzpoisitions.Add(_excludedPepIons[i].IsotopeMZs[0]); // exclude monoisotope peak
                            else
                                mzpoisitions.Add(_excludedPepIons[i].IsotopeMZs[_excludedPepIons[i].BasepeakIndex]);  // exclude base peak
                        }
                    }
                }
            }
            return mzpoisitions;
        }

        private void PutIntoExclusionList(PeptideMatch pepion)
        {
            PepIsotopes pepIsotope = new PepIsotopes(pepion, _lifecycle);
            lock (_excludedPepIons)
            {
                pepIsotope.StartCount();
                _excludedPepIons.Add(pepIsotope);
            }
        }


        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            return false;
        }

 


        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == (int)Types.ScanType.RegularMSone)
                return true;
            else
                return false;
        }

        public Bin NextIsolationBin
        {
            get
            {
                Bin b = null;
                if (_currentBins.Count > 0)
                {
                    lock (_currentBins)
                    {
                        if (_currentBins.Count > 0)
                        {
                            b = _currentBins.Dequeue();
                        }
                    }
                }
                return b;
            }
        }

 
        private static readonly object _syncExcObject = new object();
        private void AddExclusionMessage(string message)
        {
            if (_exclution != null)
            {
                lock (_syncExcObject)
                {
                    _exclution.WriteLine(message);
                    _exclution.Flush();
                }
            }
        }
        private void WriteExclusionHeader(string filepath)
        {
            _exclution = TextWriter.Synchronized(new StreamWriter(filepath));
            AddExclusionMessage("# DDA v0.3");
            AddExclusionMessage("# Exclusion list in real time");
            AddExclusionMessage("# Settings used in this experiment");
            AddExclusionMessage("# Exclude isotopes: "+_parameter.ExcludeIsotopes);
            AddExclusionMessage("# Exclude ppm: " + _parameter.PepTolerance);
            AddExclusionMessage("# Exclude time (sec): " + _parameter.DynamicExclusion);
            AddExclusionMessage("# ");
            AddExclusionMessage("#----------------- File format ------------------");
            AddExclusionMessage("#S Scan Number");
            AddExclusionMessage("#T Running Time");
            AddExclusionMessage("#E Excluded mzs");
            AddExclusionMessage("#E Excluded mzs");
            AddExclusionMessage("#------------------------------------------------");
        }


        private void WriteLogHeader(string filepath)
        {
            _log = TextWriter.Synchronized(new StreamWriter(filepath));
            AddMessage("# DDA v0.3");
            AddMessage("# ");
            AddMessage("# Settings used in this experiment");
            AddMessage("# Isolate on monoisotope peak: "+_parameter.UseMonoMZToDecideBins);
            AddMessage("#----------------------------------------");
            AddMessage("#H  Header ");
            AddMessage("#S	Scan AccessID");
            AddMessage("#T	Running Time");
            AddMessage("#N	Number of bins");
            AddMessage("#B	Lower MZ	 Upper MZ");
            AddMessage("#----------------------------------------");
        }


        private static readonly object _syncObject = new object();
        private void AddMessage(string message)
        {

            if (_log != null)
            {
                lock (_syncObject)
                {
                    _log.WriteLine(message);
                    _log.Flush();
                }
            }
        }



        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }



        //public int PeriodicScanRunningNumber { get { return _repeat_runningNumber; } }
        public bool DoneAnalysis { get { return false; } }

        public void Close()
        {
            lock (_syncObject)
            {
                if (_log != null)
                    _log.Close();
                if (_exclution != null)
                    _exclution.Close();
                //if (_peakNumlog != null)
                //    _peakNumlog.Close();
                if (_hardklor != null)
                    _hardklor.CloseLogFile();
            }
        }

        private string TimeString(TimeSpan ts)
        {
            return String.Format("H:{0:00},Min:{1:00},Sec:{2:00},MS:{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            //string elapsedTime = String.Format("Sec:{0:00},MS:{1:00}", ts.Seconds, ts.Milliseconds);
        }


    }



   
}

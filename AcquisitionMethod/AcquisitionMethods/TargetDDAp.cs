﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using mHardklor_2;


namespace AcquisitionMethod
{
    public class TargetDDAp : IAcuqisition
    {


        private ScanAnalyzer _hardklor;
        private Queue<Bin> _candidateBins = new Queue<Bin>();
        private Queue<Bin> _priorityBins = new Queue<Bin>();
        private readonly Bin _msOne = new Bin();
        private readonly Parameter _parameter;
        private readonly string _hardklorlogfilename = "";
        private StreamWriter _log = null;

        private readonly int _noMoreThanThisNumber;
        private readonly int _placeAMSoneWhenItCountsToThis;
        private readonly int _placeAMSoneIfCandidatesLessThanThis = 10;
        private readonly int _recordNo;

        private bool _readyForNextMS1 = false;
        private int _MS2Count = 0;
        private double _lowerMz = 0d;
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        
        // Target DDAp specific region
        private readonly SortedList<double, int> _targetedMzIdx = new SortedList<double, int>();
        private readonly Dictionary<double,List<Ion>> _mzToIon = new Dictionary<double, List<Ion>>();
        private readonly Dictionary<double,double> _mzToleranceDic = new Dictionary<double, double>();
        private Dictionary<Ion, int> _matchedCount = new Dictionary<Ion, int>();
        private Queue<List<Ion>> _previousMatches = new Queue<List<Ion>>();
        

        public TargetDDAp(List<Ion> targetList, Parameter parameter) 
        {
            
            _parameter = parameter;
            _recordNo = _parameter.PeptideInNMSone;
            _msOne.upperBound = double.Parse(_parameter.MSoneSetting.IsolationHigh);
            _msOne.lowerBound = double.Parse(_parameter.MSoneSetting.IsolationLow);
            _msOne.ScanType = Types.ScanType.RegularMSone;

            _noMoreThanThisNumber = _parameter.UpdateFrequency; // ~2.x second on QE+
            _placeAMSoneWhenItCountsToThis = _parameter.PlaceAMSoneAfterNMS2; //20 ~1 second on QE+

            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
            if (_hardklorlogfilename != "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);
            targetList.Sort();

            //SortedList<double, int> _targetedMzIdx = new SortedList<double, int>();
            int index = 0;
            for (int idx = 0; idx < targetList.Count; idx++)
            {
                double mz = targetList[idx].Mz;
                if (!_mzToIon.ContainsKey(mz))
                    _mzToIon.Add(mz,new List<Ion>());
                _mzToIon[mz].Add(targetList[idx]);

                for (int i = index; i < _parameter.mzBundaries.Count; i++)
                {
                    if (_parameter.mzBundaries[i] < mz && mz <= _parameter.mzBundaries[i + 1])
                    {
                        _targetedMzIdx.Add(mz, i);
                    }
                    else if (_parameter.mzBundaries[i] > mz)
                    {
                        index = i;
                        break;
                    }
                }
            }

            if (_parameter.pepToleranceUnit == Parameter.ToleranceUnit.Da)
            {
                foreach (double mz in _targetedMzIdx.Keys)
                    _mzToleranceDic.Add(mz, _parameter.PepTolerance);
            }
            else if (_parameter.pepToleranceUnit == Parameter.ToleranceUnit.ppm)
            {
                foreach (double mz in _targetedMzIdx.Keys)
                    _mzToleranceDic.Add(mz,Function.PPMToDa(mz,_parameter.PepTolerance));
            }

        }

        public TargetDDAp(string rawfile, List<Ion> targetList, Parameter parameter) : this(targetList, parameter)
        {
            
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".log";
            
            _log = new StreamWriter(filepath);
            _log.WriteLine("# Target DDAp v0.1 ");
            _log.WriteLine("# ");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            
        }
  
        public void DoAnalysis(IMsScan scan)
        {
            _readyForNextMS1 = false;
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);
            
            foreach (ICentroid peak in scan.Centroids)
                spectrum.Add(peak.Mz, (float) peak.Intensity);

            if (_log != null)
                _log.WriteLine("S\t" + number);

            // all computing steps
            List<Ion> peptideions =  _hardklor.DetectPeptideIons(spectrum);
            FindMatches(peptideions);
            RefineMatchCount();
            AssignBins();
            _readyForNextMS1 = true;

            if (_log != null)
                _log.Flush();

        }

        private void FindMatches(List<Ion> peptideion)
        {
            int index = 0;
            List<Ion> matchedIons = new List<Ion>();
            for (int i = 0; i < peptideion.Count; i++)
            {
                
                //if (peptideion[i].Charge < _parameter.minCharge || peptideion[i].Charge > _parameter.maxCharge)
                //    continue;

                for (int idx = index; idx < _targetedMzIdx.Keys.Count; idx++)
                {
                    double mz = _targetedMzIdx.Keys[idx];
                    if (peptideion[i].Mz > mz + _mzToleranceDic[mz])
                    {
                        continue;
                    }
                    else if (peptideion[i].Mz < mz - _mzToleranceDic[mz])
                    {
                        index = idx;
                        break;
                    }
                    else
                    {
                        // only interest in the m/z with particular charge states
                        foreach (Ion ion in _mzToIon[mz])
                        {
                            if (ion.Charge == peptideion[i].Charge)
                            {
                                if (!_matchedCount.ContainsKey(ion))
                                    _matchedCount.Add(ion, 1);
                                else
                                    _matchedCount[ion]++;
                                break;
                                matchedIons.Add(ion);
                            }
                        }
                    }
                }
            }
            _previousMatches.Enqueue(matchedIons);
        }

        private void RefineMatchCount()
        {
            if (_previousMatches.Count > _recordNo)
            {
                List<Ion> oldmatches = _previousMatches.Dequeue();
                foreach (Ion ion in oldmatches)
                    _matchedCount[ion]--;
            }
        }

        private void AssignBins()
        {
            
            List<Bin> bins = new List<Bin>();
            List<Bin> backup = new List<Bin>();
            Dictionary<int,bool> addedIdx = new Dictionary<int, bool>();

            foreach (Ion ion in _matchedCount.Keys)
            {
                if (_matchedCount[ion] == _recordNo)
                {
                    Bin bin = new Bin();
                    int idx = _targetedMzIdx[ion.Mz];
                    
                    // skip if the bin region has been added into the queue.
                    if (!addedIdx.ContainsKey(idx))
                        addedIdx.Add(idx,true);
                    else 
                        continue;

                    bin.lowerBound = _parameter.mzBundaries[idx];
                    bin.upperBound = _parameter.mzBundaries[idx + 1];
                    bin.ScanType = Types.ScanType.RegularMStwo;
                    if (bin.lowerBound < _lowerMz)
                        backup.Add(bin);
                    else 
                        bins.Add(bin);
                }
            }

            #region add bin into the queue
            bins.Sort(new BinMzComparer());
            Bin b = null;
            int bincount=0;
            for (int i = 0; i < bins.Count; i ++)
            {
                b = bins[i];
                AddBinInQueue(b);
                bincount++;
                if (bincount == _noMoreThanThisNumber)
                    break;
            }
            if (bincount < _noMoreThanThisNumber && backup.Count > 0)
            {
                backup.Sort(new BinMzComparer());
                for (int i = 0; i < backup.Count; i ++)
                {
                    b = backup[i];
                    AddBinInQueue(b);
                    bincount++;
                    if (bincount == _noMoreThanThisNumber)
                        break;
                }
            }
            #endregion

            if (b == null)
                _lowerMz = 0d;
            else
                _lowerMz = b.upperBound;
            
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

        }

        private void AddBinInQueue(Bin b)
        {
            lock (lockEnqueue)
            {
                _candidateBins.Enqueue(b);
            }
            if (_log != null)
                _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());
        }





        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            return false;
        }

        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }



        public Bin NextIsolationBin
        {
            get
            {
                Bin nextone = null;
                lock (lockDequeue)
                {
                    if (_priorityBins.Count > 0)
                    {
                        nextone = _priorityBins.Dequeue();
                    }
                    else if (_candidateBins.Count > 0)
                    {
                        nextone = _candidateBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }


        private int CandidateCount
        {
            get { return _candidateBins.Count; }
        }

        
        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public Types.ScanType NextScanType
        {
            get
            {
                // we need one more MS one if the candidates is less than 10 after analyzing
                if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis || _MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    if (_readyForNextMS1)
                    {
                        _priorityBins.Enqueue(_msOne);
                        ResetCustomCounting();
                        return Types.ScanType.RegularMSone;
                    }
                    else
                        return Types.ScanType.None; // waiting for analysis result of previous MS1, do nothing
                    // think about this more later
                }
                else
                {
                    return Types.ScanType.RegularMStwo;
                }
                #region remove this later
                //else if (CandidateCount == 0)
                //{
                //    if (_readyForNextMS1)
                //    {
                //        _priorityBins.Enqueue(_msOne);
                //        ResetCustomCounting();
                //        return Types.ScanType.RegularMSone;
                //    }
                //    else // do nothing 
                //        return Types.ScanType.None;
                //}
                //else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                //{
                //    _MS2Count = 0;
                //    if (CandidateCount < _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                //        //if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis)
                //    {
                //        ResetCustomCounting();
                //        return Types.ScanType.RegularMSone;
                //    }
                //    return Types.ScanType.RegularMSone;
                //}
                #endregion

            }
        }

        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == (int)_msOne.ScanType)
                return true;
            else
                return false;
        }


        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            _hardklor.CloseLogFile();
        }

    }





    


}

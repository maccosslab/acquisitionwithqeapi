﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using mHardklor_2;
using System.IO;
using System.Diagnostics;

namespace AcquisitionMethod
{
    public class DIADA : IAcuqisition
    {

        // Tryptophan 159.0922 
        // Phenylalanine 120.0813 
        private readonly List<Bin> _diaWindows = new List<Bin>();
        private readonly ScanAnalyzer _hardklor;
        private readonly double _ms2helfWinSize = 0d;
        private readonly double _simhelfWinSize = 0d;
        private readonly double _simeoffset = 1d;
        private readonly List<double> _reporterIons = new List<double>();
        private readonly List<double> _tolerances = new List<double>();

        private int _currentDIAindex = 0;
        private Queue<Bin> _simScans = new Queue<Bin>();
        private Queue<Bin> _ms2candidates = new Queue<Bin>();
        
        

        private TextWriter _log = null;
        private Stopwatch _watch = new Stopwatch();

        // add 04/28 -- temp
        //private StreamWriter _templog = null;

        // Do a low resolution MS1 every ~3 sec for AGC prediction (30,000)
        // with scan rate of 20Hz, 3 sec should equal to 60 MS/MS events, but the scan rate is actually a little bit slow than 20Hz
        // so.... I acquire a MS1 every 50 MS/MS events.
        private int _placeALowResMS1 = 20;

        private double _ms1low = 0d;
        private double _ms1high = 0d;
        
        
        //private int _ms1_scanid = 111; // ms1 and ms2
        //private int _ms2_scanid = 222;
        //private int _dia_scanid = 333;
        //private int _sim_scanid = 444;

        private Parameter _parameter;

        //check consecutive 
        /// <summary>
        /// Key is the index of DIA window
        /// </summary>
        Dictionary<double, Dictionary<int, Queue<bool>>> _consecutive = new Dictionary<double, Dictionary<int, Queue<bool>>>();

        public DIADA(string rawfile, Parameter parameter)
        {
            _parameter = parameter;
            List<double> binBundaries = CalculateCnadicate();
            for(int i =0; i < binBundaries.Count-1 ;  i++)
            {
                Bin b = new Bin();
                b.lowerBound = binBundaries[i];
                b.upperBound = binBundaries[i + 1];
                b.ScanType = Types.ScanType.sDIA;
                _diaWindows.Add(b);
                // consective scans
                //_consecutive.Add(_diaWindows.Count - 1, new Queue<int>());
            }
            // set up fragment tolerance 
            _reporterIons = _parameter.ReportIons;
            _reporterIons.Sort();
            foreach (double mz in _reporterIons)
            {
                if (_parameter.fragToleranceUnit == Parameter.ToleranceUnit.Da)
                    _tolerances.Add(_parameter.FragmentTolerance);
                else
                    _tolerances.Add(Function.PPMToDa(mz, _parameter.FragmentTolerance));
            }
            _ms2helfWinSize = double.Parse(_parameter.MStwoSetting.IsolationWindowWidth) / 2;
            _simhelfWinSize = double.Parse(_parameter.sDIASetting.IsolationWindowWidth) / 2 + _simeoffset;
            _ms1low = double.Parse(_parameter.MSoneSetting.IsolationLow);
            _ms1high = double.Parse(_parameter.MSoneSetting.IsolationHigh);
            //write log file 
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".log";
            _log = new StreamWriter(filepath);
            WriteLogHeader(filepath);

            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            string hardklorlog = Path.GetDirectoryName(filepath)+@"/"+Path.GetFileNameWithoutExtension(filepath)+".rhk";
            //_hardklor.SetUpLogFilename(hardklorlog);
            string templog = Path.GetDirectoryName(filepath) + @"/" + Path.GetFileNameWithoutExtension(filepath) + ".tmp";
            //_templog = new StreamWriter(templog);
            
        }
        
        private void WriteLogHeader(string filepath)
        {


            AddMessage("# DIADA method v0.1");
            AddMessage("# S\tscan number");
            AddMessage("# T\tDIA or SIM");
            AddMessage("# R\t if the type is DIA, it will show \"no match\" or the m/z value of matched ion; " +
                           "\tif the scan type is SIM, this line will show selected peptide ions");
            AddMessage("# END");
            AddMessage("#========================================");
            
        }


        private static readonly object _syncObject = new object();
        private void AddMessage(string message)
        {
            
            lock(_syncObject)
            {
                _log.WriteLine(message);
                _log.Flush();
            }

        }


        private List<double> CalculateCnadicate()
        {
            //_parameter.startMZ = double.Parse(_parameter.DIASetting.FirstMass);
            //_parameter.endMZ = double.Parse(_parameter.DIASetting.LastMass);
            _parameter.windowWidth = double.Parse(_parameter.sDIASetting.IsolationWindowWidth);
            return _parameter.mzBundaries;
        }
        

        //public int NextCustomScanRunningNumber {get { return _ms1_scanid; } }
        //public int PeriodicScanRunningNumber { get { return 1; } }


        /// </summary>
        /// keep a list of peptide ions detected in recent MS1 scan
        /// <param name="scan"></param>
        public void DoAnalysis(IMsScan scan)
        {
            if (!_watch.IsRunning)
                _watch.Start();

            string accID = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out accID, ref Source);
            MsScanInformationSource common = MsScanInformationSource.Common;
            string sN = "";
            scan.CommonInformation.TryGetValue("Scan", out sN, ref common);
            
            object mass;
            scan.CommonInformation.TryGetRawValue("Masses", out mass, ref common);
            double[] range = mass as Double[];

            AddMessage("S\t"+sN);

            // temp


            //_templog.WriteLine("\n=SCAN\t" + sN);
            if (accID == ((int)Types.ScanType.sDIA).ToString())
            {
                double low = range[0] - _simhelfWinSize + _simeoffset;
                double high = range[0] + _simhelfWinSize - _simeoffset;
                AddMessage("T\tDIA\t[" + low + " - " + high + "]\n");
                //_templog.WriteLine("=T\tDIA");    
                DetectReprotIon(scan);
                
            }
            else if (accID == ((int)Types.ScanType.SIM).ToString())
            {
                double low = range[0] - _simhelfWinSize;
                double high = range[0] + _simhelfWinSize;
                AddMessage("T\tSIM\t[" + low + " - " + high + "]\n");
                AnalyzeSIMScan(scan);

            }
            else
            {
                //_templog.WriteLine("=T\tOTHERS");
            }

        }
        
        // 

        private void AnalyzeSIMScan(IMsScan simScan)
        {
            //run hardklor
            SpectrumWrapper spectrum = new SpectrumWrapper();
            foreach (ICentroid peak in simScan.Centroids)
                spectrum.Add(peak.Mz, (float)peak.Intensity);
            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            // assign new peptide list

            peptides.Sort(new PepMatchIntensityComparer());
            SortedList<double, double> analyzedregion = new SortedList<double, double>();
            int addedCount = 0;

            // get boudaries
            MsScanInformationSource common = MsScanInformationSource.Common;
            object mass;
            string sN;
            simScan.CommonInformation.TryGetRawValue("Masses", out mass, ref common);
            simScan.CommonInformation.TryGetValue("Scan", out sN, ref common);
            int simScanNo = int.Parse(sN);
            double[] range = mass as Double[];
            double high = range[0] + _simhelfWinSize - _simeoffset;
            double low = range[0] - _simhelfWinSize + _simeoffset;

            for (int idx = peptides.Count-1; idx >= 0; idx--)
            {
                PeptideMatch pep = peptides[idx];
                
                if (pep.charge < _parameter.minCharge || pep.charge > _parameter.maxCharge)
                    continue;

                double mz = (pep.monoMass + pep.charge * Molecules.Proton) / pep.charge;

                if (mz > high || mz < low)
                    continue;

                if (pep.intensity < _parameter.IntensityThreshold)
                {
                    AddMessage("R\t" + mz.ToString() + "\t" + pep.intensity + "\t" + pep.charge + "\tThe intensity is too low, skip");
                    continue;
                }

                bool analyzed = false;
                for (int i = 0; i < analyzedregion.Keys.Count; i++)
                {
                    double lmz = analyzedregion.Keys[i];
                    if (mz > analyzedregion[lmz])
                        continue;
                    else if (mz >= lmz && mz <= analyzedregion[lmz])
                    {
                        analyzed = true;
                        AddMessage("R\t" + mz.ToString() + "\t" + pep.intensity + "\tThis peak has been analyzed with isoaltion window [" + lmz + "-" + analyzedregion[lmz] + "]");
                        break;
                    }
                    else
                        break;
                }
             
                if (!analyzed)
                {
                    Bin ms2 = new Bin();
                    ms2.lowerBound = mz - _ms2helfWinSize;
                    ms2.upperBound = mz + _ms2helfWinSize;
                    ms2.ScanType = Types.ScanType.RegularMStwo;
                    ms2.parentScan = simScanNo;
                    ms2.targetCharge = pep.charge;
                    AddMessage("R\t" + mz.ToString() + "\t" + pep.intensity + "\t" + ms2.targetCharge + "\t[" + ms2.lowerBound + "-" + ms2.upperBound + "]");
                    lock (_ms2candidates)
                    {
                        _ms2candidates.Enqueue(ms2);
                        addedCount++;
                        analyzedregion.Add(ms2.lowerBound, ms2.upperBound);
                        //_log.WriteLine("analyzed isolation window count: "+analyzedregion.Keys.Count);
                    }
                }
            }
            AddMessage("N\t" + addedCount.ToString() + " peptides");
            AddMessage("END");

            
        }


        // add consecutive scan setting --
        
        private void DetectReprotIon(IMsScan diaScan)
        {
            // set up data structure for consecutive scan
            MsScanInformationSource common = MsScanInformationSource.Common;
            object mass;
            diaScan.CommonInformation.TryGetRawValue("Masses", out mass, ref common);
            double[] range = mass as Double[];
            if (!_consecutive.ContainsKey(range[0]))
                _consecutive.Add(range[0], new Dictionary<int, Queue<bool>>());
            

            // write logs
            bool acquireSIM = true;
            bool onematch = false;
            bool allmatch = true;

            //double detectedMz = 0d;
            List<ICentroid> detectedPeaks = new List<ICentroid>();
            List<ICentroid> peaks = diaScan.Centroids.ToList();
            List<int> matchedReportIdx = new List<int>();
            peaks.Sort(new PepMZComparer());

            


            //ICentroid  = null;
            int peakIdx = 0;
            for (int ridx = 0; ridx < _reporterIons.Count; ridx++)
            {
                bool matched = false;
                if (!_consecutive[range[0]].ContainsKey(ridx))
                    _consecutive[range[0]].Add(ridx, new Queue<bool>());

                for (int idx = peakIdx; idx < peaks.Count; idx++)
                {
                    ICentroid peak = peaks[idx];
                    double dif = Math.Abs(peak.Mz - _reporterIons[ridx]);
                    if (dif <= _tolerances[ridx])
                    {
                        //_templog.WriteLine("Within tolerance--\tReporter ion intensity: " + peak.Intensity.ToString() + "\tint threshold:" + _parameter.FragmentIntThreshold);
                    }
                    if (dif <= _tolerances[ridx] && peak.Intensity > _parameter.FragmentIntThreshold)
                    {
                        //acquireSIM = true;
                        matched = true;
                        //matchedIdx = ridx;
                        matchedReportIdx.Add(ridx);
                        peakIdx = idx+1;
                        detectedPeaks.Add(peak);
                        break;
                    }
                    else if (peak.Mz > _reporterIons[ridx])
                    {
                        peakIdx = idx;
                        break;
                    }
                }

                //added on 04/21/15'
                //remove the old records we do not need
                //_templog.WriteLine("REGION: " + range[0]+"\tDETECT: "+matched.ToString());
                ////_templog.WriteLine("=DETECT\t"+matched.ToString());
                _consecutive[range[0]][ridx].Enqueue(matched);
                
                

                while (_consecutive[range[0]][ridx].Count > _parameter.ReporterIonInNscans)
                    _consecutive[range[0]][ridx].Dequeue();
                //_templog.WriteLine("RECORD#: " + _consecutive[range[0]][ridx].Count.ToString());

                // check all the records in queue 
                bool acquire = true;
                foreach (bool ans in _consecutive[range[0]][ridx])
                {
                    //_templog.WriteLine("\t= "+ans.ToString());
                    if (!ans)
                    {
                        acquire = false;
                        //break;
                    }
                }

                //break from the reporter ion loop
                if (_parameter.MatchAllRepoeters && acquire == false)
                {
                    allmatch = false;
                    break;
                }
                if (!_parameter.MatchAllRepoeters && acquire == true)
                {
                    onematch = true;
                    break;
                }
            }

            //
            if (_parameter.MatchAllRepoeters && allmatch)
                acquireSIM = true;
            else if (!_parameter.MatchAllRepoeters && onematch)
                acquireSIM = true;
            else
                acquireSIM = false;

            //_templog.WriteLine("=ACQUIRE\t" + acquireSIM.ToString());
            if (acquireSIM) // 
            {

                double high = range[0] + _simhelfWinSize;
                double low = range[0] - _simhelfWinSize;
                
                Bin sim = new Bin();
                sim.lowerBound = low ;
                sim.upperBound = high;
                sim.ScanType = Types.ScanType.SIM;
                lock (_simScans)
                {
                    _simScans.Enqueue(sim);
                }
                for (int idx = 0; idx <detectedPeaks.Count; idx++)
                {
                    ICentroid peak = detectedPeaks[idx];
                    AddMessage("R\t" + peak.Mz.ToString() + "\t" + peak.Intensity + "\t" + _reporterIons[matchedReportIdx[idx]].ToString());
                }
            }
            else
                AddMessage("R\tNo match");
            AddMessage("END");
        }


        // this is the working version before I adding consective scans on 04/21
        //private void DetectReprotIon(IMsScan diaScan)
        //{
        //    // write logs
        //    bool acquireSIM = false;
        //    //double detectedMz = 0d;
        //    List<ICentroid> detectedPeaks = new List<ICentroid>();
        //    List<ICentroid> peaks = diaScan.Centroids.ToList();
        //    List<int> matchedReportIdx = new List<int>();
        //    peaks.Sort(new PepMZComparer());

        //    //ICentroid  = null;
        //    int peakIdx = 0;
        //    for (int ridx = 0; ridx < _reporterIons.Count; ridx++)
        //    {
        //        for (int idx = peakIdx; idx < peaks.Count; idx++)
        //        {
        //            ICentroid peak = peaks[idx];
        //            double dif = Math.Abs(peak.Mz - _reporterIons[0]);
        //            if (dif <= _tolerances[0] && peak.Intensity > _parameter.FragmentIntThreshold)
        //            {
        //                acquireSIM = true;
        //                //matchedIdx = ridx;
        //                matchedReportIdx.Add(ridx);
        //                peakIdx = idx + 1;
        //                detectedPeaks.Add(peak);
        //                break;
        //            }
        //            else if (peak.Mz > _reporterIons[ridx])
        //            {
        //                peakIdx = idx;
        //                break;
        //            }
        //        }
        //    }

        //    if (acquireSIM) // 
        //    {
        //        MsScanInformationSource common = MsScanInformationSource.Common;
        //        object mass;
        //        diaScan.CommonInformation.TryGetRawValue("Masses", out mass, ref common);
        //        double[] range = mass as Double[];

        //        //diaScan.CommonInformation.TryGetValue("HighMass", out highMass, ref common);
        //        //diaScan.CommonInformation.TryGetValue("LowMass", out lowMass, ref common);

        //        double high = range[0] + _simhelfWinSize;
        //        double low = range[0] - _simhelfWinSize;

        //        Bin sim = new Bin();
        //        sim.lowerBound = low;
        //        sim.upperBound = high;
        //        sim.ScanType = Types.ScanType.SIM;
        //        lock (_simScans)
        //        {
        //            _simScans.Enqueue(sim);
        //        }
        //        for (int idx = 0; idx < detectedPeaks.Count; idx++)
        //        {
        //            ICentroid peak = detectedPeaks[idx];
        //            _log.WriteLine("R\t" + peak.Mz.ToString() + "\t" + peak.Intensity + "\t" + _reporterIons[matchedReportIdx[idx]].ToString());
        //        }
        //    }
        //    else
        //        _log.WriteLine("R\tNo match");
        //    _log.WriteLine("END");
        //}

        public bool QualifyForAnalysis(int accessid)
        {
            // DIA = 333
            // SIM = 444
            if (accessid == (int)Types.ScanType.sDIA || accessid == (int)Types.ScanType.SIM)
                return true;
            else
                return false;
        }

        object addcount = new object();
        private object dequeuelock = new object();
        public Bin NextIsolationBin
        {
            get
            {
                Bin nextBin = null;

                if (_scanCount >= _placeALowResMS1+1)
                {
                    lock (addcount)
                    {
                        _scanCount = 0;
                    }
                    nextBin = new Bin();
                    nextBin.ScanType = Types.ScanType.RegularMSone;
                    nextBin.lowerBound = _ms1low;
                    nextBin.upperBound = _ms1high;
                }
                lock (dequeuelock)
                {
                    if (_simScans.Count > 0)
                        nextBin = _simScans.Dequeue();
                    else if (_ms2candidates.Count > 0)
                        nextBin = _ms2candidates.Dequeue();
                }
                if (nextBin == null)
                {
                    if (_currentDIAindex >= _diaWindows.Count)
                        _currentDIAindex = 0;
                    nextBin = _diaWindows[_currentDIAindex];
                    _currentDIAindex++;
                }
                lock (addcount)
                {
                    _scanCount++;
                }
                return nextBin;
            }
        }


        private int _scanCount = 0;
        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        //public Types.ScanType NextScanType 
        //{ 
        //    get 
        //    {
        //        if (_scanCount >= _placeALowResMS1)
        //        {
        //            _scanCount = 0;
        //            return Types.ScanType.RegularMSone;
        //        }
        //        lock (_ms2candidates)
        //        {
        //            if (_ms2candidates.Count > 0)
        //            {
        //                return Types.ScanType.RegularMStwo;
        //            }
        //        }
        //        lock (_simScans)
        //        {
        //            if (_simScans.Count > 0)
        //            {
        //                return Types.ScanType.SIM;
        //            }
        //        }
        //        // DIA scans
        //        return Types.ScanType.DIA;
        //        //return NextCustomScan.ScanType.CustomMStwo;
        //    } 
        //}



        public void Close()
        {
            lock (_syncObject)
            {
                if (_log != null)
                    _log.Close();
                _hardklor.CloseLogFile();
                //_templog.Close();
            }
        }


        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            return true;
        }

        public bool DoneAnalysis
        {
            get { return true; }
        }

    }
}

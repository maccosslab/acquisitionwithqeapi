﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using mHardklor_2;


namespace AcquisitionMethod
{
    public class FeatureVoting : IAcuqisition
    {
        private ScanAnalyzer _hardklor ;
        private Queue<Bin> _currentBins = new Queue<Bin>();
        private Queue<Bin> _priorityBins = new Queue<Bin>();
        private readonly Bin _msOne;

        private readonly Parameter _parameter;
        
        private readonly int _noMoreThanThisNumber;
        private readonly int _placeAMSoneWhenItCountsToThis;
        private readonly int _placeAMSoneIfCandidatesLessThanThis = 10;
        private double _lowerMz = 0d;
        //private int _custom_runningNumber = 999;
        //private int _repeat_runningNumber = 101;
        //private int _nextAnalysisID = 1000;
        private bool _readyForNextMS1 = false;
        private int _MS2Count = 0;
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        private StreamWriter _log = null;
        private StreamWriter _peakNumlog = null;

        private int _reocrdNo;
        //private double _peakTolerance = 0.01d;
        private double _widthTolerance = 0.02d;
        private Queue<Record> _binRecords = new Queue<Record>();
        

        private string _hardklorlogfilename = "";
        private double _targetIonCount = 0d;
        private double _maxITsec = 0d;
        
        public FeatureVoting(Parameter parameter)
        {
            _parameter = parameter;
            _reocrdNo = _parameter.PeptideInNMSone;
            _noMoreThanThisNumber = _parameter.UpdateFrequency; // ~2.x second on QE+
            _placeAMSoneWhenItCountsToThis = _parameter.PlaceAMSoneAfterNMS2; //20 ~1 second on QE+
            _targetIonCount = _parameter.UnderFillRatio * double.Parse(_parameter.MStwoSetting.AGC_Target);
            _maxITsec = double.Parse(_parameter.MStwoSetting.MaxIT)/1000d;


            double res = double.Parse(parameter.MSoneSetting.Resolution);
            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge,res);
            _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
            
            if (_hardklorlogfilename != "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);

            _msOne = new Bin();
            _msOne.upperBound = double.Parse(_parameter.MSoneSetting.IsolationHigh);
            _msOne.lowerBound = double.Parse(_parameter.MSoneSetting.IsolationLow);
            _msOne.ScanType =  Types.ScanType.RegularMSone;
        }
        public FeatureVoting(string rawfile, Parameter parameter) : this(parameter)
        {
            
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".log";
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.4 FeatureVoting method.");
            _log.WriteLine("# ");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + @"\" + rawfile + ".rhk";
            // set up hardklor analysis log


            _peakNumlog = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".np");
            _peakNumlog.WriteLine("# Peak Number log");
            _peakNumlog.WriteLine("#S Scan AccessID");
            _peakNumlog.WriteLine("#");
            _peakNumlog.WriteLine("#----------------------------------------");
        }

        public int CheckPersistentInNScans
        {
            get { return _reocrdNo; }
            set { _reocrdNo = value; }
        }

        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);

            // have to analyze the whole scan
            foreach (ICentroid peak in scan.Centroids)
                spectrum.Add(peak.Mz, (float) peak.Intensity);

            if (_log != null)
                _log.WriteLine("S\t" + number);

            // this is different from previous versions, 
            // In this version, I have to record the isotope envelopes from previous scans not bin results
            //PeptideRecord record = new PeptideRecord(peptideIons);


            BinsResult binIdx = _hardklor.Analyze(spectrum);
            //FindCommonFeature
            AssignBins(binIdx);

            if (_log != null)
                _log.Flush();
        }

        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            BinsResult binIdx = _hardklor.Analyze(spectrum);
            AssignBins(binIdx);
            return true;
        }


        private void AssignBins(BinsResult binIdx)
        {
            _readyForNextMS1 = false;

            if (_binRecords.Count >= _reocrdNo)
                _binRecords.Dequeue();

            //List<int> binIndex = binIdx.QualifiedBins().Keys.ToList();
            Record r = new Record(binIdx);
            _binRecords.Enqueue(r);
            List<Bin> commonBins = FindCommonFeature(_binRecords.ToList());
            //Console.WriteLine("# bin: "+commonBins.Count);
            int bincount = 0;
            Bin b = null;
            for (int i=0;i<commonBins.Count;i++)
            {
                b = commonBins[i];
                b.ScanType = Types.ScanType.RegularMStwo;
                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString() + "\t" + b.targetCharge.ToString());

                bincount++;
                // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }
            // this means the number of candidates from the selected region is less than what we want, so we should reset
            // the lower boundary to 0 
            if (bincount < _noMoreThanThisNumber)
                _lowerMz = 0d;
            else
                _lowerMz = b.upperBound; // this should be the last bin in the queue
            // the bin won't be null, otherwise it will not get into here
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());
            if (commonBins.Count>0)
                Console.WriteLine("Added Bin: " +bincount);
            _readyForNextMS1 = true;
        }

        private List<Bin> FindCommonFeature(List<Record> records)
        //private List<int> FindCommonFeature(List<Record> records)
        {
            

            Dictionary<int, List<PersistPep>> commonDic = new Dictionary<int, List<PersistPep>>();
            foreach (int idx in records[0].IndexList)
            {
                double checkMZ = _parameter.mzBundaries[idx];
                if (checkMZ < _lowerMz)
                    continue;
                commonDic.Add(idx, new List<PersistPep>());
                foreach (Ion ion in records[0].BinIdx.QualifiedIons(idx))
                    commonDic[idx].Add(new PersistPep(ion));
            }
            int startIdx = 1;
            //Console.WriteLine("After first ruound: "+commonDic.Keys.Count);
            while (startIdx < records.Count)
            {
                commonDic = FindCommon(commonDic, records[startIdx]);
                startIdx++;
            }

            List<int> commonIdx = commonDic.Keys.ToList();
            commonIdx.Sort();
            

            // filter by intensity
            List<Bin> commonBins = new List<Bin>();
            Bin b;
            foreach (int idx in commonIdx)
            {
                Ion intensOne = new Ion();
                foreach (PersistPep pep in commonDic[idx])
                {
                    if (pep.LastIon.Intensity > intensOne.Intensity)
                        intensOne = pep.LastIon;
                }
                
                //double predictIon = intensOne.Intensity * double.Parse(_parameter.MStwoSetting.MaxIT);
                double predictIon = intensOne.Intensity * _maxITsec;
                if (_peakNumlog != null)
                    _peakNumlog.Write(_parameter.mzBundaries[idx].ToString() + "-" + _parameter.mzBundaries[idx + 1].ToString() + "\t" + intensOne.Mz + "\t" + intensOne.Intensity + "\t" + predictIon);
                //Console.WriteLine("Intensity: " + intensOne.Intensity + "\tms: " + _maxITsec + "\tPredicted: " + predictIon + "\tTarget: " + _targetIonCount);
                if (predictIon < _targetIonCount)
                {
                    if (_peakNumlog != null)
                        _peakNumlog.WriteLine("\tThe intensity is too low, skip this peak");
                    continue;
                }

                b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];
                b.targetCharge = intensOne.Charge;
                commonBins.Add(b);
                if (_peakNumlog != null)
                    _peakNumlog.WriteLine("\tAdd to candidate list");
            }
            if (_peakNumlog != null)
                _peakNumlog.WriteLine("# Qualified bin: " + commonBins.Count.ToString());

            if (commonBins.Count > 0)
                Console.WriteLine("common index : " + commonIdx.Count+"\tCommon bin: "+commonBins.Count+"\t");
            return commonBins;

        }


        private Dictionary<int, List<PersistPep>> FindCommon(Dictionary<int, List<PersistPep>> common, Record currentRecord)
        {
            Dictionary<int, List<PersistPep>> result = new Dictionary<int, List<PersistPep>>();
            //foreach ()
            foreach (int idx in currentRecord.IndexList)
            {
                if (common.ContainsKey(idx))
                {
                    // I think it's only two to three ions in a bin so... I could just use double loops.
                    foreach (PersistPep pep in common[idx])
                    {
                        Ion closetOne = null;
                        double minDif = double.MaxValue;
                        
                        foreach (Ion ion in currentRecord.BinIdx.QualifiedIons(idx))
                        {
                            bool pass = true;
                            if (pep.Charge != ion.Charge)
                                continue;
                            else
                            {
                                double dif = 0d;
                                foreach (Ion pion in pep.PepIons())
                                {
                                    double error = Math.Round(pion.Mz - ion.Mz);
                                    if (error <= _widthTolerance)
                                        dif += error;
                                    else
                                    {
                                        pass = false;
                                        break;
                                    }
                                }
                                if (pass)
                                {
                                    if (dif < minDif)
                                        closetOne = ion;
                                }
                            }
                        }
                        if (closetOne != null)
                        {
                            pep.AddIon(closetOne);
                            if (!result.ContainsKey(idx))
                                result.Add(idx,new List<PersistPep>());
                            result[idx].Add(pep);
                        }
                    }
                }
            }
            return result;
        }




        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }

        

        public Bin NextIsolationBin
        {
            get
            {
                Bin nextone = null;

                lock (lockDequeue)
                {
                    if (_priorityBins.Count > 0)
                        nextone = _priorityBins.Dequeue();
                    else if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }



        public int CandidateCount
        {
            get { return _currentBins.Count; }
        }

        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public Types.ScanType NextScanType
        {
            get
            {
                // we need one more MS one if the candidates is less than 10 after analyzing
                if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis || _MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    //_needAMSoneNow = false;
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        _priorityBins.Enqueue(_msOne);
                        return Types.ScanType.RegularMSone;
                    }
                }
                if (CandidateCount > 0)
                    return Types.ScanType.RegularMStwo;
                else
                {
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        _priorityBins.Enqueue(_msOne);

                        return Types.ScanType.RegularMSone;
                    }
                    else
                        return Types.ScanType.None; // waiting for the result
                }
            }
        }

        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == (int)_msOne.ScanType)
                return true;
            else
                return false;
        }


        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            _hardklor.CloseLogFile();
        }

    }





    


}


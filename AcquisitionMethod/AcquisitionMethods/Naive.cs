﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;

using mHardklor_2;

using System.IO;
//using System.Diagnostics;
 
namespace AcquisitionMethod
{
    /// <summary>
    /// This is the simplest method. very straightforward, i am not going to explain it 
    /// figure it out by yourself
    /// </summary>
    public class Naive : IAcuqisition
    {
        private ScanAnalyzer _hardklor ;
        private Queue<Bin> _currentBins = new Queue<Bin>();
        //private bool _doAnalysisOnNextScan = false;
        private bool _readyForNextMS1 = false;
        private int _MS2Count = 0;
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _placeAMSoneWhenItCountsToThis = 20;
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        private readonly bool _reverseOrder = false;

        private StreamWriter _log = null;
        //private Stopwatch _timer = new Stopwatch();
        private string _hardklorlogfilename = "";
        private Parameter _parameter;
        public Naive(Parameter parameter)
        {
            _parameter = parameter;
            

            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
            if (_hardklorlogfilename != "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);
            
        }

        public Naive(string rawfile, Parameter parameter) : this(rawfile, false, parameter) {}

        public Naive(string rawfile, bool reverseOrder, Parameter parameter) : this (parameter)
        {
            

            _reverseOrder = reverseOrder;
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.1 Naive method.");
            _log.WriteLine("# only update candidate list when previous are all done.");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            
            //_timer.Start();
        }
        public Bin NextMSone { get { return new Bin(); } }


        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid > 999)
                return true;
            else
                return false;
        }
        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            return false;
        }
        
        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);


            SpectrumWrapper spectrum = new SpectrumWrapper();
            //int peakNo = 0;
            foreach (ICentroid peak in scan.Centroids)
            {
                spectrum.Add(peak.Mz, (float)peak.Intensity);
                //peakNo++;
            }
            // hardklor log
            if (_log != null)
                _log.WriteLine("S\t" + number);

            // do hardklor here
            //DoAnalysis(spectrum);
            BinsResult binIdx = _hardklor.Analyze(spectrum);
            AssignBins(binIdx);
            
            if (_log != null)
                _log.Flush();
        }

        //public void DoAnalysis(SpectrumWrapper spectrum)
        //{
        //    BinsResult binIdx = _hardklor.Analyze(spectrum);
        //    AssignBins(binIdx);
        //}

        private void AssignBins(BinsResult binIdx)
        {
            List<int> keys = binIdx.QualifiedBins().Keys.ToList<int>();
            keys.Sort();
            
            if (_reverseOrder)
                keys.Reverse();

            if (_log != null)
                _log.WriteLine("N\t"+keys.Count.ToString());
            //_hardklorAnalysis.WriteLine(keys.Count.ToString());
            _currentBins = new Queue<Bin>();
            // foreach (int idx in binIdx.QualifiedBins().Keys)
            foreach (int idx in keys)
            {
                Bin b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];
                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b);
                }
                //_candidateBins.Enqueue(b);
                // hardklor record
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());
            }
            if (CandidateCount == 0)
                _readyForNextMS1 = true;
            
        }
        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }

        /// <summary>
        /// I do not lock this part, I think it is fine, just be careful about multi-threading
        /// </summary>
        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }

        /// <summary>
        /// this will return null object if the queue is empty
        /// </summary>
        public Bin NextCandidate 
        {
            get
            {
                Bin nextone = null;
                lock (lockDequeue)
                {
                    if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                        //_checking++;
                    }
                }

                //else
                //{
                //    //_doAnalysisOnNextScan = true;
                //    _readyForNextMS1 = true;
                //}
                return nextone;
            }
        }

        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber; 
            }
        }
        public int PeriodicScanRunningNumber
        {
            get { return _repeat_runningNumber; }
        }
        
 
        public NextCustomScan.ScanType NextScanType
        {
            get
            {
                // if we still have some candidates in the queue, we should finish it all before next MS1
                if (CandidateCount == 0)
                {
                    if (_readyForNextMS1)
                    {
                        _MS2Count = 0;
                        _readyForNextMS1 = false;
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    //_checking = 0;
                    return NextCustomScan.ScanType.None;
                }
                else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    _MS2Count = 0;
                    return NextCustomScan.ScanType.PeriodicMSone;
                }
                _readyForNextMS1 = true;
                return NextCustomScan.ScanType.CustomMStwo;

                #region working
                //if (_candidateBins.Count == 0)
                //{
                //    if (_readyForNextMS1)
                //    {
                //        ReportTime(_timer.Elapsed);
                //        SetMSOneScan();
                //        _readyForNextMS1 = false;
                //        _MS2Count = 0;
                //        return;
                //    }
                //    AddingMessage("Placed " + _checking.ToString() + " MS2 scans in queue\n");
                //    _checking = 0;
                //    return;
                //}
                //else if (_MS2Count == 10)
                //{
                //    SetPeriodicMSOne();
                //    _MS2Count = 0;
                //    return;
                //}
                //selectedBin = _candidateBins.Dequeue();
                //_MS2Count++;
                //_checking++;
                //_readyForNextMS1 = true;

                #endregion
            }
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            _hardklor.CloseLogFile();
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;

// ReSharper disable RedundantUsingDirective
using mHardklor_2;
// ReSharper restore RedundantUsingDirective


namespace AcquisitionMethod
{

    /// <summary>
    /// we analyzed every MS1 scan we acquired, used three or five latest MS1 scans to update the candidate list.
    /// 
    /// </summary>
    public class BinVoting : IAcuqisition
    {
        private ScanAnalyzer _hardklor ;
        private Queue<Bin> _currentBins = new Queue<Bin>();
        private StreamWriter _log = null;

        private double _lowerMz = 0d;
        private int _noMoreThanThisNumber ;
        private int _placeAMSoneWhenItCountsToThis;
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _nextAnalysisID = 1000;
        private int _placeAMSoneIfCandidatesLessThanThis = 10;

        bool _readyForNextMS1 = false;
        int _MS2Count = 0;
        
        private object lockDequeue = new object();
        private object lockEnqueue = new object();

        // how many previous scans you want keep record

        private int _recordNo;
        private Queue<Record> _records = new Queue<Record>();
        private Parameter _parameter;
        private string _hardklorlogfilename = "";

        public BinVoting(Parameter parameter)
        {
            
            _parameter = parameter;
            _recordNo = _parameter.PeptideInNMSone;
            
            _noMoreThanThisNumber = _parameter.UpDataFrequency; // ~2.x second on QE+
            _placeAMSoneWhenItCountsToThis = _parameter.PlaceAMSoneAfterNMS2; //20 ~1 second on QE+

            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
            if (_hardklorlogfilename != "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);
        }
        public Bin NextMSone { get { return new Bin(); } }
        public BinVoting(string rawfile,Parameter parameter) : this( parameter)
        {
            
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.3 BinVoting method.");
            _log.WriteLine("# ");
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            
        }

        public int CheckPersistentInNScans
        {
            get { return _recordNo;}
            set { _recordNo = value; }
        }

        
        

        //private Dictionary<int, List<int>> _records = new Dictionary<int, List<int>>();


        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);

            foreach (ICentroid peak in scan.Centroids)
            {
                spectrum.Add(peak.Mz, (float)peak.Intensity);
            }

            if (_log != null)
                _log.WriteLine("S\t" + number);

            //DoAnalysis(spectrum);
            BinsResult binIdx = _hardklor.Analyze(spectrum);
            AssignBins(binIdx);

            if (_log != null)
                _log.Flush();
        }



        /// <summary>
        /// this is for testing 
        /// </summary>
        /// <param name="spectrum"></param>
        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            BinsResult binIdx = _hardklor.Analyze(spectrum);
            AssignBins(binIdx);
            return true;
        }

        
        private void AssignBins(BinsResult binIdx)
        {
            _readyForNextMS1 = false;
            
            //List<int> indexList = binIdx.QualifiedBins().Keys.ToList();
            Record record = new Record(binIdx);
            if (_records.Count >= _recordNo)
                _records.Dequeue();
            _records.Enqueue(record);


            //
            List<int> commonIdx = FindCommonBins(_records.ToList());
            //List<Record> recs = _records.ToList();
            //List<int> commonIdx = recs[0].IndexList;
            //
            int bincount = 0;
            Bin b = null;

            foreach (int idx in commonIdx)
            {

                // m/z checking has been movded to FindCommonBins function. 
                //double checkMZ = _parameter.mzBundaries[idx];
                //if (checkMZ < _lowerMz)
                //    continue;

                b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];

                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString());

                bincount++; // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }

            if (bincount < _noMoreThanThisNumber)
                _lowerMz = 0d;
            else
                _lowerMz = b.upperBound; // the bin won't be null, otherwise it will not get into here

            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

            _readyForNextMS1 = true;

        }


        // this probably will take many miniseconds.... hmmmm ... 
        private List<int> FindCommonBins(List<Record> records)
        {
            List<int> commonIdx = new List<int>();

            if (records.Count == 1)
            {
                foreach (int idx in records[0].IndexList)
                {
                    // checking mz
                    double checkMZ = _parameter.mzBundaries[idx];
                    if (checkMZ < _lowerMz)
                        continue;
                    commonIdx.Add(idx);
                }
            }
            else
            {
                Dictionary<int, int> commonDic = new Dictionary<int, int>();
                foreach (int idx in records[0].IndexList)
                {
                    // checking mz
                    double checkMZ = _parameter.mzBundaries[idx];
                    if (checkMZ < _lowerMz)
                        continue;


                    commonDic.Add(idx, 1);
                }

                int startIdx = 1;

                while (startIdx < records.Count)
                {
                    List<int> idxlist = records[startIdx].IndexList;
                    commonDic = FindCommon(commonDic, idxlist);
                    startIdx++;
                }
                commonIdx = commonDic.Keys.ToList();
                commonIdx.Sort();
            }
            return commonIdx;
        }


        private Dictionary<int, int> FindCommon(Dictionary<int, int> common, List<int> idxlist)
        {
            Dictionary<int,int> result = new Dictionary<int, int>();
            foreach (int idx in idxlist)
            {
                if (common.ContainsKey(idx))
                    result.Add(idx,common[idx]+1);
            }
            return result;
        }

        public int PeriodicScanRunningNumber
        {
            get { return _repeat_runningNumber; }
        }
        
        public Bin NextCandidate 
        {
            get
            {
                Bin nextone = null;
                // the _currentBins may be filled by another thread, 
                lock (lockDequeue)
                {
                    if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }
        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }

        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public NextCustomScan.ScanType NextScanType
        {
            get
            {
                // we need one more MS one if the candidates is less than 10 after analyzing
                if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis && _readyForNextMS1)
                {
                    //_needAMSoneNow = false;
                    ResetCustomCounting();
                    return NextCustomScan.ScanType.CustomMSone;
                }
                else if (CandidateCount == 0)
                {
                    if (_readyForNextMS1)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.None;
                }
                else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    _MS2Count = 0;
                    if (CandidateCount < _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                    //if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis)
                    {
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.PeriodicMSone;
                }
                else
                {
                    return NextCustomScan.ScanType.CustomMStwo;
                }
            }
        }


        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }

        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
            //if (_analyzed)
            //{
            //_analyzed = false;
            _nextAnalysisID = _custom_runningNumber + 1;
            //}
        }

        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber;
            }
            
        }


        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == _nextAnalysisID)
            {
                return true;
            }
            else
                return false;
        }

        public void Close()
        {
            if (_log != null)
                _log.Close();
            _hardklor.CloseLogFile();
        }


    }


    // this is just a sorting list. I should use sorting list instead
    // of using this
    //internal class BinRecord
    //{
    //    public readonly List<int> BinIndex = new List<int>();

    //    public BinRecord(List<int> indexList)
    //    {
    //        indexList.Sort();
    //        BinIndex = indexList;
    //    }
    //}
}

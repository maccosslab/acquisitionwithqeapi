﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using mHardklor_2;


namespace AcquisitionMethod
{
    public class DynamicMS : IAcuqisition
    {
        private ScanAnalyzer _hardklor ;
        private Queue<Bin> _currentBins = new Queue<Bin>();

        // 
        private Parameter _parameter;
        private double _lowerMz = 0d;
        private int _noMoreThanThisNumber;
        private int _placeAMSoneWhenItCountsToThis;
        private int _custom_runningNumber = 999;
        private int _repeat_runningNumber = 101;
        private int _nextAnalysisID = 1000;
        private bool _readyForNextMS1 = false;
        private int _MS2Count = 0;
        private object lockDequeue = new object();
        private object lockEnqueue = new object();
        private StreamWriter _log = null;
        private int _placeAMSoneIfCandidatesLessThanThis = 10;
        

        //for detecting persistent peptide feature
        //private int _reocrdNo = Parameter.GetInstance().PeptideInNMSone;
        private int _reocrdNo = 5;
        private double _widthTolerance = 0.02d;
        private Queue<Record> _binRecords = new Queue<Record>();
        private StreamWriter _peakNumlog = null;

        //for dynamic ms one
        private double _defaultMSoneStart = 0d;
        private double _defaultMSoneEnd = 0d;
        private Stopwatch _watch = null;
        private List<int> _latestFullScanBinRecord = new List<int>();
        private List<int> _templist = new List<int>();
        private int _binOffset = 0;
        private int _lastIdxForBinList = 0;
        private double _minScanSize = 100d;
        private bool _updated = false;
        private double _dynamicStart = 0d;
        private double _dynamicEnd = 0d;
        private object _lock2 = new object();
        //private int _placeAPerioticMSoneWhenCountsToThis = 40;
        //private bool _justStart = true;
        //private int _binOffsetForMinScanSize = 0;
        // for signal filter
        private double _targetIonCount = 0d;
        private string _hardklorlogfilename = "";
        public DynamicMS(Parameter parameter)
        {
            _parameter = parameter;
            
            _defaultMSoneEnd = double.Parse(_parameter.MSoneSetting.LastMass);
            _defaultMSoneStart = double.Parse(_parameter.MSoneSetting.FirstMass);
            _targetIonCount = _parameter.UnderFillRatio * double.Parse(_parameter.MStwoSetting.AGC_Target);
            _binOffset = (int)(10 / _parameter.windowWidth);
            _noMoreThanThisNumber = _parameter.UpDataFrequency; // ~2.x second on QE+
            _placeAMSoneWhenItCountsToThis = _parameter.PlaceAMSoneAfterNMS2; //20 ~1 second on QE+
            //_binOffsetForMinScanSize = (int)(_minScanSize/Parameter.GetInstance().windowWidth);
            //_lowerMz = _defaultMSoneStart;
            _hardklor = new ScanAnalyzer(parameter.mzBundaries, parameter.minCharge, parameter.maxCharge);
            _hardklor.UseMonoMz = _parameter.UseMonoMZToDecideBins;
            if (_hardklorlogfilename != "")
                _hardklor.SetUpLogFilename(_hardklorlogfilename);
        }
        public Bin NextMSone { get { return new Bin(); } }
        public DynamicMS(string rawfile, Parameter parameter) :this(parameter)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + rawfile + "_Bin.log";
            _peakNumlog = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + rawfile + ".np");
            _log = new StreamWriter(filepath);
            _log.WriteLine("# DDAp v0.56 DynamicMS method.");
            _log.WriteLine("# this is semi-dynamic... ");
            _log.WriteLine("#");
            _log.WriteLine("# Default MS one range: " + _defaultMSoneStart.ToString() + "-" + _defaultMSoneEnd.ToString());
            _log.WriteLine("#----------------------------------------");
            _log.WriteLine("#H  Header ");
            _log.WriteLine("#S	Scan AccessID");
            _log.WriteLine("#N	Number of bins");
            _log.WriteLine("#B	Lower MZ	 Upper MZ");
            _log.WriteLine("#----------------------------------------");

            _peakNumlog.WriteLine("# Peak Number log");
            _peakNumlog.WriteLine("#S Scan AccessID");
            _peakNumlog.WriteLine("#");
            _peakNumlog.WriteLine("#----------------------------------------");

            // set up hardklor analysis log
            _hardklorlogfilename = AppDomain.CurrentDomain.BaseDirectory + rawfile + ".rhk";
            
        }

        public int CheckPersistentInNScans
        {
            get { return _reocrdNo; }
            set { _reocrdNo = value; }
        }

        public void DoAnalysis(IMsScan scan)
        {
            string number = "";
            MsScanInformationSource Source = MsScanInformationSource.Trailer;
            scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
            string firstMass = "";
            MsScanInformationSource common = MsScanInformationSource.Common;
            scan.CommonInformation.TryGetValue("LowMass", out firstMass, ref common);
            string lastMass = "";
            scan.CommonInformation.TryGetValue("HighMass", out lastMass, ref common);
            
            SpectrumWrapper spectrum = new SpectrumWrapper();
            spectrum.Index = int.Parse(number);

            // have to analyze the whole scan
            foreach (ICentroid peak in scan.Centroids)
                spectrum.Add(peak.Mz, (float) peak.Intensity);

            if (_log != null)
                _log.WriteLine("S\t" + number + "\t" + firstMass + "-" + lastMass);
            if (_peakNumlog != null)
                _peakNumlog.WriteLine("S\t" + number);

            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            BinsResult binIdx = _hardklor.AssignBins(peptides);

            //FindCommonFeature
            if (int.Parse(number) != _repeat_runningNumber)
            {
                Record r = new Record(binIdx);
                r.mzStart = double.Parse(firstMass);
                r.mzEnd = double.Parse(lastMass);
                AssignBins(r);
            }
            else
                UpdateLatestFullScanAnalysis(binIdx);

            if (_log != null)
                _log.Flush();
        }

        //private object _lock = new object();
        private void UpdateLatestFullScanAnalysis(BinsResult binIdx)
        {
            List<int> index = binIdx.QualifiedBins().Keys.ToList();
            index.Sort();
            _templist = index;
            _updated = true;
        }


        public double DynamicMSoneStart
        {
            get
            {
                double mz = _defaultMSoneStart;
                if (_lowerMz > _defaultMSoneStart)
                {
                    int idx = (int)((_lowerMz - _defaultMSoneStart) / _parameter.windowWidth) - _binOffset;
                    if (idx < 0)
                        mz = _defaultMSoneStart;
                    else
                        mz = _parameter.mzBundaries[idx];
                }
                _dynamicStart = mz;
                return mz;
            }
        }
        
        public double DynamicMSoneEnd
        {
            get
            {
                lock (_lock2)
                {
                    if (_updated)
                    {
                        _latestFullScanBinRecord = _templist;
                        _lastIdxForBinList = 0;
                        _updated = false;
                    }
                    double mz = 0d;
                    
                    if (_latestFullScanBinRecord.Count == 0)
                    {
                        int idx = (int)((_minScanSize + _dynamicStart - _defaultMSoneStart) / _parameter.windowWidth) + _binOffset;
                        if (idx >= _parameter.mzBundaries.Count)
                            mz = _defaultMSoneEnd;
                        else
                            mz = _parameter.mzBundaries[idx];
                    }
                    else
                    {
                        double check = _parameter.mzBundaries[_latestFullScanBinRecord[_lastIdxForBinList]];
                        if (check < _dynamicStart)
                        {
                            for (int i = _lastIdxForBinList; i < _latestFullScanBinRecord.Count; i++)
                            {
                                if (_parameter.mzBundaries[_latestFullScanBinRecord[i]] > _dynamicStart)
                                {
                                    _lastIdxForBinList = i;
                                    break;
                                }
                            }
                        }

                        int idx = _lastIdxForBinList + _noMoreThanThisNumber;
                        if (idx >= _latestFullScanBinRecord.Count)
                        {
                            mz = _defaultMSoneEnd;
                            _lastIdxForBinList = 0;
                        }
                        else
                        {
                            int rightBundary = _latestFullScanBinRecord[idx] + (_binOffset*2); // 2 is just a random number. I just want to make sure that enough bin in the range
                            if (rightBundary >= _parameter.mzBundaries.Count)
                            {
                                mz = _defaultMSoneEnd;
                                _lastIdxForBinList = 0;
                            }
                            else
                            {
                                mz = _parameter.mzBundaries[rightBundary];
                                _lastIdxForBinList = idx;
                            }
                        }
                    }
                    _dynamicEnd = mz;
                    //_log.WriteLine("final mz end:"+mz.ToString());
                    return mz;
                }
            }
        }

        public bool TestAlgorithm(SpectrumWrapper spectrum)
        {
            List<PeptideMatch> peptides = _hardklor.DetectPepMatches(spectrum);
            BinsResult binIdx = _hardklor.AssignBins(peptides);
            //AssignBins(binIdx);
            return true;
        }

        private void AssignBins(Record r)
        {
            _readyForNextMS1 = false;

            if (_binRecords.Count >= _reocrdNo)
                _binRecords.Dequeue();
            _binRecords.Enqueue(r);

            ////BinsResult binIdx
            ////Record r = new Record(binIdx);
            

            List<Bin> commonBins = FindCommonFeature(_binRecords.ToList());


            int bincount = 0;
            Bin b = null;
            for (int i=0;i<commonBins.Count;i++)
            {
                b =commonBins[i];

                lock (lockEnqueue)
                {
                    _currentBins.Enqueue(b); // because this maybe modified by another thread, queue is not thread-safe 
                }
                if (_log != null)
                    _log.WriteLine("B\t" + b.lowerBound.ToString() + "\t" + b.upperBound.ToString()+"\t"+b.targetCharge.ToString());

                bincount++;
                // use this, do not use _currentBins count, because _currentBins may be changed by another thread
                if (bincount == _noMoreThanThisNumber)
                    break;
            }

            if (_dynamicEnd == _defaultMSoneEnd)
                _lowerMz = 0d;
            else if (b == null)
                _lowerMz = _dynamicEnd; 
            else
                _lowerMz = b.upperBound;
            
            // the bin won't be null, otherwise it will not get into here
            if (_log != null)
                _log.WriteLine("N\t" + bincount.ToString());

            //if (bincount == 0)
            _readyForNextMS1 = true;
            //_needAMSoneNow = true;
            //_analyzed = true;
        }




        private List<Bin> FindCommonFeature(List<Record> records)
        {
            List<int> commonIdx = new List<int>();
            List<Bin> commonBins = new List<Bin>();

            //the key is bin idx
            Dictionary<int, List<PersistPep>> commonDic = new Dictionary<int, List<PersistPep>>();

            foreach (int idx in records[0].IndexList)
            {
                // checking mz
                double checkMZ = _parameter.mzBundaries[idx];
                if (checkMZ < _lowerMz)
                    continue;
                commonDic.Add(idx, new List<PersistPep>());
                foreach (Ion ion in records[0].BinIdx.QualifiedIons(idx))
                    commonDic[idx].Add(new PersistPep(ion));
            }

            int startIdx = 1;
            while (startIdx < records.Count)
            {
                commonDic = FindCommon(commonDic, records[startIdx]);
                startIdx++;
            }

            commonIdx = commonDic.Keys.ToList();
            commonIdx.Sort();

            Bin b;
            foreach (int idx in commonIdx)
            {
                Ion intensOne = new Ion();
                foreach (PersistPep pep in commonDic[idx])
                {
                    if (pep.LastIon.Intensity > intensOne.Intensity)
                        intensOne = pep.LastIon;
                }

                double predictIon = intensOne.Intensity * double.Parse(_parameter.MStwoSetting.MaxIT) / 1000d;
                _peakNumlog.Write(_parameter.mzBundaries[idx].ToString() + "-" +
                                  _parameter.mzBundaries[idx + 1].ToString() + "\t" + intensOne.Mz + "\t" +
                                  intensOne.Intensity + "\t" + predictIon);
       
                if (predictIon < _targetIonCount)
                {
                    _peakNumlog.WriteLine("\tThe intensity is too low, skip this peak");
                    continue;
                }
                b = new Bin();
                b.lowerBound = _parameter.mzBundaries[idx];
                b.upperBound = _parameter.mzBundaries[idx + 1];
                b.targetCharge = intensOne.Charge;
                commonBins.Add(b);
                _peakNumlog.WriteLine("\tAdd to candidate list");
            }
            _peakNumlog.WriteLine("# Qualified bin: " + commonBins.Count.ToString());
            return commonBins;
        }

        private Dictionary<int, List<PersistPep>> FindCommon(Dictionary<int, List<PersistPep>> common, Record currentRecord)
        {
            Dictionary<int, List<PersistPep>> result = new Dictionary<int, List<PersistPep>>();
            //foreach ()
            foreach (int idx in currentRecord.IndexList)
            {
                if (common.ContainsKey(idx))
                {
                    // I think it's only two to three ions in a bin so... I could just use double loops.
                    foreach (PersistPep pep in common[idx])
                    {
                        Ion closetOne = null;
                        double minDif = double.MaxValue;
                        
                        foreach (Ion ion in currentRecord.BinIdx.QualifiedIons(idx))
                        {
                            bool pass = true;
                            if (pep.Charge != ion.Charge)
                                continue;
                            else
                            {
                                double dif = 0d;
                                foreach (Ion pion in pep.PepIons())
                                {
                                    double error = Math.Round(pion.Mz - ion.Mz);
                                    if (error <= _widthTolerance)
                                        dif += error;
                                    else
                                    {
                                        pass = false;
                                        break;
                                    }
                                }
                                if (pass)
                                {
                                    if (dif < minDif)
                                        closetOne = ion;
                                }
                            }
                        }
                        if (closetOne != null)
                        {
                            pep.AddIon(closetOne);
                            if (!result.ContainsKey(idx))
                                result.Add(idx,new List<PersistPep>());
                            result[idx].Add(pep);
                        }
                    }
                }
            }
            return result;
        }




        public bool DoneAnalysis
        {
            get { return _readyForNextMS1; }
        }

        

        public Bin NextCandidate
        {
            get
            {
                Bin nextone = null;
                // the _currentBins may be filled by another thread, but it's fine. 
                lock (lockDequeue)
                {
                    if (_currentBins.Count > 0)
                    {
                        nextone = _currentBins.Dequeue();
                        _MS2Count++;
                    }
                }
                return nextone;
            }
        }

        public int NextCustomScanRunningNumber
        {
            get
            {
                _custom_runningNumber++;
                return _custom_runningNumber;
            }
        }

        public int PeriodicScanRunningNumber
        {
            get { return _repeat_runningNumber; }
        }

        private int CandidateCount
        {
            get { return _currentBins.Count; }
        }

        private bool firstpass = true;
        private bool _acquirePerioticMSone = true;


        /// <summary>
        /// !! this should be called by one thread one time !!
        /// </summary>
        public NextCustomScan.ScanType NextScanType
        {
            get
            {
                //_count++;

                if (CandidateCount == 0)
                {
                    if (_readyForNextMS1)
                    {
                        if (_acquirePerioticMSone)
                        {
                            _acquirePerioticMSone = !_acquirePerioticMSone;
                            return NextCustomScan.ScanType.PeriodicMSone;
                        }
                        if (!firstpass)
                            _acquirePerioticMSone = true;
                        firstpass = !firstpass;
                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    return NextCustomScan.ScanType.None;
                }
                else if (CandidateCount <= _placeAMSoneIfCandidatesLessThanThis && _readyForNextMS1)
                {
            
                    if (_acquirePerioticMSone)
                    {
                        _acquirePerioticMSone = !_acquirePerioticMSone;
                        return NextCustomScan.ScanType.PeriodicMSone;
                    }
                    if (!firstpass)
                        _acquirePerioticMSone = true;
                    firstpass = !firstpass;
                    ResetCustomCounting();
                    return NextCustomScan.ScanType.CustomMSone;
                }
                else if (_MS2Count == _placeAMSoneWhenItCountsToThis)
                {
                    _MS2Count = 0;
                    if (CandidateCount < _placeAMSoneWhenItCountsToThis && _readyForNextMS1)
                    {
                        if (_acquirePerioticMSone)
                        {
                            _acquirePerioticMSone = !_acquirePerioticMSone;
                            return NextCustomScan.ScanType.PeriodicMSone;
                        }
                        if (!firstpass)
                            _acquirePerioticMSone = true;
                        firstpass = !firstpass;

                        ResetCustomCounting();
                        return NextCustomScan.ScanType.CustomMSone;
                    }
                    // I should check this again -- this actually never happened
                    return NextCustomScan.ScanType.PeriodicMSone;
                }
                else
                {
                    return NextCustomScan.ScanType.CustomMStwo;
                }
            }
        }

        public bool QualifyForAnalysis(int accessid)
        {
            if (accessid == _nextAnalysisID)
                return true;
            else if (accessid == _repeat_runningNumber)
                return true;
            else
                return false;
        }


        private void ResetCustomCounting()
        {
            _readyForNextMS1 = false;
            _MS2Count = 0;
            _nextAnalysisID = _custom_runningNumber + 1;
        }

   

        public void Close()
        {
            if (_log != null)
                _log.Close();
            if (_peakNumlog != null)
                _peakNumlog.Close();
            _hardklor.CloseLogFile();
        }

    }





    


}

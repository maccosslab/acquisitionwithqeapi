// mHardklor_2.h

#pragma once
using namespace System;
using namespace System::Collections::Generic;

namespace mHardklor_2 {

	//public enum Algorithm {Basic, Version1, Version2};
	//public enum ChargeAlgorithm {Quick, FFT, Patterson, Senko, None};
	//public enum Instrument {Orbitrap, FTICR, TOF, QIT};

	public ref class ParameterWrapper
	{
		public :
			//Algorithm algorithm;
			//ChargeAlgorithm chargeA;
			//Instrument instrument;
			double resolution;
			double mzMax;
			double mzMin;
			int chargeMax;
			int chargeMin;
			double correlation;
			int sensitivity; 
			int depth;
			int centroid;
			double mzWindow;
			int maxFeature;
			//bool noBase;

			ParameterWrapper()
			{
				resolution = 84853; // i used 42426 for R70,000, but QE+ is 120,000
				mzMax = 0;
				mzMin = 0;
				chargeMax = 5;
				chargeMin = 1;
				correlation = 0.95;
				sensitivity = 3;
				depth = 3;
				centroid = 1;
				mzWindow = 5.25;
				maxFeature = 15;
			}
			~ParameterWrapper(){}
			CHardklorSetting* GetParameter();

	private :
		CHardklorSetting* _setting; // this should be moved to parameter wrapper
	};


	public ref class SpectrumWrapper
	{
		public :
			int Index;
			float RetentionTime;
			int PeakCount;
			SpectrumWrapper()
			{
				_spec = new Spectrum();
				PeakCount = 0;
			}
			
			~SpectrumWrapper()
			{
				delete _spec;
			}
			void Add(double mz,float intensity);
			
			Spectrum* GetSpectrum();
			
		private :
			Spectrum* _spec;
			int _count;
	};


	public ref class PeptideMatch
	{
		public :
			PeptideMatch(pepHit* hit)
			{
				_hit = new pepHit(*hit); // new another copy, in case the object has been released.
				//_hit = hit;
				//System::Console::WriteLine("Mono: {0},Charge: {1}",_hit->monoMass,_hit->charge);
			}
			PeptideMatch(){}
			~PeptideMatch()
			{
				delete _hit;
			}
			property int basePeakIndex {public: int get() { return _hit->basePeakIndex;}	} 
			property int charge {public: int get() { return _hit->charge;}	} 
			property int lowIndex {public: int get() { return _hit->lowIndex;}	} 
			property int highIndex {public: int get() { return _hit->highIndex;}	} 
			property int variantIndex {public: int get() { return _hit->variantIndex;}	} 
			property float intensity {public: float get() { return _hit->intensity;}	} 
			property float area {public: float get() { return _hit->area;}	} 
			property double massShift {public: double get() { return _hit->massShift;}	} 
			property double lowMZ {public: double get() { return _hit->lowMZ;}	} 
			property double highMZ {public: double get() { return _hit->highMZ;}	} 
			property double monoMass {public: double get() { return _hit->monoMass;}	} 
			property double corr {public: double get() { return _hit->corr;}	} 
			property double baseMZ {public: double get () { return _hit->baseMZ;}}

			property List<float>^ distribution 
			{
				public: 
					List<float>^ get () 
					{ 
						List<float>^ distribution = gcnew List<float>();
						for (int i =0; i <_hit->distribution.size();i++) distribution->Add(_hit->distribution[i]);
						return distribution;
					}
			}

		
			//property List<float> Distribution {public: vector<float> get() 
			//{	
			//	List<float> abc = new List<float>();

			//	return _hit->distribution;
			//} // intensity distribution of isotopic peaks
			
		private :
			pepHit* _hit;


	};



	public ref class HardklorWrapper
	{
		public :
			HardklorWrapper(ParameterWrapper^ parameter)
			{
				CHardklorSetting* setting = parameter->GetParameter();
				_hardklor = new HardklorLite(*setting);
			}
			~HardklorWrapper(){}
			List<PeptideMatch^>^ AnalyzeSpectrum(SpectrumWrapper^ spectrum);
		private:
			HardklorLite* _hardklor;
			
	};



}

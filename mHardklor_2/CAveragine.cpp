#include <iomanip>
#include <iostream>
#include "CAveragine.h"





using namespace std;

//this may only work in .Net 
//using namespace System; 
//using namespace System::Text::RegularExpressions;

// this will never be called in ManagedHardklor
/*
CAveragine::CAveragine(char* fn, char* fn2){
	PT = new CPeriodicTable(fn2);
  atoms = new int[PT->size()];
  for(int i=0;i<PT->size();i++) atoms[i]=0;
  enrich = new vector<atomInfo>;
  loadTable(fn); 
}
*/

CAveragine::CAveragine(char* isotopefile, char* hardklorfile)
//CAveragine::CAveragine()
{
  PT = new CPeriodicTable(hardklorfile);
  atoms = new int[PT->size()];
  for(int i=0;i<PT->size();i++) atoms[i]=0;
  enrich = new vector<atomInfo>;
  loadIsotopeTable(isotopefile); 
}

CAveragine::~CAveragine(){
  delete [] atoms;
  for(unsigned int i=0;i<enrich->size();i++){
    delete enrich->at(i).abundance;
    delete enrich->at(i).mass;
  }
  delete enrich;
}


char* CAveragine::GetMessage()
{
	return message;
}



void CAveragine::calcAveragine(double dMass, CHardklorVariant hv) {
  int i,j;
  int iso;
  int atom;
  double fixedD = dMass;
  double units;
  double *enr;
  double *abun;
  double maxAbun;
  double totAbun;
  enr = new double[PT->size()];
  for(i=0;i<PT->size();i++) enr[i]=0;

  /*System::Console::WriteLine("size Enrich: {0}",hv.sizeEnrich());
  System::Console::WriteLine("size Atom: {0}",hv.sizeAtom());
  Console::WriteLine("mass\t{0}",dMass);*/

  for(i=0;i<hv.sizeEnrich();i++){
   
    atom = hv.atEnrich(i).atomNum;
    iso = enrich->at(atom).numIsotopes;
    abun = new double[iso];

    //Set our abundance array and find maximum
    maxAbun=0;
    for(j=0;j<iso;j++){
      abun[j]=enrich->at(atom).abundance->at(j);
      if(abun[j]>maxAbun) maxAbun=abun[j];
    }

    //Normalize to 1
    for(j=0;j<iso;j++) abun[j]/=maxAbun;
    
    //Normalize, then Calculate enrichment
    totAbun=0;
    enr[atom]=0;
    for(j=0;j<iso;j++){
      abun[j]/=maxAbun;
      if(j==hv.atEnrich(i).isotope) abun[j]=(1-hv.atEnrich(i).ape)*abun[j]+hv.atEnrich(i).ape;
      else abun[j]=(1-hv.atEnrich(i).ape)*abun[j];
      totAbun+=abun[j];
      enr[atom]+=abun[j]*enrich->at(atom).mass->at(j);
    }

    //Calculate average mass
    enr[atom] /= totAbun;
    enr[atom] -= PT->at(atom).mass;

    delete [] abun;
  }

  for(i=0;i<hv.sizeAtom();i++) {
    
    atoms[hv.atAtom(i).iLower]+=hv.atAtom(i).iUpper;
    fixedD -= hv.atAtom(i).iUpper*(PT->at(hv.atAtom(i).iLower).mass + enr[hv.atAtom(i).iLower]);
  }

  units = (fixedD)/(AVE_MASS + enr[1]*AVE_H + enr[6]*AVE_C + enr[7]*AVE_N + enr[8]*AVE_O + enr[16]*AVE_S);

  //System::Console::WriteLine("units: {0}",units);
  
  //Quick fix; assumes complete periodic table
  atoms[6] += (int)(AVE_C*units+0.5);
  atoms[8] += (int)(AVE_O*units+0.5);
  atoms[7] += (int)(AVE_N*units+0.5);
  atoms[16] += (int)(AVE_S*units+0.5);

  atoms[1] += (int)(fixedD-( ((int)(AVE_C*units+0.5)) * (PT->at(6).mass + enr[6]) + 
			     ((int)(AVE_N*units+0.5)) * (PT->at(7).mass + enr[7]) + 
			     ((int)(AVE_O*units+0.5)) * (PT->at(8).mass + enr[8]) +  
			     ((int)(AVE_S*units+0.5)) * (PT->at(16).mass + enr[16]) )+0.5);


  /*for(int i =0; i < PT->size();i++)
  {
	  System::Console::WriteLine("atom {0} is : {1}",i,gcnew System::String(PT->at(i).symbol));
  }*/

  delete [] enr;

}
void CAveragine::clear(){
  delete [] atoms;
  atoms = new int[PT->size()];
  for(int i=0;i<PT->size();i++) atoms[i]=0;
}

int CAveragine::getElement(int i){
  return atoms[i];
}


void CAveragine::getAveragine(char *str){
  char tstr[10];
  int i;
  str[0]=0;
  /*
  for(i=0;i<PT->size();i++)
  {
	  System::Console::WriteLine("atom {0} is {1}, and number is {2}",i,gcnew System::String(PT->at(i).symbol),atoms[i]);
  }
  System::Console::WriteLine("Total size of table is {0}",PT->size());
  */
  for(i=0;i<PT->size();i++)
  {
    if(atoms[i]>0)
	{
      sprintf(tstr,"%s%i",PT->at(i).symbol,atoms[i]);
      strcat(str,tstr);
    }
  }
  
}
double CAveragine::getMonoMass(){
  double d=0;
  int i;

  for(i=0;i<PT->size();i++) d+=atoms[i]*PT->at(i).mass;
  return d;

}
// this is written in Managed C++
/*
int CAveragine::loadIsotopeTable()
{
	// the isotope file has been embeded into ManagedHardklor.dll module, therefore,
	// reading file via FILE doesn't work anymore.
	// 
	// the embeded text file is stored in char* content,
	// we parse data from char* directly
	// HY
	const char* data;
	char* buffer;
	
	atomInfo a;
	double d;
	int idx = 0;


	// read and load embedded isotope.dat
	HMODULE handle = LoadLibrary(TEXT("mHardklor.dll"));
	HRSRC rc = FindResource(handle, MAKEINTRESOURCE(IDR_TEXT_ISOTOPE),MAKEINTRESOURCE(TEXTFILE));
	HGLOBAL rcData = LoadResource(handle, rc);
	DWORD dwSize = SizeofResource(handle, rc);
	// lock resoure into global memory
	data = static_cast<const char*>(LockResource(rcData));
	buffer = new char[dwSize+1];
	memcpy(buffer, data, dwSize); // copy
	buffer[dwSize] = 0;
	

	// parse loaded resource
	String^ content = gcnew String(buffer);

   
	String^ pattern = "(?<symbol>[a-zA-Z]+)\\s+(?<no>\\d+)\\s+(?<what>([\\d\\.]+\\s+[\\d\\.]+\\s+)+)";

	Regex^ regex = gcnew Regex( pattern );
	for( Match^ match = regex->Match( content ); match->Success; match = match->NextMatch( )) 
	{
		if( match->Value->Length > 0 )
		{
			enrich->push_back(a);
			enrich->at(idx).symbol[0] = match->Groups["symbol"]->Value[0];
			enrich->at(idx).symbol[1] = (match->Groups["symbol"]->Value->Length > 1) ? match->Groups["symbol"]->Value[1] : 0;
			enrich->at(idx).symbol[2]=0;

			enrich->at(idx).numIsotopes = int::Parse(match->Groups["no"]->Value);
		 
			//Console::WriteLine("{0}\t{1}",match->Groups["symbol"]->Value,match->Groups["no"]->Value);

			String^ proportion = "(?<mass>[\\d\\.]+)\\s+(?<abundance>[\\d\\.]+)\\s+";
			Regex^ reg_pro = gcnew Regex( proportion );
			int k = 0;
			enrich->at(idx).mass = new vector<double>;
			enrich->at(idx).abundance = new vector<double>;
			for (Match^ match_pro = reg_pro->Match(match->Groups["what"]->Value); match_pro->Success; match_pro = match_pro->NextMatch( ))
			{
				//Console::WriteLine(">>{0}\t{1}",match_pro->Groups["mass"]->Value,match_pro->Groups["abundance"]->Value);
				enrich->at(idx).mass->push_back(d);
				enrich->at(idx).abundance->push_back(d);
				enrich->at(idx).mass->at(k) = double::Parse(match_pro->Groups["mass"]->Value);
				enrich->at(idx).abundance->at(k)= double::Parse(match_pro->Groups["abundance"]->Value);
				k++;
			}
			idx++;
		}
	  
	}
	//Console::WriteLine("Size: {0}",enrich->size());
	
   
	free(buffer); // free buffer
	FreeLibrary(handle); // free resource


	// check enrich 
	//for (int i=0; i < enrich->size(); i++ )
	//{
	//	String^ s = gcnew String(enrich->at(i).symbol);
	//	Console::WriteLine("{0}\t{1}",s,enrich->at(i).numIsotopes);
	//	Console::WriteLine("{0}",enrich->at(i).mass->size());
	//	for (int j = 0; j < enrich->at(i).mass->size();j++)
	//	{
	//		Console::WriteLine("\tM:{0},A:{1}",enrich->at(i).mass->at(j),enrich->at(i).abundance->at(j));
	//	}
	//}
	//Console::WriteLine("finish load isotope table--Total number of atoms: {0}",enrich->size());
	
	return 1;
}
*/

int CAveragine::loadIsotopeTable(char* isotopefile)
{
	FILE *f;
	int  i,j;
	atomInfo a;
	double d=0;
  
	f = fopen(isotopefile,"rt");
	if(f==NULL) {
		cout << "Cannot read " << isotopefile << endl;
		message = "can not open file [CAveragine::loadTable]"; 
		//exit(-1); //HY
		return 1;
	}

	i=0;
	while(!feof(f)){

	  // enrich is a vector of atomInfo
    enrich->push_back(a);
	fscanf(f,"%2s\t%d\n",&enrich->at(i).symbol,&enrich->at(i).numIsotopes);
    enrich->at(i).mass = new vector<double>;
    enrich->at(i).abundance = new vector<double>;

    for(j=0;j<enrich->at(i).numIsotopes;j++)
	{
		enrich->at(i).mass->push_back(d);
		enrich->at(i).abundance->push_back(d);
		fscanf(f,"%lf\t%lf\n",&enrich->at(i).mass->at(j),&enrich->at(i).abundance->at(j));
    }

	fscanf(f," \n");
    i++;
  }
  fclose(f);
  return 1;
}


/* 
void CAveragine::loadTable(char* c) // c is filepath of ISOTOPE.DAT
{
 
 // how hardklor read and parse file 
  FILE *f;
  int  i,j;
  atomInfo a;
	double d=0;
  
  f = fopen(c,"rt");
  if(f==NULL) {
    cout << "Cannot read " << c << endl;
	message = "can not open file [CAveragine::loadTable]"; 
    //exit(-1); //HY
	return;
  }

  i=0;
  while(!feof(f)){

	  // enrich is a vector of atomInfo
    enrich->push_back(a);
		fscanf(f,"%2s\t%d\n",&enrich->at(i).symbol,&enrich->at(i).numIsotopes);
    enrich->at(i).mass = new vector<double>;
    enrich->at(i).abundance = new vector<double>;

    for(j=0;j<enrich->at(i).numIsotopes;j++){
			enrich->at(i).mass->push_back(d);
			enrich->at(i).abundance->push_back(d);
			fscanf(f,"%lf\t%lf\n",&enrich->at(i).mass->at(j),&enrich->at(i).abundance->at(j));
    }

		fscanf(f," \n");
    i++;
  }
  fclose(f);
 
}
*/






//This is not to be used normally - just here to make my life easier.
void CAveragine::setAveragine(int C, int H, int N, int O, int S){
  atoms[1]=H;
  atoms[6]=C;
  atoms[7]=N;
  atoms[8]=O;
  atoms[16]=S;
}

//
// This program is orginally from CHardklor2.cpp and modified by HY -2013
//
// 
//
//


//using namespace System;

#include "HardklorLite.h"



HardklorLite::HardklorLite(CHardklorSetting sett){
	cs = sett;
	if(PT==NULL) PT = new CPeriodicTable(sett.HardklorFile); //
	averagine = new CAveragine(sett.MercuryFile,sett.HardklorFile); // 
	mercury = new CMercury8(sett.MercuryFile); // 
	models = new CModelLibrary(averagine,mercury);

	// setting variant only used when modification is given. Do NOT support this function in ManagedHardklor & HardklorLite
	vector<CHardklorVariant> pepVariants;
	CHardklorVariant hkv;
	pepVariants.push_back(hkv);
	models->eraseLibrary();
	models->buildLibrary(sett.minCharge,sett.maxCharge, pepVariants);

}

HardklorLite::~HardklorLite(){
	averagine=NULL;
	mercury=NULL;
	models=NULL;
	if(PT!=NULL) {
		delete PT;
		PT=NULL;
	}
}

vector<pepHit> HardklorLite::Analyze(Spectrum& spec)
{
	vector<pepHit> vPeps = QuickHardklor(spec);
	//vector<pepHit> vPeps;
	return	vPeps;
}


int HardklorLite::BinarySearch(Spectrum& s, double mz, bool floor){

	int mid=s.size()/2;
	int upper=s.size();
	int lower=0;

	if(mz>=s[s.size()-1].mz) return s.size()-1;
	if(mz<=s[0].mz) return 0;

	while(s[mid].mz!=mz){
		if(lower>=upper) break;
		if(s[mid].mz>mz){
			upper=mid-1;
			mid=(lower+upper)/2;
		} else {
			lower=mid+1;
			mid=(lower+upper)/2;
		}
	}

	if(floor && s[mid].mz>mz && mid>0) return mid-1;
	else if(!floor && s[mid].mz<mz && mid<s.size()) return mid+1;
	else return mid;

}

//Calculates the resolution (FWHM) of a peak
double HardklorLite::CalcFWHM(double mz,double res,int iType){
	double deltaM;
	switch(iType){
	case 0: //Orbitrap
		deltaM = mz * sqrt(mz) / (20*res);  //sqare root of 400
		break;
	case 1: //TOF
		deltaM = mz / res;
		break;
	case 2: //QIT
		deltaM = res / 5000.0;
		break;
	case 3: //FTICR
	default:
		deltaM = mz * mz / (400*res);
		break;
	}
	return deltaM;
}

bool HardklorLite::CheckForPeak(vector<Result>& vMR, Spectrum& s, int index){
	double dif=100.0;
	double massDif;
	bool match=false;

	int mid=s.size()/2;
	int upper=s.size();
	int lower=0;

	double FWHM=CalcFWHM(vMR[index].mass,cs.res400,cs.msType);

	if(vMR[index].mass>=s[s.size()-1].mz) {
		mid=s.size()-1;
	} else if(vMR[index].mass<=s[0].mz) {
		mid=0;
	} else {

		while(s[mid].mz!=vMR[index].mass){
			if(lower>=upper) break;
			if(s[mid].mz>vMR[index].mass){
				upper=mid-1;
				mid=(lower+upper)/2;
			} else {
				lower=mid+1;
				mid=(lower+upper)/2;
			}
		}
	}

	dif=fabs(s[mid].mz-vMR[index].mass);
	if(mid>0){
		massDif=fabs(s[mid-1].mz-vMR[index].mass);
		if(massDif<dif) {
			dif=massDif;
			mid--;
		}
	} 
	if(mid<s.size()-1){
		massDif=fabs(s[mid+1].mz-vMR[index].mass);
		if(massDif<dif) {
			dif=massDif;
			mid++;
		}
	}

	if(dif<FWHM){
		if(mask[mid].intensity>1.0) return false;
		else return true;
	}
	return false;

}

int HardklorLite::CompareBPI(const void *p1, const void *p2){
  const pepHit d1 = *(pepHit *)p1;
  const pepHit d2 = *(pepHit *)p2;
	if(d1.basePeakIndex<d2.basePeakIndex) return -1;
	else if(d1.basePeakIndex>d2.basePeakIndex) return 1;
  else return 0;
}

double HardklorLite::LinReg(vector<float>& mer, vector<float>& obs){

  int i,sz;
  double sxx=0,syy=0,sxy=0;

  //Cosine angle correlation
  sxy=0;
  sxx=0;
  syy=0;
	sz=(int)mer.size();
  for(i=0;i<sz;i++){
    sxy += (mer[i]*obs[i]);
    sxx += (mer[i]*mer[i]);
    syy += (obs[i]*obs[i]);
  }

  if(sxx>0 && syy>0 && sxy>0) return sxy/sqrt(sxx*syy);
  else return 0;
    
}

bool HardklorLite::MatchSubSpectrum(Spectrum& s, int peakIndex, pepHit& pep){

	int i,k,n;
	unsigned int varCount;
	unsigned int v;
	float max=s[peakIndex].intensity;
	int maxMercuryIndex[3];
	vector<int> charges;
	double dif;
	vector<float> obs;
	vector<float> mer;
	vector<int> vMatchIndex;
	vector<float> vMatchPeak;
	vector<Result> vMR;
	Result r;
	double corr;
	double da;

	//keep track of best hits
	double bestCorr;
	double bestDA;
	int bestCharge;
	double bestMass;
	vector<int> bestMatchIndex;
	vector<float> bestMatchPeak;
	int matchCount;
	int bestMatchCount;
	int thisMaxIndex=0;
	int bestVariant;

	mercuryModel* model=NULL;
	mercuryModel* matchedModel=NULL;

	double deltaM = CalcFWHM(s[peakIndex].mz,cs.res400,cs.msType);
	QuickCharge(s,peakIndex,charges);

	bestCorr=0.0;
	bestMatchCount=0;

	//Mark number of variants to analyze
	if(cs.noBase) varCount=cs.variant->size();
	else varCount=cs.variant->size()+1;

	//iterate through all charge states
	for(i=0;i<(int)charges.size();i++){

		for(v=0;v<varCount;v++){

			//get model from library
			dif=0;
			model=models->getModel(charges[i],v,s[peakIndex].mz);
			for(k=0; k<model->size; k++) {
				if(model->peaks[k].intensity>dif){
					dif = model->peaks[k].intensity;
					maxMercuryIndex[0]=k;
				}
			}
			if(k==0) maxMercuryIndex[1]=-1;
			else maxMercuryIndex[1]=maxMercuryIndex[0]-1;		//allow right shift
			maxMercuryIndex[2]=maxMercuryIndex[0]+1;				//allow left shift

			//Apply shift and find mz boundaries
			n=0;
			while(n<3){
				
				if(maxMercuryIndex[n]<0) {
					n++;
					continue;
				}

				//Align the mercury distribution (MD) to the observed peak. The MD can shift in
				//either direction for one peak to adjust for system noise. The width of the MD
				//determines the boundaries for correlation to the observed data.
				double lower=5000.0;
				double upper=0.0;
				double shft = s[peakIndex].mz - model->peaks[maxMercuryIndex[n]].mz;
				
				vMR.clear();
				for(k=0; k<model->size; k++) {				
					r.data=model->peaks[k].intensity;
					r.mass=model->peaks[k].mz+shft;
					vMR.push_back(r);
					if(model->peaks[k].intensity>99.999) thisMaxIndex=vMR.size()-1;
					
					if(r.mass<lower) lower=r.mass;
					if(r.mass>upper) upper=r.mass;
				}
				da=model->area;

				//Add a little buffer to the bounds
				lower-=0.1;
				upper+=0.1;

				//Match predictions to the observed peaks and record them in the proper array.
				corr=PeakMatcherB(vMR,s,lower,upper,deltaM/2,peakIndex,matchCount,vMatchIndex,vMatchPeak);
				//cout << "\tMSS: " << s[peakIndex].mz << " " << s[peakIndex].intensity << "\t" << charges[i] << "\t" << matchCount << "\t" << corr << endl;

				if(corr>bestCorr || (corr>cs.corr && corr+0.025*(matchCount-bestMatchCount)>bestCorr) ){
					bestMatchIndex=vMatchIndex;
			
					bestMatchPeak=vMatchPeak;
					bestMatchCount=matchCount;
					bestCorr=corr;
					bestMass=model->zeroMass+shft*charges[i];
					bestCharge=charges[i];
					bestDA=da;
					bestVariant=v;
					matchedModel = model;

				}

				n++;

			}//while

		}//for v (variant)

	}//for i (charge)
	model=NULL;


	//if above threshold, erase peaks.
	if(bestCorr>cs.corr){
		pep.area=(float)bestDA;
		strcpy(pep.averagine,"");
		pep.basePeakIndex=0;
		pep.charge=bestCharge;
		pep.corr=bestCorr;
		pep.highMZ=0;
		pep.lowMZ=0;
		pep.massShift=0;
		pep.monoMass=bestMass;
		pep.intensity=s[peakIndex].intensity;
		pep.variantIndex=bestVariant;
		pep.distribution = bestMatchPeak;

		//mark which peaks contributed to this analysis
		for(k=0;k<(int)bestMatchIndex.size();k++){
			if(bestMatchPeak[k]*max/s[bestMatchIndex[k]].intensity>0.5) s[bestMatchIndex[k]].intensity=-s[bestMatchIndex[k]].intensity;
			else s[bestMatchIndex[k]].intensity-=bestMatchPeak[k]*max;
		}

		return true;
	}

	return false;

}

double HardklorLite::PeakMatcher(vector<Result>& vMR, Spectrum& s, double lower, double upper, double deltaM, int matchIndex, int& matchCount, int& indexOverlap, vector<int>& vMatchIndex, vector<float>& vMatchIntensity){

	vMatchIndex.clear();
	vMatchIntensity.clear();

	vector<float> obs;
	vector<float> mer;
				
	bool match;
	bool bMax=false;
	double corr=0.0;
	double dif;
	double massDif;

	matchCount=0;
	indexOverlap=-1;

	int j,k;
	for(k=0;k<(int)vMR.size();k++) {
		if(vMR[k].data>99.9) bMax=true;

		match=false;
		dif=deltaM;
						
		//look left
		j=matchIndex;
		while(j>-1 && s[j].mz>=lower){
			massDif=s[j].mz-vMR[k].mass;
			if(massDif<-deltaM) break;
			if(fabs(massDif)<dif){
				dif=fabs(massDif);
				match=true;
				matchIndex=j;
			}
			j--;
		}

		//look right
		j=matchIndex+1;
		while(j<s.size() && s[j].mz<=upper){
			massDif=s[j].mz-vMR[k].mass;
			if(massDif>deltaM) break;
			if(fabs(massDif)<dif){
				dif=fabs(massDif);
				match=true;
				matchIndex=j;
			}
			j++;
		}
	
		if(!match) 
		{
      //if expected peak is significant (above 50 rel abun) and has no match, match it to 0.
			if(vMR[k].data>50.0) {
        //cout << "xM: " << vMR[k].mass << "\t0" << endl;
			mer.push_back((float)vMR[k].data);
			obs.push_back(0.0f);
			if(bMax) break;
			}
      
		} else {
			mer.push_back((float)vMR[k].data);
      //cout << "xM: " << vMR[k].mass << "\t" << s[matchIndex].mz << endl;
			if(mask[matchIndex].intensity>1.0 && vMR[k].data>50) {
				if(indexOverlap<0) indexOverlap=matchIndex;
			}
			if(s[matchIndex].intensity<1.0) {
				obs.push_back(0.0f);
			} else {
				matchCount++;
				obs.push_back(s[matchIndex].intensity);
			}
			vMatchIndex.push_back(matchIndex);
			vMatchIntensity.push_back((float)vMR[k].data/100.0f);
		}
	}

	if(matchCount<2) corr=0.0;
	else corr=LinReg(mer,obs);

	//for(j=0;j<mer.size();j++){
  //  cout << "M:" << mer[j] << "\t" << "O:" << obs[j] << endl;
	//}
	//cout << "Corr: " << corr << "(" << matchCount << ")" << endl;

  //remove last matched peaks (possibly overlap with other peaks) but only if they are of low abundance.
	int tmpCount=matchCount;
  while(corr<0.90 && matchCount>2 && mer[mer.size()-1]<50.0){
		mer.pop_back();
		obs.pop_back();
		matchCount--;
		double corr2=LinReg(mer,obs);
		//cout << "Old corr: " << corr << "(" << matchCount+1 << ")" << " New corr: " << corr2 << endl;
		if(corr2>corr) {
			corr=corr2;
			tmpCount=matchCount;
		}
	}
	matchCount=tmpCount;

	return corr;
}

double HardklorLite::PeakMatcherB(vector<Result>& vMR, Spectrum& s, double lower, double upper, double deltaM, int matchIndex, int& matchCount, vector<int>& vMatchIndex, vector<float>& vMatchIntensity){

	vMatchIndex.clear();
	vMatchIntensity.clear();

	vector<float> obs;
	vector<float> mer;
				
	bool match;
	bool bMax=false;
	double corr=0.0;
	double dif;
	double massDif;

	matchCount=0;

	int j,k;
	for(k=0;k<(int)vMR.size();k++) 
	{
		if(vMR[k].data>99.9) bMax=true;
		else bMax=false;

		match=false;
		dif=deltaM;
						
		//look left
		j=matchIndex;
		while(j>-1 && s[j].mz>=lower){
			massDif=s[j].mz-vMR[k].mass;
			if(massDif<-deltaM) break;
			if(fabs(massDif)<dif){
				dif=fabs(massDif);
				match=true;
				matchIndex=j;
			}
			j--;
		}

		//look right
		j=matchIndex+1;
		while(j<s.size() && s[j].mz<=upper){
			massDif=s[j].mz-vMR[k].mass;
			if(massDif>deltaM) break;
			if(fabs(massDif)<dif){
				dif=fabs(massDif);
				match=true;
				matchIndex=j;
			}
			j++;
		}
	
		if(!match) {
			break;
		} else {
			mer.push_back((float)vMR[k].data);
			if(s[matchIndex].intensity<1.0) {
				obs.push_back(0.0f);
			} else {
				matchCount++;
				obs.push_back(s[matchIndex].intensity);
			}
			vMatchIndex.push_back(matchIndex);
			vMatchIntensity.push_back((float)vMR[k].data/100.0f);
		}
	}

	if(matchCount<2) corr=0.0;
	else corr=LinReg(mer,obs);

	int tmpCount=matchCount;
	while(corr<0.90 && matchCount>2){
		mer.pop_back();
		obs.pop_back();
		matchCount--;
		double corr2=LinReg(mer,obs);
		if(corr2>corr) {
			corr=corr2;
			tmpCount=matchCount;
		}
	}

	matchCount=tmpCount;
	return corr;

}

void HardklorLite::QuickCharge(Spectrum& s, int index, vector<int>& v){

	int i,j;
	double dif;
	double rawCh;
	double rawChR;
	int ch;
	int charge[1000];

	for(i=cs.minCharge;i<=cs.maxCharge;i++) charge[i]=0;

	//check forward
	for(j=index+1;j<s.size();j++){
		//if(s[j].intensity<1.0f) continue;
			
		dif = s[j].mz - s[index].mz;
		if(dif > 1.1) break;
			
		rawCh=1/dif;
		ch = (int)(rawCh+0.5);
		rawChR=rawCh-(int)rawCh;
		if(rawChR>0.2 && rawChR<0.8) continue;
		if(ch<cs.minCharge || ch>cs.maxCharge) continue;
		charge[ch]=1;
	}
  //if no forward charge, exit now.
  bool bMatch=false;
  for(i=cs.minCharge;i<=cs.maxCharge;i++){
    if(charge[i]>0) {
      bMatch=true;
      break;
    }
  }
  if(!bMatch) {
    v.clear();
    return;
  }

	//check backward
	for(j=index-1;j>=0;j--){
		//if(s[j].intensity<=0.0f) continue;
			
		dif = s[index].mz - s[j].mz;
		if(dif > 1.1) break;
			
		rawCh=1/dif;
		ch = (int)(rawCh+0.5);
		rawChR=rawCh-(int)rawCh;
		if(rawChR>0.2 && rawChR<0.8) continue;
		if(ch<cs.minCharge || ch>cs.maxCharge) continue;
		charge[ch]=1;
	}

	v.clear();
	for(i=cs.minCharge;i<=cs.maxCharge;i++){
		if(charge[i]>0) v.push_back(i);
	}

}

vector<pepHit> HardklorLite::QuickHardklor(Spectrum& s) {

	// return object
	vector<pepHit> vPeps;

	//iterators
	int i,j,k,n,m,x;
	unsigned int varCount;
	unsigned int v;

	//tracking spectrum peak intensities
	float maxHeight=9999999999999.9f;
	float max=0.0f;
	float lowPoint=9999999999999.9f;

	//Mercury storage and variables aligning mercury data (including 1 da shifts)
	mercuryModel* model;
	vector<Result> vMR;
	Result r;
	int maxIndex;
	int thisMaxIndex;
	int maxMercuryIndex[3];
	double da;
	double lower;
	double upper;
	double shft;

	//peak variables
	vector<int> charges;
	double deltaM;
	double dif;
	double corr;
	vector<float> obs;
	vector<float> mer;
	vector<int> vMatchIndex;
	vector<float> vMatchPeak;
	vector<int> vMatchIndex2;
	vector<float> vMatchPeak2;
	int matchCount,matchCount2;
	int indexOverlap;
  double top3[3];

	//refinement variables
	bool keepPH;
	pepHit ph2;
	//pepHit bestKeepPH;
	int lowIndex;
	int highIndex;
	bool corr2;
	double corr3;

	//best hit variables
	double bestCorr;
	double bestLow;
	double bestHigh;
	double bestDA;
	int bestCharge;
	double bestMass;
	vector<int> bestMatchIndex;
	vector<float> bestMatchPeak;
	int bestMatchCount;
	pepHit bestPH;
	bool bestKeepPH;
	int bestOverlap;
	int bestLowIndex;
	int bestHighIndex;
	int bestVariant;


	//Results
	pepHit ph;

	//Spectrum variables
	Spectrum origSpec=s;
	Spectrum refSpec=s;
	Spectrum tmpSpec;

	//create mask
	mask.clear();
	for(i=0;i<s.size();i++) mask.add(s[i].mz,0);

	//find lowest intensity;
	for(i=0;i<s.size();i++){
    //printf("%.6lf\t%.1f\n",s[i].mz, s[i].intensity);
		if(s[i].intensity<lowPoint) lowPoint=s[i].intensity;
	}

	//clear results vector
	

	vPeps.clear();

	
	//Mark number of variants to analyze
	if(cs.noBase) varCount=cs.variant->size();
	else varCount=cs.variant->size()+1;

	//start the loop through all peaks
	while(true)
	{

		//Find most intense peak. Note that sorting is not possible because
		//peaks change in intensity as they are deconvolved. Also it is advantageous
		//to keep peaks in m/z order
		max=0.0f;
		for(i=0;i<s.size();i++){
			if(s[i].intensity<maxHeight && s[i].intensity>max){
				max=s[i].intensity;
				maxIndex=i;
			}
		}

		//stop searching when we reach lowest original point
		//this prevents overfitting with lots of partial noise peaks
		if(max<lowPoint) break;

		//Get the FWHM estimate for the peak we are at.
		deltaM = CalcFWHM(s[maxIndex].mz,cs.res400,cs.msType);

		//Get the charge states. Note that only remaining peaks are used in the estimate.
		//I'm not sure this is best, but it is simpler and faster
		QuickCharge(s,maxIndex,charges);



		//Reset our correlation and matchcount scores. Then iterate through each charge state and find best
		//match to the peaks.
		bestCorr=0.0;
		bestMatchCount=0;
		for(i=0;i<(int)charges.size();i++)
		{

			//cout << s[maxIndex].mz << "\t" << charges[i] << endl;

			//check all variants
			for(v=0;v<varCount;v++)
			{
				//System::Console::WriteLine("Got into varCount loop");
				//use model library, align to top 3 peaks
				dif=0;
			    top3[0]=top3[1]=top3[2]=0;
				maxMercuryIndex[0]=maxMercuryIndex[1]=maxMercuryIndex[2]=-1;

		
				model=models->getModel(charges[i],v,s[maxIndex].mz);

				//System::Console::WriteLine("model size: {0}",model->size);

				for(k=0; k<model->size; k++) 
				{
          //cout << "i\t" << model->peaks[k].mz << "\t" << model->peaks[k].intensity << endl;
					//if(model->peaks[k].intensity>dif){
					//System::Console::WriteLine("What is maxMercury Index");
					  if(model->peaks[k].intensity>top3[0]){
									//dif = model->peaks[k].intensity;
						top3[2]=top3[1];
						top3[1]=top3[0];
						top3[0]=model->peaks[k].intensity;
						
						maxMercuryIndex[2]=maxMercuryIndex[1];
						maxMercuryIndex[1]=maxMercuryIndex[0];
						maxMercuryIndex[0]=k;
						

						

					  } else if(model->peaks[k].intensity>top3[1]) {
						top3[2]=top3[1];
						top3[1]=model->peaks[k].intensity;
						maxMercuryIndex[2]=maxMercuryIndex[1];
						maxMercuryIndex[1]=k;
					  } else if(model->peaks[k].intensity>top3[2]) {
						top3[2]=model->peaks[k].intensity;
									maxMercuryIndex[2]=k;
					  }
					  //System::Console::WriteLine("It's fine");
				}
				//if(k==0) maxMercuryIndex[1]=-1;
				//else maxMercuryIndex[1]=maxMercuryIndex[0]-1;		//allow right shift
				//maxMercuryIndex[2]=maxMercuryIndex[0]+1;				//allow left shift

				//Test all three positions for the model. Note that if the first peak is the base peak, then
				//no left shift is tested.
				n=0;
				//System::Console::WriteLine("everything was fine before n>3");
				while(n<3)
				{

					//skip the left shift if already at leftmost peak.
					if(maxMercuryIndex[n]<0) 
					{
						n++;
						continue;
					}

          //cout << "ii\tShift #" << n << endl;

					//Align the mercury distribution (MD) to the observed peak. The MD can shift in
					//either direction for one peak to adjust for system noise. The width of the MD
					//determines the boundaries for correlation to the observed data.
					lower=5000.0;
					upper=0.0;
					shft = s[maxIndex].mz - model->peaks[maxMercuryIndex[n]].mz;
					vMR.clear();
					thisMaxIndex=0;
					da=0.0f;

					//use model library
					for(k=0; k<model->size; k++) 
					{
						
						r.data=model->peaks[k].intensity;
						r.mass=model->peaks[k].mz+shft;
						vMR.push_back(r);
						if(model->peaks[k].intensity>99.999) thisMaxIndex=vMR.size()-1;
					
						if(r.mass<lower) lower=r.mass;
						if(r.mass>upper) upper=r.mass;
					}
					da=model->area;

					//Add a little buffer to the m/z boundaries
					lower-=0.1;
					upper+=0.1;

					//Narrow the search to just the area of the spectrum we need
					lowIndex=BinarySearch(s,lower,true);
					highIndex=BinarySearch(s,upper,false);

					//if max peak shifts to already solved peak, skip
					if(!CheckForPeak(vMR,s,thisMaxIndex))
					{
						n++;
						continue;
					}

					//System::Console::WriteLine("everything was fine before PeakMatcher");

					//Match predictions to the observed peaks and record them in the proper array.
					corr=PeakMatcher(vMR,s,lower,upper,deltaM/2,maxIndex,matchCount,indexOverlap,vMatchIndex,vMatchPeak);
					//cout << "ii.i\t" << s[maxIndex].mz << " " << s[maxIndex].intensity << "\t" << charges[i] << "\t" << matchCount << "\t" << corr << "\t" << indexOverlap << "\t" << maxIndex << "\tn" << n << endl;

					//check any overlap with observed peptides. Overlap indicates deconvolution may be necessary.
					//Deconvolution is at best a rough estimate and is not used if it does not improve the correlation
					//scores.
					keepPH=false;
					//System::Console::WriteLine("everything was fine before indexOverlap loop");
					if(indexOverlap>-1 /*&& indexOverlap>maxIndex*/)
					{

            //cout << "iii\tChecking overlap: " << indexOverlap << "\t" << maxIndex << endl;

						//Find overlapping peptide
						for(m=0;m<(int)vPeps.size();m++)
						{
							if(vPeps[m].basePeakIndex==indexOverlap) break;
						}

						//break out subspectrum; this is done using the original spectrum peak heights, not
						//the current peak heights. The peak heights are then adjusted to account for the currently
						//overlapping peptide model.
						tmpSpec.clear();
						x=0;
						int subIndex=-1;
						for(j=vPeps[m].lowIndex;j<=vPeps[m].highIndex;j++){
							
							while(x<(int)vMatchIndex.size() && j>vMatchIndex[x]) x++;

							//generate temporary subspectrum with peak heights reduced for overlapping model
							if(x<(int)vMatchIndex.size() && j==vMatchIndex[x]){
								tmpSpec.add(origSpec[j].mz,origSpec[j].intensity-vMatchPeak[x]*max);
								x++;
							} else {
								tmpSpec.add(origSpec[j]);
							}

							//get the base peak index of the subspectrum
							if(j==indexOverlap) subIndex=tmpSpec.size()-1;
						}

						//Re-Solve subspectrum and see if it has better correlation
						corr2=MatchSubSpectrum(tmpSpec,subIndex,ph2);
						//cout << "iii.i\tCorr2: " << corr2 << "\t" << ph2.corr << "\t" << origSpec[vPeps[m].basePeakIndex].mz << "\t" << vPeps[m].charge << endl;

						//If correlation is better (or close), go back and try the
						//newly adjusted peaks.
						if(corr2 && ph2.corr+0.025>vPeps[m].corr){
							x=0;

							for(j=lowIndex;j<=highIndex;j++){
								if(x<tmpSpec.size() && s[j].mz==tmpSpec[x].mz){
									refSpec[j].intensity=(origSpec[j].intensity+tmpSpec[x].intensity);
									x++;
								} else {
									refSpec[j].intensity=s[j].intensity;
								}
							}

							//solve merged models
							corr3=PeakMatcher(vMR,refSpec,lower,upper,deltaM/2,maxIndex,matchCount2,indexOverlap,vMatchIndex2,vMatchPeak2);
							//cout << "iii.ii\tCorr3: " << s[maxIndex].mz << " " << s[maxIndex].intensity << "\t" << charges[i] << "\t" << matchCount2 << "\t" << corr3 << "\t" << indexOverlap << endl;

							//keep the new model if it is better than the old one.
							if(corr3>corr) {

								corr=corr3;
								vMatchIndex=vMatchIndex2;
								vMatchPeak=vMatchPeak2;
								matchCount=matchCount2;

								//refine the overlapping one.
								keepPH=true;
								
							} else {

								//it failed, do nothing and move on.
								keepPH=false;

							}
							
						}

					}//if indexOverlap>-1
          double tCorr;
          if(bestMatchCount==0) tCorr=0;
          else tCorr=0.025*(matchCount-bestMatchCount)/bestMatchCount;
					//cout << "Old best corr: " << bestCorr << "(" << bestMatchCount << ") This corr: " << corr << "," << corr+tCorr << "(" << matchCount << ")" << endl;
					if(/*corr>bestCorr ||*/ (corr>cs.corr && corr+tCorr>bestCorr) ){
						bestMatchIndex=vMatchIndex;
						bestMatchPeak=vMatchPeak;
						bestMatchCount=matchCount;
						bestCorr=corr;
						bestMass=model->zeroMass+shft*charges[i];
						bestCharge=charges[i];
						bestDA=da;
						bestLow=lower;
						bestHigh=upper;
						bestKeepPH=keepPH;
						bestPH=ph2;
						bestOverlap=m;
						bestLowIndex=lowIndex;
						bestHighIndex=highIndex;
						bestVariant=v;
					}

					n++;
				}//while
				
			}//for v (variants)
			//System::Console::WriteLine("Out of varCount loop -- End of charge loop");
		}//for i (charges)
		//System::Console::WriteLine("Start best correlation check");
		//if above threshold, erase peaks.
		if(bestCorr>cs.corr)
		{
			ph.area=(float)bestDA;
			ph.basePeakIndex=maxIndex;
			ph.charge=bestCharge;
			ph.corr=bestCorr;
			ph.highMZ=bestHigh;
			ph.intensity=max;
			ph.lowMZ=bestLow;
			ph.massShift=0.0;
			ph.monoMass=bestMass;
			ph.lowIndex=bestLowIndex;
			ph.highIndex=bestHighIndex;
			ph.variantIndex=bestVariant;
			ph.baseMZ = s[ph.basePeakIndex].mz;
			ph.distribution = bestMatchPeak;
			if(bestKeepPH){
				vPeps[bestOverlap].area=bestPH.area;
				vPeps[bestOverlap].intensity=bestPH.intensity;
				vPeps[bestOverlap].corr=bestPH.corr;
				vPeps[bestOverlap].charge=bestPH.charge;
				vPeps[bestOverlap].monoMass=bestPH.monoMass;
				vPeps[bestOverlap].variantIndex=bestPH.variantIndex;
				vPeps[bestOverlap].distribution = bestMatchPeak;
				
				
			}
			vPeps.push_back(ph);
			mask[maxIndex].intensity=100.0f;

			for(k=0;k<(int)bestMatchIndex.size();k++){
				if(bestMatchPeak[k]*max/s[bestMatchIndex[k]].intensity>0.5)
				{
					// I really do not understand why this is =-
					s[bestMatchIndex[k]].intensity =- s[bestMatchIndex[k]].intensity;
					//s[bestMatchIndex[k]].intensity -= s[bestMatchIndex[k]].intensity;

				} 
				else 
				{
					s[bestMatchIndex[k]].intensity-=bestMatchPeak[k]*max;
				}
        //cout << "iv\t" << s[bestMatchIndex[k]].mz << " is now " << s[bestMatchIndex[k]].intensity << endl;
			}
		} // end of bestCorr>cs.corr

		//set new maximum
		maxHeight=max;

	} // finally.... end of while loop

	//Sort results by base peak
	if(vPeps.size()>0) qsort(&vPeps[0],vPeps.size(),sizeof(pepHit),CompareBPI);

	//System::Console::WriteLine("\n >>QuickHardklor is finished \n");
	//Refine overfitting based on density
	RefineHits(vPeps,origSpec);
	

	return vPeps;
}
//Reduces the number of features (cs.depth) per 1 Da window. This removes a lot
//of false hits resulting from jagged tails on really large peaks. Criteria for
//removal is lowest peak intensity
void HardklorLite::RefineHits(vector<pepHit>& vPeps, Spectrum& s){

	unsigned int i;
	int j;
	double lowp,highp;
	bool bRestart=true;
	vector<pepHit> vTmpHit;
	vector<int> vPepMask;
	list<int> vList;
	list<int>::iterator it;

	//generate an index of the peptides to keep or throw away
	for(i=0;i<vPeps.size();i++) vPepMask.push_back(0);

	//iterate through all hits
	for(i=0;i<vPeps.size();i++){

		//skip anything already marked for removal
		if(vPepMask[i]>0)	continue;

		//put a tolerance around each peak
		lowp=s[vPeps[i].basePeakIndex].mz-0.5;
		highp=s[vPeps[i].basePeakIndex].mz+0.5;

		//put the current hit in the list
		vList.clear();
		vList.push_front(i);

		//find all other hits in the tolerance window
		//look left first
		j=i-1;
		while(j>-1){

			//break out when boundary is reached
			if(s[vPeps[j].basePeakIndex].mz<lowp) break;

			//skip anything marked for removal.
			if(vPepMask[j]>0){
				j--;
				continue;
			}

			//add to list from high to low
			for(it=vList.begin();it!=vList.end();it++){
				if(s[vPeps[j].basePeakIndex].intensity > s[vPeps[*it].basePeakIndex].intensity) break;
			}
			vList.insert(it,j);

			j--;
		}

		//look right
		j=i+1;
		while(j<(int)vPeps.size()){

			//break out when boundary is reached
			if(s[vPeps[j].basePeakIndex].mz>highp) break;

			//skip anything marked for removal.
			if(vPepMask[j]>0){
				j++;
				continue;
			}

			//add to list from high to low
			for(it=vList.begin();it!=vList.end();it++){
				if(s[vPeps[j].basePeakIndex].intensity > s[vPeps[*it].basePeakIndex].intensity) break;
			}
			vList.insert(it,j);

			j++;
		}

		//remove the lowest peptides below threshold (user specified depth)
		if((int)vList.size()>cs.depth){
			it=vList.begin();
			for(j=0;j<cs.depth;j++)	it++;
			for(it=it;it!=vList.end();it++)	vPepMask[*it]=1;
		}

	}

	//copy over the keepers
	for(i=0;i<vPeps.size();i++){
		if(vPepMask[i]) continue;
		vTmpHit.push_back(vPeps[i]);
	}
	vPeps.clear();
	for(i=0;i<vTmpHit.size();i++)vPeps.push_back(vTmpHit[i]);

}





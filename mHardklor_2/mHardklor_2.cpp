// This is the main DLL file.

#include "stdafx.h"
#include "HardklorLite.h"
#include "CHardklorSetting.h"
#include "Spectrum.h"
#include "mHardklor_2.h" // the one has using System has to be the last

namespace mHardklor_2 {

	CHardklorSetting* ParameterWrapper::GetParameter()
	{
		_setting = new CHardklorSetting();
		// here are default parameters.
		_setting->algorithm = Version2;
		_setting->chargeMode = 'Q';
		_setting->msType = OrbiTrap;
		_setting->sn = 0; // signal to noise calculation has been skept.
		// end
		_setting->res400 = resolution; 
		_setting->centroid = centroid;
		_setting->depth = depth;
		_setting->peptide =maxFeature;
		_setting->corr = correlation;
		_setting->sl = sensitivity;
		_setting->maxCharge = chargeMax;
		_setting->minCharge = chargeMin;
		_setting->winSize = mzWindow;

		//_setting->formula = '0.75H1';
		//_setting->snWin = 250; // this is used for singal to noise calculation, however, 

		return _setting;
	}


	Spectrum* SpectrumWrapper::GetSpectrum()
	{
		return _spec;
	}
	void SpectrumWrapper::Add(double mz,float intensity)
	{
		_spec->add(mz,intensity);
		PeakCount ++;
	}
	

	List<PeptideMatch^>^ HardklorWrapper::AnalyzeSpectrum(SpectrumWrapper^ spectrum)
	{
		Spectrum* spec = spectrum->GetSpectrum();
		List<PeptideMatch^>^ peptides = gcnew List<PeptideMatch^>();
		//Console::WriteLine("number of peaks:{0}",spec->size());
		vector<pepHit> pephits = _hardklor->Analyze(*spec);
		for (int i =0 ; i < pephits.size();i++)
		{
			PeptideMatch^ pep = gcnew PeptideMatch(&pephits[i]);
			peptides->Add(pep);
		}
		//Console::WriteLine("number of peptide hits: {0}",pephits.size());
		//vector<pepHit>.swap(pephits);
		return peptides;
	}





}


#include <windows.h>
#ifndef _CPERIODICTABLE_H
#define _CPERIODICTABLE_H



#include <iostream>
#include <vector>

// read embedded resource

#include <tchar.h>
#include "resource.h"

using namespace std;

typedef struct {
  int atomicNum;
  char symbol[3];
  double mass;
} element;

class CPeriodicTable {
 public:
   //Constructors & Destructors
   //CPeriodicTable();
   //CPeriodicTable(char* c="Hardklor.dat");
   CPeriodicTable(char*);
   //CPeriodicTable();
   ~CPeriodicTable();

   //Methods:
   element& at(int);
   int size();

 protected:
 private:
   //Methods:
   //void loadTable(char*);
   void loadPeriodicTable(char* PeriodicTable);

   //Data Members:
   vector<element> table;

};

#endif

#include "CPeriodicTable.h"

//using namespace std;

//this may only work in .Net 
//using namespace System; 
//using namespace System::Text::RegularExpressions;

/*
CPeriodicTable::CPeriodicTable(){
  table = new vector<element>;
  loadTable("Hardklor.dat");
}
*/



CPeriodicTable::CPeriodicTable(char* CPeriodicTable)
{
  //loadTable(c);
	loadPeriodicTable(CPeriodicTable);
}
//CPeriodicTable::CPeriodicTable()
//{
//	loadPeriodicTable();
//}

CPeriodicTable::~CPeriodicTable(){
}

element& CPeriodicTable::at(int i){
  return table.at(i);
}


void CPeriodicTable::loadPeriodicTable(char* PeriodicTable)
{
  FILE* fptr;
  element e;
  
  fptr = fopen(PeriodicTable,"rt");
  if(fptr==NULL) {
    cout << "Cannot open periodic table!" << endl;
    return;
  }
  
//  /* 
//     This loop reads in table entries, line by line.
//     It has basic error detection (missing data), but additional
//     checks should be made to confirm the content of data
//  */
  while(!feof(fptr)){

		fscanf(fptr,"%d\t%s\t%lf\n",&e.atomicNum,e.symbol,&e.mass);   
    table.push_back(e);
    
  }

  fclose(fptr);
}

/*
void CPeriodicTable::loadPeriodicTable()
{
	const char* data;
	char* buffer;
	
	element atom;

	// read and load embedded isotope.dat
	HMODULE handle = LoadLibrary(TEXT("mHardklor.dll"));
	HRSRC rc = FindResource(handle, MAKEINTRESOURCE(IDR_TEXT_HARDKLOR),MAKEINTRESOURCE(TEXTFILE));
	HGLOBAL rcData = LoadResource(handle, rc);
	DWORD dwSize = SizeofResource(handle, rc);
	// lock resoure into global memory
	data = static_cast<const char*>(LockResource(rcData));
	buffer = new char[dwSize+1];
	memcpy(buffer, data, dwSize); // copy
	buffer[dwSize] = 0;

	String^ content = gcnew String(buffer);


	String^ pattern = "(?<no>[\\d]+)\\s+(?<symbol>\\w+)\\s+(?<mass>[\\d\\.]+)\\s+";
	Regex^ regex = gcnew Regex(pattern);
	

	for (Match^ match = regex->Match(content); match->Success;match = match->NextMatch())
	{
		if( match->Value->Length > 0 )
		{
			
			atom.atomicNum = int::Parse(match->Groups["no"]->Value);
			atom.symbol[0] = match->Groups["symbol"]->Value[0];
			atom.symbol[1] = (match->Groups["symbol"]->Value->Length > 1) ? match->Groups["symbol"]->Value[1] : 0;
			atom.symbol[2]=0;
			atom.mass = double::Parse(match->Groups["mass"]->Value);
			//Console::WriteLine("atome symbol:{0}",gcnew System::String(atom.symbol));
			table.push_back(atom);
			
		}
	}
	// check table
	//for (int i = 0 ; i < table.size(); i ++)
	//{
	//	String^ sym = gcnew String(table.at(i).symbol);
	//	Console::WriteLine("{0},{1},{2}>>",table.at(i).atomicNum,sym,table.at(i).mass);
	//}
	//Console::WriteLine("Size of table: {0}",table.size());


	free(buffer);
	FreeLibrary(handle); // free resource
	//Console::WriteLine("finish load hardklor dat");
}
*/

//void CPeriodicTable::loadTable(char* c){
//  FILE* fptr;
//  element e;
//  
//  fptr = fopen(c,"rt");
//  if(fptr==NULL) {
//    cout << "Cannot open periodic table!" << endl;
//    return;
//  }
//  
//  /* 
//     This loop reads in table entries, line by line.
//     It has basic error detection (missing data), but additional
//     checks should be made to confirm the content of data
//  */
//  while(!feof(fptr)){
//
//		fscanf(fptr,"%d\t%s\t%lf\n",&e.atomicNum,e.symbol,&e.mass);   
//    table.push_back(e);
//    
//  }
//
//  fclose(fptr);
//  
//}







int CPeriodicTable::size(){
  return table.size();
}

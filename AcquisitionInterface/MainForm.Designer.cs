﻿namespace AcquisitionInterface
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkCurrentExperimentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.scanDefinitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hardklorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.includingListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_hybrid = new System.Windows.Forms.GroupBox();
            this.richTextBox_message = new System.Windows.Forms.RichTextBox();
            this.toolStrip1.SuspendLayout();
            this.groupBox_hybrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton2,
            this.toolStripDropDownButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(625, 25);
            this.toolStrip1.TabIndex = 36;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.checkCurrentExperimentsToolStripMenuItem});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(132, 22);
            this.toolStripDropDownButton2.Text = "Experiment sequence";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.newToolStripMenuItem.Text = "Add an experiment";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // checkCurrentExperimentsToolStripMenuItem
            // 
            this.checkCurrentExperimentsToolStripMenuItem.Name = "checkCurrentExperimentsToolStripMenuItem";
            this.checkCurrentExperimentsToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.checkCurrentExperimentsToolStripMenuItem.Text = "Check existing experiments";
            this.checkCurrentExperimentsToolStripMenuItem.Click += new System.EventHandler(this.checkCurrentExperimentsToolStripMenuItem_Click_1);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scanDefinitionToolStripMenuItem,
            this.hardklorToolStripMenuItem,
            this.includingListToolStripMenuItem});
            this.toolStripDropDownButton1.Enabled = false;
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(79, 22);
            this.toolStripDropDownButton1.Text = "Parameters";
            // 
            // scanDefinitionToolStripMenuItem
            // 
            this.scanDefinitionToolStripMenuItem.Enabled = false;
            this.scanDefinitionToolStripMenuItem.Name = "scanDefinitionToolStripMenuItem";
            this.scanDefinitionToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.scanDefinitionToolStripMenuItem.Text = "Scan Definition";
            // 
            // hardklorToolStripMenuItem
            // 
            this.hardklorToolStripMenuItem.Enabled = false;
            this.hardklorToolStripMenuItem.Name = "hardklorToolStripMenuItem";
            this.hardklorToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.hardklorToolStripMenuItem.Text = "DDAp";
            // 
            // includingListToolStripMenuItem
            // 
            this.includingListToolStripMenuItem.Enabled = false;
            this.includingListToolStripMenuItem.Name = "includingListToolStripMenuItem";
            this.includingListToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.includingListToolStripMenuItem.Text = "Including list";
            // 
            // groupBox_hybrid
            // 
            this.groupBox_hybrid.Controls.Add(this.richTextBox_message);
            this.groupBox_hybrid.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox_hybrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_hybrid.Location = new System.Drawing.Point(0, 25);
            this.groupBox_hybrid.Name = "groupBox_hybrid";
            this.groupBox_hybrid.Size = new System.Drawing.Size(625, 506);
            this.groupBox_hybrid.TabIndex = 37;
            this.groupBox_hybrid.TabStop = false;
            this.groupBox_hybrid.Text = "Message Box";
            // 
            // richTextBox_message
            // 
            this.richTextBox_message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_message.Location = new System.Drawing.Point(3, 16);
            this.richTextBox_message.Name = "richTextBox_message";
            this.richTextBox_message.Size = new System.Drawing.Size(619, 487);
            this.richTextBox_message.TabIndex = 0;
            this.richTextBox_message.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 531);
            this.Controls.Add(this.groupBox_hybrid);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.Text = "DDA plus prototype";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox_hybrid.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkCurrentExperimentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem scanDefinitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hardklorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem includingListToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox_hybrid;
        private System.Windows.Forms.RichTextBox richTextBox_message;
    }
}
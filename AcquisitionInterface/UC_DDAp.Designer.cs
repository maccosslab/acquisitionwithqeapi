﻿namespace AcquisitionInterface
{
    partial class UC_DDAp
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox_place_ms1_after_ms2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_Nms2 = new System.Windows.Forms.TextBox();
            this.textBox_nMS1 = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_place_ms1_after_ms2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBox_Nms2);
            this.groupBox2.Controls.Add(this.textBox_nMS1);
            this.groupBox2.Location = new System.Drawing.Point(23, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(357, 131);
            this.groupBox2.TabIndex = 72;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acquisition setting";
            // 
            // textBox_place_ms1_after_ms2
            // 
            this.textBox_place_ms1_after_ms2.Location = new System.Drawing.Point(234, 61);
            this.textBox_place_ms1_after_ms2.Name = "textBox_place_ms1_after_ms2";
            this.textBox_place_ms1_after_ms2.Size = new System.Drawing.Size(40, 20);
            this.textBox_place_ms1_after_ms2.TabIndex = 62;
            this.textBox_place_ms1_after_ms2.Text = "20";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Place a MS1 After N MS2:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 13);
            this.label1.TabIndex = 59;
            this.label1.Text = "Update candidate list after acquiring N MS2 scans:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Persistent Peptide Feature in N MSs: ";
            // 
            // textBox_Nms2
            // 
            this.textBox_Nms2.Location = new System.Drawing.Point(298, 34);
            this.textBox_Nms2.Name = "textBox_Nms2";
            this.textBox_Nms2.Size = new System.Drawing.Size(40, 20);
            this.textBox_Nms2.TabIndex = 60;
            this.textBox_Nms2.Text = "40";
            // 
            // textBox_nMS1
            // 
            this.textBox_nMS1.Location = new System.Drawing.Point(234, 90);
            this.textBox_nMS1.Name = "textBox_nMS1";
            this.textBox_nMS1.Size = new System.Drawing.Size(40, 20);
            this.textBox_nMS1.TabIndex = 54;
            this.textBox_nMS1.Text = "2";
            // 
            // UC_DDAp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Name = "UC_DDAp";
            this.Size = new System.Drawing.Size(439, 377);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox_place_ms1_after_ms2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_Nms2;
        private System.Windows.Forms.TextBox textBox_nMS1;
    }
}

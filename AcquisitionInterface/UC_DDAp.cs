﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public partial class UC_DDAp : WizardUCBase
    {
        public UC_DDAp()
        {
            InitializeComponent();
        }
        private Parameter _parameter = null;
        public override void SetupDefaultParameters()
        {
            _parameter = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);
            textBox_Nms2.Text = _parameter.UpdateFrequency.ToString();
            textBox_place_ms1_after_ms2.Text = _parameter.PlaceAMSoneAfterNMS2.ToString();
            textBox_nMS1.Text = _parameter.PeptideInNMSone.ToString();
        }
        public override bool UpdateSetting()
        {
            int updateFrequency = 0;
            int afterNms2 = 0;
            int persistScan = 0;

            if (int.TryParse(textBox_Nms2.Text, out updateFrequency))
            {
                if (updateFrequency < 10)
                    _warning.AppendLine("please give a intenger number higher or equal to 10");
            }
            else
                _warning.AppendLine("higher or equal to 10");
            if (!int.TryParse(textBox_place_ms1_after_ms2.Text, out afterNms2))
                _warning.AppendLine("after N ms2");
            if (!int.TryParse(textBox_nMS1.Text, out persistScan))
                _warning.AppendLine("persistent peptide in N ms one");

            _parameter.UpdateFrequency = updateFrequency;
            _parameter.PlaceAMSoneAfterNMS2 = afterNms2;
            _parameter.PeptideInNMSone = persistScan;
            if (_warning.Length > 0)
                return false;
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Thermo.Interfaces.ExactiveAccess_V1.Control.Acquisition.Workflow;
using Thermo.Interfaces.ExactiveAccess_V1.Control.InstrumentValues;
using Thermo.Interfaces.ExactiveAccess_V2.Control;
using Thermo.Interfaces.InstrumentAccess_V1;
using Thermo.Interfaces.InstrumentAccess_V1.Control;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Acquisition;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Acquisition.Modes;
using Thermo.Interfaces.InstrumentAccess_V1.Control.InstrumentValues;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Methods;
using Thermo.Interfaces.InstrumentAccess_V1.Control.Scans;
using Thermo.Interfaces.InstrumentAccess_V1.MsScanContainer;
using AcquisitionMethod;
using mHardklor_2;
using System.Threading;


namespace AcquisitionInterface
{
    public partial class MainForm : Form
    {

        private bool _testmod = false;

        public MainForm()
        {
            InitializeComponent();

            _logfile = AppDomain.CurrentDomain.BaseDirectory + @"/order_logfile.txt";
            _logSW = TextWriter.Synchronized(new StreamWriter(_logfile));

            if (!_testmod)
            {
                try
                {
                    ConnectInstrument();
                    if (InstrumentObject.CreateSuccfully)
                    {
                        EventSetting();
                        this.Closing += OnClosing;
                        AddingMessage("\nConnect to instrument succfully\n");
                    }
                    else
                    {
                        AddingMessage("\n!!!CAN NOT CONNECT TO INSTRUMENT!!!\n");
                    }
                }
                catch (Exception e)
                {
                    AddingMessage("Error ocurred when connecting to instrument\n");
                    AddingMessage(e.Message);
                }
            }
            else
            {
                AddingMessage("\n----Testing mode----\n");
            }

            //CheckBundaries();

            _baseDirectory = AppDomain.CurrentDomain.BaseDirectory + @"WorkingFolder\";
            if (!Directory.Exists(_baseDirectory))
                Directory.CreateDirectory(_baseDirectory);
            ExperimentManager.GetInstance().Setfilefolder(_baseDirectory);

        }

        string _testfile = AppDomain.CurrentDomain.BaseDirectory + @"\precursorMZlist.txt";

        #region variable setting 

        private InstrumentInstance InstrumentObject;
        private bool _inAcquisition = false;
        private Types.ScanOrder _scanOrder = Types.ScanOrder.MS;
        private bool _outputWhenScanArrived = true;
        private bool _testingMode = false;
        private string _rawfilename = "";

        // log
        private string _logfile = "";
        private TextWriter _logSW;

        // real stuff
        private object _scanlocker = new object();
        private object _scanqueuelocker = new object();
        private IAcuqisition _acquisitionMethod;
        private Types.AcuqisitionMethodType _acquisitionType = Types.AcuqisitionMethodType.None;
        private int _precursorID = 0;
        private Stopwatch _timer = null;
        private bool _reverseOrder = false;

        private int _scanNo = 0;
        private Queue<IMsScan> ScanAnalysisQueue = new Queue<IMsScan>();
        private string _identifierTag = "DDAp";
        private bool _outMS2Info = false;

        private Parameter _currentParameter;
        #endregion

        private string _baseDirectory = "";
        private double _timelimit = 1;

        
        #region some stuff


        private void ConnectInstrument()
        {
            InstrumentObject = new InstrumentInstance();
        }


        private static readonly object _syncObject = new object();
        private void AddingMessage(string message)
        {
            if (richTextBox_message.InvokeRequired)
            {
                richTextBox_message.BeginInvoke(new Action(delegate { AddingMessage(message); }));
                return;
            }
            //Console.Write(message);
            richTextBox_message.AppendText(message);
            lock (_syncObject)
            {
                _logSW.Write(message);
            }
        }
        #endregion


        private void TestBinSize()
        {
            Parameter parameter = new Parameter();
            parameter.startMZ = 500d;
            parameter.endMZ = 900d;
            parameter.windowWidth = 10d;
            List<Bin> _diaWindows = new List<Bin>();

            double width = 2d;
            
                Dictionary<double, List<Bin>> _analyticDIAs = new Dictionary<double, List<Bin>>();
                List<double> binBundaries = parameter.mzBundaries;
                for (int i = 0; i < binBundaries.Count - 1; i++)
                {
                    Bin b = new Bin();
                    b.lowerBound = binBundaries[i];
                    b.upperBound = binBundaries[i + 1];
                    b.ScanType = Types.ScanType.sDIA;
                    _diaWindows.Add(b);
                    // consective scans
                    //_consecutive.Add(_diaWindows.Count - 1, new Queue<int>());
                }
                for (int i = 0; i < _diaWindows.Count; i++)
                {
                    Bin sbin = _diaWindows[i];
                    int target = (int)((sbin.lowerBound + sbin.upperBound) / 2d);

                    double lowmz = Math.Floor(sbin.lowerBound);
                    double highmz = Math.Floor(sbin.upperBound);

                    List<double> bundaries = IsoloationWindowCalculator.mzBundaries(lowmz, highmz, width);
                    List<Bin> abins = new List<Bin>();
                    for (int idx = 0; idx < bundaries.Count - 1; idx++)
                    {
                        Bin abin = new Bin();
                        abin.upperBound = bundaries[idx + 1];
                        abin.lowerBound = bundaries[idx];
                        abin.ScanType = Types.ScanType.aDIA;
                        abin.targetCharge = 2;
                        abins.Add(abin);
                    }
                    _analyticDIAs.Add(target, abins);
                }
        }


        #region Important part

        
        void scanContainer_AcquisitionStreamOpening(object sender, MsAcquisitionOpeningEventArgs e)
        {

            string rawname = "projectName";
            e.SpecificInformation.TryGetValue("RawFileName", out rawname);
            _rawfilename = Path.GetFileNameWithoutExtension(rawname);
            //string[] info = _rawfilename.Split('_');

            // the default value is false
            _reverseOrder = false; 
            
            AddingMessage("====== DETECT A NEW ACQUISITION ======\n");


            if (ExperimentManager.GetInstance().ContainExperimentSetting(_rawfilename))
            {
                _inAcquisition = true;
                _currentParameter = ExperimentManager.GetInstance().GetParameter(_rawfilename);
                _acquisitionType = _currentParameter.AcquisitionMethod;
                
            }
            else
                _inAcquisition = false;

            if (_acquisitionType == Types.AcuqisitionMethodType.None)
                _inAcquisition = false;


            // this is a temporarily method for test tDDAp

            //end test



            if (_inAcquisition)
            {
                string timelimit = "";
                e.SpecificInformation.TryGetValue("ExpectedRuntime", out timelimit);
                _timelimit = double.Parse(timelimit);

                AddingMessage("\nFile: " + _rawfilename + " will be collected using "+_acquisitionType.ToString()+" Method");
                AddingMessage("\nAcquisition time: " + _timelimit.ToString() + " mins\n");
                AddingMessage("\n===================================================================\n\n");
            }
            else
            {
                AddingMessage("\nFile: " + _rawfilename + " does not have definded DDAp or DIADA setting");
                AddingMessage("\nSpectral data will be collected based the definations in Theromo method file");
                AddingMessage("\n===================================================================\n\n");
            }
        }

        
        private void StartAcquisition()
        {

            if (_acquisitionType == Types.AcuqisitionMethodType.FeatureVotes)
            {
                _acquisitionMethod = new FeatureVoting(_rawfilename, _currentParameter);
            }
            else if (_acquisitionType == Types.AcuqisitionMethodType.TargetDDAp)
            {
                List<Ion> targetList = Function.ParseTargetList(_testfile);
                _acquisitionMethod = new TargetDDAp(_rawfilename, targetList, _currentParameter);
            }
            else if (_acquisitionType == Types.AcuqisitionMethodType.DIADA)
            {
                _acquisitionMethod = new DIADA(_rawfilename, _currentParameter);
                // create acquisition object to perform all detection and calculation
            }
            else if (_acquisitionType == Types.AcuqisitionMethodType.DIAIA)
            {
                _acquisitionMethod = new DIAIA(_rawfilename, _currentParameter);
                AddingMessage("new a DIAIA acquistion object\n");
            }
            else if (_acquisitionType == Types.AcuqisitionMethodType.mDDA)
            {
                _acquisitionMethod = new mDDAv3(_rawfilename, _currentParameter);
                AddingMessage("starting self-implemented DDA\n");
            }

            //AddingMessage("After new object");
            if (_acquisitionMethod == null)
            {
                AddingMessage("\nThe acuqisition method object is null, please check and rerun it again\n\n");
                return;
            }
            AddingMessage("\nLog file: " + _rawfilename + ".log\n");

            _timer = new Stopwatch();
            _timer.Start();
            ReportTime(_timer.Elapsed, "\nSTART Acquisition");
 
            sBin startBin = new Bin();
            startBin.upperBound = double.Parse(_currentParameter.MSoneSetting.LastMass);
            startBin.lowerBound = double.Parse(_currentParameter.MSoneSetting.FirstMass);
            startBin.ScanType = Types.ScanType.RegularMSone;
            SetUpScan(startBin, _currentParameter.MSoneSetting); // set one ms one
        }

 

        
        private void ScanContainerOnMsScanArrived(object sender, MsScanEventArgs msScanEventArgs)
        {
            object order = new object(); // this may also affect the testing result
            //string answer = "";
            string number = "";
            string sN = "";

            try
            {
                
                #endregion
                if (_inAcquisition)
                {
                    IMsScan newscan = msScanEventArgs.GetScan();
                    
                    #region Get scan information
                    
                    MsScanInformationSource common = MsScanInformationSource.Common;
                    newscan.CommonInformation.TryGetValue("Scan", out sN, ref common);
                    MsScanInformationSource Source = MsScanInformationSource.Trailer;
                    newscan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
                    newscan.CommonInformation.TryGetRawValue("MSOrder", out order);
                    int accessid = int.Parse(number);

                    //_scanNo++;
                    if (_outputWhenScanArrived)
                    {

                        if (_acquisitionType != Types.AcuqisitionMethodType.DIADA && _acquisitionType != Types.AcuqisitionMethodType.DIAIA)
                            ReportTime(_timer.Elapsed, "Get a scan--MS Level: " + order.ToString() + "\tScan No:" + sN + "\t Access Id: " + number);
                        else 
                        {
                            if (accessid == (int)Types.ScanType.sDIA)
                            {
                                ReportTime(_timer.Elapsed, "Get a survey DIA scan-\tScan No:" + sN);
                            }
                            else if (accessid == (int)Types.ScanType.aDIA)
                            {
                                ReportTime(_timer.Elapsed, "Get a Analytic DIA scan-\tScan No:" + sN);
                            }
                            else if (accessid == (int)Types.ScanType.SIM)
                            {
                                ReportTime(_timer.Elapsed, "Get a SIM scan-\tScan No:" + sN);
                            }
                            else if (accessid == (int)Types.ScanType.RegularMSone)
                            {
                                ReportTime(_timer.Elapsed, "Get a MS one scan-\tScan No:" + sN);
                            }
                            else if (accessid == (int)Types.ScanType.RegularMStwo)
                            {
                                ReportTime(_timer.Elapsed, "Get a MS two scan-\tScan No:" + sN);
                            }
                            else if (accessid == -1 || accessid == 0)
                                ReportTime(_timer.Elapsed, "Get an instrument scan-\tScan No:" + sN);
                            else
                            {
                                ReportTime(_timer.Elapsed, "Get a MS two scan-\tScan No:" + sN);
                            }
                        }
                        
                    }
                    if (StopAcquisition(_timer.Elapsed))
                    {
                        AddingMessage("the acquisition should be stopped -- ScanContainerOnMsScanArrived");
                        return;
                    }
                    
                    if (_acquisitionMethod.QualifyForAnalysis(accessid))
                    {
                        lock (_scanqueuelocker)
                        {
                            ScanAnalysisQueue.Enqueue(newscan);
                            InvokeSpectraAnalysisAndBinAssignment(); // 
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AddingMessage("Error in scan arrived action -- " + e.Message + "\n");
                AddingMessage("\tSource: " + e.Source + "\n");
                AddingMessage("\tStackTrace: " + e.StackTrace + "\n");
            }
        }



        private void InvokeSpectraAnalysisAndBinAssignment() 
        {
            //AddingMessage("InvokeSpectraAnalysis\n");
            Thread runjob = new Thread(DoAnalysisOnScan);
            runjob.Start();
        }

        // continue checking 
        private object _analysislock = new object();
        private void DoAnalysisOnScan()
        {
            if (ScanAnalysisQueue.Count == 0)
                return;
            if (StopAcquisition(_timer.Elapsed))
            {
                AddingMessage("the acqusition should be stopped now -- Do analysis on arrived scans");
                return;
            }
            //AddingMessage("Analyze MS one scan\n");

            // every thread will wait here until previous analysis is finished
            // this make sure everything is in the right order.
            lock (_analysislock) 
            {
                if (ScanAnalysisQueue.Count > 0)
                {
                    IMsScan scan = ScanAnalysisQueue.Dequeue();
                    string number = "";
                    MsScanInformationSource Source = MsScanInformationSource.Trailer;
                    scan.SpecificInformation.TryGetValue("Access Id:", out number, ref Source);
                    int precursorID = int.Parse(number); // set up _precursor id
                    
                    //ReportTime(_timer.Elapsed);
                    //AddingMessage("Analyze MS1 scan -- AccessID: " + precursorID + "\n");
                    
                    Stopwatch watch = new Stopwatch();
                    watch.Start();

                    //if (StopAcquisition(_timer.Elapsed))
                    //    return;
                    try
                    {
                        // write here
                        _acquisitionMethod.DoAnalysis(scan);
                        // invoke scan action manually -- this is weird.. and I forgot whey I wrote this
                        if (_acquisitionType != Types.AcuqisitionMethodType.DIAIA && _acquisitionType != Types.AcuqisitionMethodType.DIADA)
                            ScanManager_CanAcceptNextCustomScan(new object(), new EventArgs());

                        TimeSpan total = watch.Elapsed;
                        
                        //AddingMessage("Wait for scan analysis and bin assignment - " + total.Milliseconds + " milliseconds\n");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error in forloop -- " + e.Message, e);
                        AddingMessage("Error -- " + e.Message + "\n");
                        AddingMessage("\tSource: " + e.Source + "\n");
                        AddingMessage("\tStackTrace: " + e.StackTrace + "\n");
                        TimeSpan total = watch.Elapsed;
                        AddingMessage("Wait for scan analysis and bin assignment - " + total.Milliseconds + " milliseconds\n");
                    }
                    scan.Dispose();
                    scan = null;
                }
            }
        }


        // 2015/01/20 -- need to fix all other acquisition method, because I want to replace NextType with NextBin in the IAcquisition..
        private void ScanManager_CanAcceptNextCustomScan(object sender, EventArgs e)
        {
            //AddingMessage("look for an user defined scan\n");
            //AddingMessage("Before check the timer and in Acquisition -- " + _inAcquisition + "\n");
            //AddingMessage("in acquisition -- " + _inAcquisition + "\n");



            if (StopAcquisition(_timer.Elapsed) || !_inAcquisition)
            {
                AddingMessage("The acquisition should be stopped now -- ScanManager_CanAcceptNextCustomScan");
                return;
            }

            

            if (_inAcquisition)
            {
                //AddingMessage("CAN ACCEPT NEXT CUSTOM SCAN NOW!\n");
                //Bin selectedBin = new Bin();
                Bin selectedBin = null;
                MSScanParameter setting = null;
                bool writeSim = false;

                lock (_scanlocker)
                {
                    selectedBin = _acquisitionMethod.NextIsolationBin;
                    if (selectedBin == null)
                    {
                        ReportTime(_timer.Elapsed, "No scan definition in the queue");
                        return;
                    }
                    switch (selectedBin.ScanType)
                    {
                        case Types.ScanType.SIM:
                            setting = _currentParameter.SIMSetting;
                            //selectedBin = _acquisitionMethod.NextIsolationBin;
                            writeSim = true;
                            break;
                        case Types.ScanType.RegularMSone:
                            setting = _currentParameter.MSoneSetting;
                            //selectedBin = new Bin();
                            break;
                        case Types.ScanType.RegularMStwo:
                            setting = _currentParameter.MStwoSetting;
                            //selectedBin = _acquisitionMethod.NextIsolationBin;
                            break;
                        case Types.ScanType.sDIA:
                            setting = _currentParameter.sDIASetting;
                            //selectedBin = _acquisitionMethod.NextIsolationBin;
                            break;
                        case Types.ScanType.aDIA:
                            setting = _currentParameter.aDIASetting;
                            break;
                        case Types.ScanType.None:
                            return;
                    }

                    #region old version
                    //switch (_acquisitionMethod.NextScanType)
                    //{
                    //    case Types.ScanType.SIM:
                    //        setting = _currentParameter.SIMSetting;
                    //        selectedBin = _acquisitionMethod.NextIsolationBin;
                    //        writeInfo = true;
                    //        break;
                    //    case Types.ScanType.RegularMSone:
                    //        setting = _currentParameter.MSoneSetting;
                    //        selectedBin = new Bin();
                    //        selectedBin.lowerBound = double.Parse(_currentParameter.MSoneSetting.FirstMass);
                    //        selectedBin.upperBound = double.Parse(_currentParameter.MSoneSetting.LastMass);
                    //        selectedBin.accID = 111;
                    //        break;
                    //    case Types.ScanType.RegularMStwo:
                    //        setting = _currentParameter.MStwoSetting;
                    //        selectedBin = _acquisitionMethod.NextIsolationBin;
                    //        break;
                    //    case Types.ScanType.DIA:
                    //        setting = _currentParameter.DIASetting;
                    //        selectedBin = _acquisitionMethod.NextIsolationBin;
                    //        break;
                    //    case Types.ScanType.None:
                    //        return;
                    //}
                    #endregion
                    
                    if (writeSim)
                    {
                        ReportTime(_timer.Elapsed,"New SIM scan- Range:" + selectedBin.lowerBound + "-" + selectedBin.upperBound);
                    }
                    //else
                    //    ReportTime(_timer.Elapsed, "Next Scan Type: " + selectedBin.ScanType.ToString());

                    if (setting != null)
                    {
                        SetUpScan(selectedBin, setting);
                    }
                    else
                        ReportTime(_timer.Elapsed, "Parameter is null --- No desired scan type, please check code");
                }
            }
        }

        private bool StopAcquisition(TimeSpan timespan)
        {
            int hr = timespan.Hours;
            int sec = timespan.Seconds;
            int min = timespan.Minutes;
            double ss = sec / 60d;
            min = min + hr * 60;
            //AddingMessage("Time: " + min.ToString() + "\tsec " + ss + " ---- " + _timelimit);
            if ((min + ss) >= _timelimit)
            {
                AddingMessage("Stop Acquisition ---" + min.ToString() + "\tSec:" + sec.ToString() + " Final Second: " + ss.ToString() + "\n");
                return true;
            }
            else
                return false;
        }


        #endregion



        #region place scans

        private void SetUpScan(Bin nextBin, MSScanParameter setting)
        {
            ICustomScan cscan = InstrumentObject.ScanManager.CreateCustomScan();

            if (nextBin.parentScan > 0)  // default is negative numbers
                cscan.RunningNumber = (nextBin.parentScan)*10;
            else
                cscan.RunningNumber = (int)nextBin.ScanType;

            setting.IsolationLow = nextBin.lowerBound.ToString();
            setting.IsolationHigh = nextBin.upperBound.ToString();
            
            if (nextBin.ScanType == Types.ScanType.RegularMStwo || 
                nextBin.ScanType == Types.ScanType.aDIA || 
                nextBin.ScanType == Types.ScanType.sDIA)
            {
                //double low = double.Parse(setting.IsolationLow);
                //double high = double.Parse(setting.IsolationHigh);
                double low = nextBin.lowerBound;
                double high = nextBin.upperBound;
                double target = (low + high)/2d;
                int charge = 2;
                if (setting.Charge < 2)
                    charge = 2;
                else if (setting.Charge > 5)
                    charge = 5;
                setting.FirstMass = Parameter.FirstMZ(target, charge);
                setting.LastMass = Parameter.LastMZ(target, charge);
                //50.0-6000.0	First m/z, lower edge of displayed spectral range
            }
            //else if (nextBin.ScanType == Types.ScanType.sDIA)
            //{
            //    setting.FirstMass = "100";
            //    setting.LastMass = "1000";
            //}
            
            DefineMSScans.SetUpScanParameter(cscan, setting);


            

            try
            {
                bool success = InstrumentObject.ScanManager.SetCustomScan(cscan);
            }
            catch (Exception e)
            {
                AddingMessage("\n!!An exception occured when seting a customized MS scan!!\n");
                AddingMessage(e.Message + "\n");
                AddingMessage(e.StackTrace + "\n");
                AddingMessage(e.Source + "\n");
                AddingMessage("\n===================\n");
            }
        }



        #endregion

        #region action events
        object obj = new object();
        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            if (InstrumentObject.Access.Connected)
            {
                setStdby_Click(obj, new EventArgs()); // set instrument in standby
            }
            DropEvents();
            CloseLogFiles();
            if (_acquisitionMethod != null)
                _acquisitionMethod.Close();
            InstrumentObject.Dispose();
            
        }

        private void CloseLogFiles()
        {
            lock (_syncObject)
            {
                if (_logSW != null)
                    _logSW.Close();
            }
        }

        private void EventSetting()
        {
            IMsScanContainer scanContainer = InstrumentObject.ScanContainer;
            scanContainer.MsScanArrived += ScanContainerOnMsScanArrived;
            scanContainer.AcquisitionStreamOpening += new EventHandler<MsAcquisitionOpeningEventArgs>(scanContainer_AcquisitionStreamOpening);
            scanContainer.AcquisitionStreamClosing += new EventHandler(scanContainer_AcquisitionStreamClosing);
            InstrumentObject.Access.ErrorsArrived += new EventHandler<Thermo.Interfaces.InstrumentAccess_V1.ErrorsArrivedEventArgs>(Instrument_ErrorsArrived);
            InstrumentObject.Access.ConnectionChanged += new EventHandler(AccessOnConnectionChanged);
            InstrumentObject.Acquisition.StateChanged += new EventHandler<Thermo.Interfaces.InstrumentAccess_V1.Control.Acquisition.StateChangedEventArgs>(Acquisition_StateChanged);
            if (InstrumentObject.ScanManager != null)
            {
                InstrumentObject.ScanManager.CanAcceptNextCustomScan += new EventHandler(ScanManager_CanAcceptNextCustomScan);
            }
            if (scanContainer == null || InstrumentObject.ScanManager == null)
                AddingMessage("null object, please check");
        }

        private void DropEvents()
        {
            InstrumentObject.Access.ErrorsArrived -= new EventHandler<Thermo.Interfaces.InstrumentAccess_V1.ErrorsArrivedEventArgs>(Instrument_ErrorsArrived);
            InstrumentObject.Access.ConnectionChanged -= new EventHandler(AccessOnConnectionChanged);
            InstrumentObject.Acquisition.StateChanged -= new EventHandler<Thermo.Interfaces.InstrumentAccess_V1.Control.Acquisition.StateChangedEventArgs>(Acquisition_StateChanged);

            if (InstrumentObject.ScanManager != null)
                InstrumentObject.ScanManager.CanAcceptNextCustomScan -= new EventHandler(ScanManager_CanAcceptNextCustomScan);

            IMsScanContainer scanContainer = InstrumentObject.ScanContainer;
            scanContainer.MsScanArrived -= ScanContainerOnMsScanArrived;
            scanContainer.AcquisitionStreamOpening -= new EventHandler<MsAcquisitionOpeningEventArgs>(scanContainer_AcquisitionStreamOpening);
            scanContainer.AcquisitionStreamClosing -= new EventHandler(scanContainer_AcquisitionStreamClosing);
        }
     
        

        private void setOn_Click(object sender, EventArgs e)
        {
            IOnMode on = InstrumentObject.Acquisition.CreateOnMode();
            InstrumentObject.Acquisition.SetMode(on);
        }

        private void setStdby_Click(object sender, EventArgs e)
        {
            IStandbyMode standby = InstrumentObject.Acquisition.CreateStandbyMode();
            InstrumentObject.Acquisition.SetMode(standby);
        }

        private void AccessOnConnectionChanged(object sender, EventArgs eventArgs)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("The instrument ");
            if (InstrumentObject.Access.Connected)
                sb.AppendLine("is connected");
            else
                sb.AppendLine("is dropped..");

            AddingMessage(sb.ToString());
        }


        /// <summary>
        /// When errors from the instrument arrived we publish them.
        /// </summary>
        /// <param name="sender">doesn't matter</param>
        /// <param name="e">contains the messages to be dumped</param>
        private void Instrument_ErrorsArrived(object sender, ErrorsArrivedEventArgs e)
        {
            foreach (IError error in e.Errors)
            {
                Console.WriteLine("ERROR>>> " + error.Content.ToUpper());
                AddingMessage("ERROR>>> " + error.Content.ToUpper());
            }
        }

        void scanContainer_AcquisitionStreamClosing(object sender, EventArgs e)
        {
            //_inAcquisition = false;
            AddingMessage("\n==END OF ACQUISITION==\n");
            _inAcquisition = false;
            if (_acquisitionMethod != null)
            {
                _acquisitionMethod.Close();
            }
            if (InstrumentObject.Access.Connected)
            {
                IStandbyMode standby = InstrumentObject.Acquisition.CreateStandbyMode();
                InstrumentObject.Acquisition.SetMode(standby);
            }
        }

        private void Acquisition_StateChanged(object sender, Thermo.Interfaces.InstrumentAccess_V1.Control.Acquisition.StateChangedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("\n=========Acquisition is changed=======\n");
            sb.AppendLine("State Changed: state is " + e.State.SystemState);
            sb.AppendLine("State Changed: mode is " + e.State.SystemMode);
            
            if (e.State.SystemState == InstrumentState.Running ||  e.State.SystemState == InstrumentState.Initializing)
            {
                if (_inAcquisition)
                {
                    _scanNo = 0;
                    StartAcquisition();
                }
            }
            else if (e.State.SystemState == InstrumentState.PostRun )
            {
                _inAcquisition = false;
            }
            //AddingMessage(sb.ToString());
            //AddingMessage("Last Open Raw file name: " + e.State.LastOpenedRawFileName + "\n");
        }
        #endregion


        #region UI Actions

        private void ReportTime(TimeSpan ts, string saywhat)
        {
            ReportTime(ts);
            AddingMessage(saywhat+"\n");
        }

        private void ReportTime(TimeSpan ts)
        {
            string elapsedTime = String.Format("H:{0:00},Min:{1:00},Sec:{2:00},MS:{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds);
            //string elapsedTime = String.Format("Sec:{0:00},MS:{1:00}", ts.Seconds, ts.Milliseconds);
            AddingMessage("Time: " + elapsedTime+":\t");
        }


        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            wizardForm settingForm = new wizardForm();
            //settingForm.SetFileFolder(_baseDirectory);
            settingForm.Show();
        }



        #endregion


        #region Previous parameter setting UI
        
        //private void scanDefinitionToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    Form showform = new Form();
        //    showform.Size = new Size(_scandefinitionUI.Size.Width+10, _scandefinitionUI.Size.Height+10);
        //    showform.Controls.Add(_scandefinitionUI);
        //    showform.Show();
        //    showform.FormClosing += new FormClosingEventHandler(showform_FormClosing);
        //}
        //private void hardklorToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    Form showform = new Form();
        //    showform.Size = new Size(_DDApParameterUI.Size.Width + 10, _DDApParameterUI.Size.Height + 10);
        //    showform.Controls.Add(_DDApParameterUI);
        //    showform.Show();
        //    showform.FormClosing +=new FormClosingEventHandler(showform_FormClosing);
        //}
        //void showform_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    if (((Form)sender).Controls.Contains(_scandefinitionUI))
        //        ((Form)sender).Controls.Remove(_scandefinitionUI);
        //    if (((Form)sender).Controls.Contains(_DDApParameterUI))
        //        ((Form)sender).Controls.Remove(_DDApParameterUI);
        //}

        #endregion 


        #region logs

        private void LogParameterSetting(string filename, Parameter parameter)
        {
            string filepath = AppDomain.CurrentDomain.BaseDirectory + filename + ".para";
            StreamWriter sw = new StreamWriter(filepath);

            sw.WriteLine("===MS One Scan Setting===");
            sw.WriteLine("m/z range: " + parameter.MSoneSetting.FirstMass + " - " + parameter.MSoneSetting.LastMass);
            sw.WriteLine("MaxIT: " + parameter.MSoneSetting.MaxIT);
            sw.WriteLine("AGC Target: " + parameter.MSoneSetting.AGC_Target);
            sw.WriteLine("Resolution: " + parameter.MSoneSetting.Resolution);
            sw.WriteLine("\n===MS Two Scan Setting===");
            sw.WriteLine("NCE: " + parameter.MStwoSetting.NCE);
            sw.WriteLine("MaxIT: " + parameter.MStwoSetting.MaxIT);
            sw.WriteLine("AGC Target: " + parameter.MStwoSetting.AGC_Target);
            sw.WriteLine("Resolution: " + parameter.MStwoSetting.Resolution);

            sw.WriteLine("\n========== Acquisition Setting =========");
            sw.WriteLine("m/z range: "+parameter.startMZ.ToString() +" - "+parameter.endMZ.ToString());
            sw.WriteLine("isolation window width: "+parameter.windowWidth.ToString());

            if (_acquisitionType == Types.AcuqisitionMethodType.DIADA)
            {
                sw.WriteLine("\n========== DIADA Setting =========");
                sw.WriteLine("Reporter ions");
                StringBuilder mzs = new StringBuilder();
                mzs.Append(_currentParameter.ReportIons[0]);
                //double mz in _currentParameter.ReportIons
                for (int i = 1; i < _currentParameter.ReportIons.Count; i++)
                    mzs.Append(";" + _currentParameter.ReportIons[i]);

                sw.WriteLine(mzs.ToString());
                sw.WriteLine("Match all:" + _currentParameter.MatchAllRepoeters.ToString());
                sw.WriteLine("Reporter intensity thershould:" + _currentParameter.FragmentIntThreshold);
            }
            else if (_acquisitionType == Types.AcuqisitionMethodType.DIAIA)
            {

            }
            else
            {
                //sw.WriteLine("\n========== DDAp Setting =========");
                //sw.WriteLine("DDAp Algorithm: " + _acquisitionType.ToString());
                //if (parameter.UseMonoMZToDecideBins)
                //    sw.WriteLine("Used MonoMZ to do bin assignment");
                //else
                //    sw.WriteLine("Used baseMZ to do bin assignment");
                //sw.WriteLine("Place a MS one after every N MS2: " + parameter.PlaceAMSoneAfterNMS2.ToString());
                //if (_acquisitionType == Types.AcuqisitionMethodType.BinVotes ||
                //    _acquisitionType == Types.AcuqisitionMethodType.FeatureVotes ||
                //    _acquisitionType == Types.AcuqisitionMethodType.UpdateList ||
                //    _acquisitionType == Types.AcuqisitionMethodType.DynamicMS ||
                //    _acquisitionType == Types.AcuqisitionMethodType.BinCharge
                //    )
                //{
                //    sw.WriteLine("Force to update candidate list after N MS2: " + parameter.UpdateFrequency.ToString());
                //    if (_acquisitionType == Types.AcuqisitionMethodType.BinVotes ||
                //        _acquisitionType == Types.AcuqisitionMethodType.FeatureVotes ||
                //        _acquisitionType == Types.AcuqisitionMethodType.DynamicMS ||
                //        _acquisitionType == Types.AcuqisitionMethodType.BinCharge)
                //        sw.WriteLine("Persistent Peptide in N MS1 scans: " + parameter.PeptideInNMSone.ToString());
                //}
            }
            sw.Close();
        }


        #endregion


        private void CheckBundaries()
        {
            //Parameter testParameter = new Parameter();
            //testParameter.startMZ = 600d;
            //testParameter.endMZ = 800d;
            //testParameter.windowWidth = 10d;
            //List<double> bundaries = testParameter.mzBundaries;
            //List<Bin> _diaWindows = new List<Bin>();
            //for (int i = 0; i < bundaries.Count - 1; i++)
            //{
            //    Bin b = new Bin();
            //    b.lowerBound = bundaries[i];
            //    b.upperBound = bundaries[i + 1];
            //    b.ScanType = Types.ScanType.DIA;
            //    _diaWindows.Add(b);
            //    Console.WriteLine(_diaWindows.Count);
            //}
        }
        private void checkCurrentExperimentsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form showForm = new Form();
            UC_experiments uc = new UC_experiments();
            showForm.Size = new Size(uc.Size.Width+10,uc.Size.Height+10);
            showForm.Controls.Add(uc);
            uc.Dock = DockStyle.Fill;
            showForm.Show();

        }

        



        











        #region API testing region
        //private void button_cancel_Click(object sender, EventArgs e)
        //{
        //    ChangeResult whatisthis = InstrumentObject.Acquisition.CancelAcquisition();

        //}

        //private void button_pause_Click(object sender, EventArgs e)
        //{
        //    if (InstrumentObject.Acquisition != null)
        //    {
        //        bool canChange = InstrumentObject.Acquisition.CanPause;
        //        AddingMessage("The current acquisition can be paused by API: " + canChange.ToString() + "\n");
        //        bool success = InstrumentObject.Acquisition.Pause();
        //        if (success)
        //            _inAcquisition = false;
        //        AddingMessage("attempt to pause acquisition: " + success.ToString() + "\n");
        //    }
        //    else
        //    {
        //        AddingMessage("No acquisition object now\n");
        //    }

        //}

        //private void button_resume_Click(object sender, EventArgs e)
        //{
        //    if (InstrumentObject.Acquisition != null)
        //    {
        //        bool canChange = InstrumentObject.Acquisition.CanResume;
        //        AddingMessage("The current acquisition can be resumed by API: " + canChange.ToString() + "\n");
        //        bool success = InstrumentObject.Acquisition.Resume();
        //        if (success)
        //            _inAcquisition = true;
        //        AddingMessage("attempt to resume acquisition: " + success.ToString() + "\n");
        //    }
        //    else
        //    {
        //        AddingMessage("No acquisition object now\n");
        //    }
        //}

        //private void button_new_Click(object sender, EventArgs e)
        //{

        //    if (_acqType == Types.AcquisitionType.Count)
        //    {
        //        IAcquisitionWorkflow scanCountWorkflow = InstrumentObject.Acquisition.CreateAcquisitionLimitedByCount(20);
        //        // the IacquisitionWorkflow.Trigger is always null.
        //        scanCountWorkflow.RawFileName = AppDomain.CurrentDomain.BaseDirectory + @"\Acquire20scans.raw";
        //        InstrumentObject.Acquisition.StartAcquisition(scanCountWorkflow);
        //    }
        //    else if (_acqType == Types.AcquisitionType.Time)
        //    {
        //        TimeSpan time = new TimeSpan(0, 0, 0, 30);
        //        IAcquisitionLimitedByTime timeWorkflow =
        //            InstrumentObject.Acquisition.CreateAcquisitionLimitedByDuration(time);
        //        timeWorkflow.RawFileName = AppDomain.CurrentDomain.BaseDirectory + @"\30secondRun.raw";
        //        InstrumentObject.Acquisition.StartAcquisition(timeWorkflow);
        //    }

        //}
        #endregion

    }
}

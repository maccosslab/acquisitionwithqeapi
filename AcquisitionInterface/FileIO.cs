﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public class FileIO
    {
        public static bool ReadTargetDDApList(string filepath, out List<Ion> ions )
        {
            StreamReader sr = new StreamReader(filepath);
            bool parsed = true;
            ions = new List<Ion>();
            List<Ion> targets = new List<Ion>();
            
            while (sr.Peek() != -1)
            {
                string oneline = sr.ReadLine();
                string[] content = oneline.Split('\t');
                double mz;
                int charge =1;
                if (!double.TryParse(content[0],out mz))
                    parsed = false;
                if (content.Length > 1)
                {
                    if (int.TryParse(content[1], out charge))
                    {
                        if (!(charge > 0))
                            parsed = false;
                    }
                }
                
                
                if (!parsed)
                    break;
                Ion ion = new Ion();
                ion.Mz = mz;
                ion.Charge = charge;
                targets.Add(ion);
            }
            sr.Close();
            if (parsed)
                ions = targets;

            return parsed;
        }

     
    }
}

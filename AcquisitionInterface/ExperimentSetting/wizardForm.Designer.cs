﻿
using System.Windows.Forms;

namespace AcquisitionInterface
{
    partial class wizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.wizard1 = new CristiPotlog.Controls.Wizard();
            this.w_close = new CristiPotlog.Controls.WizardPage();
            this.wizardPage1 = new CristiPotlog.Controls.WizardPage();
            this.w_panel = new CristiPotlog.Controls.WizardPage();
            this.wizard1.SuspendLayout();
            this.SuspendLayout();
            // 
            // wizard1
            // 
            this.wizard1.Controls.Add(this.w_close);
            this.wizard1.Controls.Add(this.wizardPage1);
            this.wizard1.Controls.Add(this.w_panel);
            this.wizard1.Location = new System.Drawing.Point(0, 0);
            this.wizard1.Name = "wizard1";
            this.wizard1.Pages.AddRange(new CristiPotlog.Controls.WizardPage[] {
            this.w_panel,
            this.wizardPage1,
            this.w_close});
            this.wizard1.Size = new System.Drawing.Size(596, 468);
            this.wizard1.TabIndex = 0;
            this.wizard1.Finish += new System.EventHandler(this.wizard1_Finish);
            this.wizard1.AfterSwitchPages += new CristiPotlog.Controls.Wizard.AfterSwitchPagesEventHandler(this.wizard1_AfterSwitchPages);
            this.wizard1.BeforeSwitchPages += new CristiPotlog.Controls.Wizard.BeforeSwitchPagesEventHandler(this.wizard1_BeforeSwitchPages);
            this.wizard1.Cancel += new System.ComponentModel.CancelEventHandler(this.wizard1_Cancel);
            // 
            // w_close
            // 
            this.w_close.BackColor = System.Drawing.Color.Turquoise;
            this.w_close.Location = new System.Drawing.Point(0, 0);
            this.w_close.Name = "w_close";
            this.w_close.Size = new System.Drawing.Size(596, 420);
            this.w_close.Style = CristiPotlog.Controls.WizardPageStyle.Finish;
            this.w_close.TabIndex = 15;
            // 
            // wizardPage1
            // 
            this.wizardPage1.Location = new System.Drawing.Point(0, 0);
            this.wizardPage1.Name = "wizardPage1";
            this.wizardPage1.Size = new System.Drawing.Size(596, 420);
            this.wizardPage1.Style = CristiPotlog.Controls.WizardPageStyle.Custom;
            this.wizardPage1.TabIndex = 16;
            // 
            // w_panel
            // 
            this.w_panel.Location = new System.Drawing.Point(0, 0);
            this.w_panel.Name = "w_panel";
            this.w_panel.Size = new System.Drawing.Size(596, 420);
            this.w_panel.Style = CristiPotlog.Controls.WizardPageStyle.Custom;
            this.w_panel.TabIndex = 14;
            // 
            // wizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 468);
            this.Controls.Add(this.wizard1);
            this.Name = "wizardForm";
            this.Text = "W_comPara";
            this.wizard1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CristiPotlog.Controls.Wizard wizard1;
        private CristiPotlog.Controls.WizardPage w_panel;
        private CristiPotlog.Controls.WizardPage w_close;
        private CristiPotlog.Controls.WizardPage wizardPage1;
        

    }
}
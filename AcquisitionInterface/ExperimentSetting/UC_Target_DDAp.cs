﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public class UC_Target_DDAp : WizardUCBase
    {
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton_ignore;
        private System.Windows.Forms.RadioButton radioButton_yes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_toleranceUnit;
        private System.Windows.Forms.TextBox textBox_tolerance;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_filename;
        private System.Windows.Forms.Button button_open;
        private System.Windows.Forms.Label label1;

        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton_ignore = new System.Windows.Forms.RadioButton();
            this.radioButton_yes = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_toleranceUnit = new System.Windows.Forms.ComboBox();
            this.textBox_tolerance = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_filename = new System.Windows.Forms.TextBox();
            this.button_open = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton_ignore);
            this.groupBox1.Controls.Add(this.radioButton_yes);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBox_toleranceUnit);
            this.groupBox1.Controls.Add(this.textBox_tolerance);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox_filename);
            this.groupBox1.Controls.Add(this.button_open);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(32, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 233);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Target DDAp setting";
            // 
            // radioButton_ignore
            // 
            this.radioButton_ignore.AutoSize = true;
            this.radioButton_ignore.Checked = true;
            this.radioButton_ignore.Location = new System.Drawing.Point(268, 179);
            this.radioButton_ignore.Name = "radioButton_ignore";
            this.radioButton_ignore.Size = new System.Drawing.Size(55, 17);
            this.radioButton_ignore.TabIndex = 10;
            this.radioButton_ignore.TabStop = true;
            this.radioButton_ignore.Text = "Ignore";
            this.radioButton_ignore.UseVisualStyleBackColor = true;
            this.radioButton_ignore.CheckedChanged += new System.EventHandler(this.radioButton_ignore_CheckedChanged);
            // 
            // radioButton_yes
            // 
            this.radioButton_yes.AutoSize = true;
            this.radioButton_yes.Location = new System.Drawing.Point(154, 179);
            this.radioButton_yes.Name = "radioButton_yes";
            this.radioButton_yes.Size = new System.Drawing.Size(43, 17);
            this.radioButton_yes.TabIndex = 9;
            this.radioButton_yes.TabStop = true;
            this.radioButton_yes.Text = "Yes";
            this.radioButton_yes.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Match charge states:";
            // 
            // comboBox_toleranceUnit
            // 
            this.comboBox_toleranceUnit.FormattingEnabled = true;
            this.comboBox_toleranceUnit.Location = new System.Drawing.Point(124, 135);
            this.comboBox_toleranceUnit.Name = "comboBox_toleranceUnit";
            this.comboBox_toleranceUnit.Size = new System.Drawing.Size(72, 21);
            this.comboBox_toleranceUnit.TabIndex = 7;
            // 
            // textBox_tolerance
            // 
            this.textBox_tolerance.Location = new System.Drawing.Point(168, 101);
            this.textBox_tolerance.Name = "textBox_tolerance";
            this.textBox_tolerance.Size = new System.Drawing.Size(100, 20);
            this.textBox_tolerance.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Tolerance unit:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "M/z matching tolerance:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Filename:";
            // 
            // textBox_filename
            // 
            this.textBox_filename.Location = new System.Drawing.Point(113, 63);
            this.textBox_filename.Name = "textBox_filename";
            this.textBox_filename.Size = new System.Drawing.Size(210, 20);
            this.textBox_filename.TabIndex = 2;
            // 
            // button_open
            // 
            this.button_open.Location = new System.Drawing.Point(158, 29);
            this.button_open.Name = "button_open";
            this.button_open.Size = new System.Drawing.Size(62, 23);
            this.button_open.TabIndex = 1;
            this.button_open.Text = "Open";
            this.button_open.UseVisualStyleBackColor = true;
            this.button_open.Click += new System.EventHandler(this.button_open_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Load list of target m/z:";
            // 
            // UC_Target_DDAp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox1);
            this.Name = "UC_Target_DDAp";
            this.Size = new System.Drawing.Size(444, 376);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        public UC_Target_DDAp()
        {
            InitializeComponent();
            CurrentPageType = PageType.TargetDDAp;
        }

        private Parameter _parameter = null;
        public override void SetupDefaultParameters()
        {
            _parameter = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);
            comboBox_toleranceUnit.Items.Clear();
            comboBox_toleranceUnit.Items.Add("Da");
            comboBox_toleranceUnit.Items.Add("ppm");
            string unit = _parameter.pepToleranceUnit.ToString();
            if (unit == "Da")
                comboBox_toleranceUnit.SelectedIndex = 0;
            else if (unit == "ppm")
                comboBox_toleranceUnit.SelectedIndex = 1;

            radioButton_ignore.Checked = !_parameter.MatchChargeState;
            radioButton_yes.Checked = _parameter.MatchChargeState;
            //base.SetupDefaultParameters();
        }

        public override PageType NextPageType()
        {
            return PageType.CommonParameter;
        }

        public override bool UpdateSetting()
        {
            double tolerance;
            List<Ion> ions = new List<Ion>();
            if (_filepath != "")
            {
                bool parsed = FileIO.ReadTargetDDApList(_filepath, out ions);
                if (!parsed)
                    _warning.AppendLine("the input file does not in right format");
            }
            else
                _warning.AppendLine("please input a filename");

            if (_warning.Length > 0)
                return false;
            string unit = comboBox_toleranceUnit.Items[comboBox_toleranceUnit.SelectedIndex].ToString();
            switch (unit)
            {
                case"Da":
                    _parameter.pepToleranceUnit = Parameter.ToleranceUnit.Da;
                    break;
                case "ppm":
                    _parameter.pepToleranceUnit = Parameter.ToleranceUnit.ppm;
                    break;
                default:
                    _parameter.pepToleranceUnit = Parameter.ToleranceUnit.ppm;
                    break;
            }

            return base.UpdateSetting();
        }



        private string _filepath = "";
        private void button_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Multiselect = false;

            if (op.ShowDialog() == DialogResult.OK)
            {
                _filepath = op.FileName;
                string filename = Path.GetFileNameWithoutExtension(_filepath);
                textBox_filename.Text = filename;
            }
        }

        private void radioButton_ignore_CheckedChanged(object sender, EventArgs e)
        {
            radioButton_ignore.Checked = !_parameter.MatchChargeState;
            radioButton_yes.Checked = _parameter.MatchChargeState;
        }

        


        
    }
}

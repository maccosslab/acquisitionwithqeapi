﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public class UC_ScanSetting : WizardUCBase
    {
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox_delay;
        private System.Windows.Forms.TextBox textBox_agc;
        private System.Windows.Forms.TextBox textBox_NCE;
        private System.Windows.Forms.TextBox textBox_maxIT;
        private System.Windows.Forms.TextBox textBox_Resolution; 
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox_scanOrder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private Label label_th;
        private TextBox textBox_iso_window;
        private Label label_isowindow;
        private Label label2;
        private Label label_zto;
        private TextBox textBox_maxZ;
        private TextBox textBox_minZ;
        private Label label_pep_z;
        private TextBox textBox_pepInt;
        private Label label_pep_int;
        private Label label_to;
        private TextBox textBox_mzmax;
        private TextBox textBox_mzmin;
        private Label label_mzrange;
        private System.Windows.Forms.Label label3;
    
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_to = new System.Windows.Forms.Label();
            this.textBox_mzmax = new System.Windows.Forms.TextBox();
            this.textBox_mzmin = new System.Windows.Forms.TextBox();
            this.label_mzrange = new System.Windows.Forms.Label();
            this.textBox_pepInt = new System.Windows.Forms.TextBox();
            this.label_pep_int = new System.Windows.Forms.Label();
            this.label_zto = new System.Windows.Forms.Label();
            this.textBox_maxZ = new System.Windows.Forms.TextBox();
            this.textBox_minZ = new System.Windows.Forms.TextBox();
            this.label_pep_z = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_th = new System.Windows.Forms.Label();
            this.textBox_iso_window = new System.Windows.Forms.TextBox();
            this.label_isowindow = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox_delay = new System.Windows.Forms.TextBox();
            this.textBox_agc = new System.Windows.Forms.TextBox();
            this.textBox_NCE = new System.Windows.Forms.TextBox();
            this.textBox_maxIT = new System.Windows.Forms.TextBox();
            this.textBox_Resolution = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox_scanOrder = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_to);
            this.groupBox1.Controls.Add(this.textBox_mzmax);
            this.groupBox1.Controls.Add(this.textBox_mzmin);
            this.groupBox1.Controls.Add(this.label_mzrange);
            this.groupBox1.Controls.Add(this.textBox_pepInt);
            this.groupBox1.Controls.Add(this.label_pep_int);
            this.groupBox1.Controls.Add(this.label_zto);
            this.groupBox1.Controls.Add(this.textBox_maxZ);
            this.groupBox1.Controls.Add(this.textBox_minZ);
            this.groupBox1.Controls.Add(this.label_pep_z);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label_th);
            this.groupBox1.Controls.Add(this.textBox_iso_window);
            this.groupBox1.Controls.Add(this.label_isowindow);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.textBox_delay);
            this.groupBox1.Controls.Add(this.textBox_agc);
            this.groupBox1.Controls.Add(this.textBox_NCE);
            this.groupBox1.Controls.Add(this.textBox_maxIT);
            this.groupBox1.Controls.Add(this.textBox_Resolution);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.comboBox_scanOrder);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(41, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(440, 331);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scan Event Setting";
            // 
            // label_to
            // 
            this.label_to.AutoSize = true;
            this.label_to.Location = new System.Drawing.Point(199, 208);
            this.label_to.Name = "label_to";
            this.label_to.Size = new System.Drawing.Size(16, 13);
            this.label_to.TabIndex = 80;
            this.label_to.Text = "to";
            // 
            // textBox_mzmax
            // 
            this.textBox_mzmax.Location = new System.Drawing.Point(221, 205);
            this.textBox_mzmax.Name = "textBox_mzmax";
            this.textBox_mzmax.Size = new System.Drawing.Size(42, 20);
            this.textBox_mzmax.TabIndex = 79;
            // 
            // textBox_mzmin
            // 
            this.textBox_mzmin.Location = new System.Drawing.Point(151, 205);
            this.textBox_mzmin.Name = "textBox_mzmin";
            this.textBox_mzmin.Size = new System.Drawing.Size(42, 20);
            this.textBox_mzmin.TabIndex = 78;
            // 
            // label_mzrange
            // 
            this.label_mzrange.AutoSize = true;
            this.label_mzrange.Location = new System.Drawing.Point(24, 208);
            this.label_mzrange.Name = "label_mzrange";
            this.label_mzrange.Size = new System.Drawing.Size(59, 13);
            this.label_mzrange.TabIndex = 77;
            this.label_mzrange.Text = "M/z range:";
            // 
            // textBox_pepInt
            // 
            this.textBox_pepInt.Location = new System.Drawing.Point(183, 296);
            this.textBox_pepInt.Name = "textBox_pepInt";
            this.textBox_pepInt.Size = new System.Drawing.Size(64, 20);
            this.textBox_pepInt.TabIndex = 76;
            this.textBox_pepInt.Text = "10000";
            // 
            // label_pep_int
            // 
            this.label_pep_int.AutoSize = true;
            this.label_pep_int.Location = new System.Drawing.Point(24, 299);
            this.label_pep_int.Name = "label_pep_int";
            this.label_pep_int.Size = new System.Drawing.Size(150, 13);
            this.label_pep_int.TabIndex = 75;
            this.label_pep_int.Text = "Peptide ion intensity threshold:";
            // 
            // label_zto
            // 
            this.label_zto.AutoSize = true;
            this.label_zto.Location = new System.Drawing.Point(194, 269);
            this.label_zto.Name = "label_zto";
            this.label_zto.Size = new System.Drawing.Size(10, 13);
            this.label_zto.TabIndex = 74;
            this.label_zto.Text = "-";
            // 
            // textBox_maxZ
            // 
            this.textBox_maxZ.Location = new System.Drawing.Point(211, 266);
            this.textBox_maxZ.Name = "textBox_maxZ";
            this.textBox_maxZ.Size = new System.Drawing.Size(36, 20);
            this.textBox_maxZ.TabIndex = 73;
            this.textBox_maxZ.Text = "4";
            // 
            // textBox_minZ
            // 
            this.textBox_minZ.Location = new System.Drawing.Point(150, 266);
            this.textBox_minZ.Name = "textBox_minZ";
            this.textBox_minZ.Size = new System.Drawing.Size(38, 20);
            this.textBox_minZ.TabIndex = 72;
            this.textBox_minZ.Text = "2";
            // 
            // label_pep_z
            // 
            this.label_pep_z.AutoSize = true;
            this.label_pep_z.Location = new System.Drawing.Point(24, 269);
            this.label_pep_z.Name = "label_pep_z";
            this.label_pep_z.Size = new System.Drawing.Size(113, 13);
            this.label_pep_z.TabIndex = 71;
            this.label_pep_z.Text = "Peptide charge states:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 349);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 59;
            this.label2.Text = "-";
            // 
            // label_th
            // 
            this.label_th.AutoSize = true;
            this.label_th.Location = new System.Drawing.Point(227, 237);
            this.label_th.Name = "label_th";
            this.label_th.Size = new System.Drawing.Size(20, 13);
            this.label_th.TabIndex = 55;
            this.label_th.Text = "Th";
            // 
            // textBox_iso_window
            // 
            this.textBox_iso_window.Location = new System.Drawing.Point(150, 234);
            this.textBox_iso_window.Name = "textBox_iso_window";
            this.textBox_iso_window.Size = new System.Drawing.Size(71, 20);
            this.textBox_iso_window.TabIndex = 54;
            // 
            // label_isowindow
            // 
            this.label_isowindow.AutoSize = true;
            this.label_isowindow.Location = new System.Drawing.Point(24, 241);
            this.label_isowindow.Name = "label_isowindow";
            this.label_isowindow.Size = new System.Drawing.Size(116, 13);
            this.label_isowindow.TabIndex = 53;
            this.label_isowindow.Text = "Isolation window width:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(222, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 13);
            this.label13.TabIndex = 52;
            this.label13.Text = "ms";
            // 
            // textBox_delay
            // 
            this.textBox_delay.Location = new System.Drawing.Point(150, 174);
            this.textBox_delay.Name = "textBox_delay";
            this.textBox_delay.Size = new System.Drawing.Size(71, 20);
            this.textBox_delay.TabIndex = 51;
            // 
            // textBox_agc
            // 
            this.textBox_agc.Location = new System.Drawing.Point(150, 146);
            this.textBox_agc.Name = "textBox_agc";
            this.textBox_agc.Size = new System.Drawing.Size(71, 20);
            this.textBox_agc.TabIndex = 50;
            // 
            // textBox_NCE
            // 
            this.textBox_NCE.Location = new System.Drawing.Point(150, 118);
            this.textBox_NCE.Name = "textBox_NCE";
            this.textBox_NCE.Size = new System.Drawing.Size(71, 20);
            this.textBox_NCE.TabIndex = 49;
            // 
            // textBox_maxIT
            // 
            this.textBox_maxIT.Location = new System.Drawing.Point(150, 89);
            this.textBox_maxIT.Name = "textBox_maxIT";
            this.textBox_maxIT.Size = new System.Drawing.Size(71, 20);
            this.textBox_maxIT.TabIndex = 48;
            // 
            // textBox_Resolution
            // 
            this.textBox_Resolution.Location = new System.Drawing.Point(150, 61);
            this.textBox_Resolution.Name = "textBox_Resolution";
            this.textBox_Resolution.Size = new System.Drawing.Size(71, 20);
            this.textBox_Resolution.TabIndex = 47;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 32);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 46;
            this.label12.Text = "Scan type:";
            // 
            // comboBox_scanOrder
            // 
            this.comboBox_scanOrder.FormattingEnabled = true;
            this.comboBox_scanOrder.Items.AddRange(new object[] {
            "MS one",
            "MS two"});
            this.comboBox_scanOrder.Location = new System.Drawing.Point(150, 32);
            this.comboBox_scanOrder.Name = "comboBox_scanOrder";
            this.comboBox_scanOrder.Size = new System.Drawing.Size(126, 21);
            this.comboBox_scanOrder.TabIndex = 45;
            this.comboBox_scanOrder.SelectionChangeCommitted += new System.EventHandler(this.comboBox_scanOrder_SelectionChangeCommitted);
            this.comboBox_scanOrder.SelectedIndexChanged += new System.EventHandler(this.comboBox_scanOrder_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(227, 177);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 42;
            this.label11.Text = "second";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 41;
            this.label8.Text = "SingleDelay:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "AGC_Target:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "NCE:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "MaxIT:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "Resolution:";
            // 
            // UC_ScanSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox1);
            this.Name = "UC_ScanSetting";
            this.Size = new System.Drawing.Size(519, 423);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        public UC_ScanSetting()
        {
            InitializeComponent();
            CurrentPageType = PageType.ScanSetting;
        }


        private Parameter _parameter = null;
        private string _mzmin = "100";
        private string _mzmax = "500";
        public override void SetupDefaultParameters()
        {
            //base.SetupDefaultParameters();
            _parameter = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);
            comboBox_scanOrder.Items.Clear();
            
            comboBox_scanOrder.Items.Add("MS1-Full MS");

            if (_parameter.AcquisitionMethod == Types.AcuqisitionMethodType.DIAIA)
            {
                comboBox_scanOrder.Items.Add("Survey DIA");
                comboBox_scanOrder.Items.Add("Analytic DIA");
            }
            else if (_parameter.AcquisitionMethod == Types.AcuqisitionMethodType.DIADA)
            {
                comboBox_scanOrder.Items.Add("SIM");
                comboBox_scanOrder.Items.Add("Survey DIA");
                comboBox_scanOrder.Items.Add("MS2");
            }
            else
                comboBox_scanOrder.Items.Add("MS2");
            comboBox_scanOrder.SelectedIndex = 0;
        }

        public override bool UpdateSetting()
        {
            bool ans = SaveCurrentParameter();
            return ans;
        }

        public override PageType NextPageType()
        {
            return PageType.Summary;
        }

        MSScanParameter _scanParameter = null;

        
        private void comboBox_scanOrder_SelectionChangeCommitted(object sender, EventArgs e)
        {


        }

        private void comboBox_scanOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("after change comboBox");
            // save all the value before changing
            bool _keepSelectedItem = !SaveCurrentParameter();
            if (_keepSelectedItem)
            {
                //MessageBox.Show("Previous selected item: " + comboBox_scanOrder.Items[_previousSelected] + "\tindex" + _previousSelected.ToString());
                //comboBox_scanOrder.SelectedIndex = _previousSelected;
                return;
            }
            else
            {
                string selected = comboBox_scanOrder.Items[comboBox_scanOrder.SelectedIndex].ToString();
                switch (selected)
                {
                    case "MS1-Full MS":
                        _scanParameter = _parameter.MSoneSetting;
                        DefaultSettingForMS1();

                        break;
                    case "MS2":
                        _scanParameter = _parameter.MStwoSetting;
                        DefaultSettingForMS2();
                        DefaultSettingForDDAMS2();

                        break;
                    case "SIM":
                        _scanParameter = _parameter.SIMSetting;
                        DefaultSettingForMS1();
                        DefaultSettingForSIM();
                        break;
                    case "Survey DIA":
                        _scanParameter = _parameter.sDIASetting;
                        DefaultSettingForMS2();
                        break;
                    case "Analytic DIA":
                        _scanParameter = _parameter.aDIASetting;
                        DefaultSettingForMS2();

                        break;
                    default:
                        break;
                }
                UpdateCommonParameters();
            }

            //MessageBox.Show("NEW selected item: " + comboBox_scanOrder.Items[comboBox_scanOrder.SelectedIndex] + "\tindex" + comboBox_scanOrder.SelectedIndex.ToString());
        }

        private void DefaultSettingForMS1()
        {
            
            textBox_NCE.Enabled = false;
            textBox_iso_window.Enabled = false;
            textBox_maxZ.Enabled = false;
            textBox_minZ.Enabled = false;
            textBox_pepInt.Enabled = false;
            textBox_mzmax.Enabled = true;
            textBox_mzmin.Enabled = true;
            textBox_pepInt.Text = "-";
            textBox_NCE.Text = "-";
            textBox_iso_window.Text = "-";
            textBox_maxZ.Text = "0";
            textBox_minZ.Text = "0";
            textBox_mzmax.Text = _parameter.endMZ.ToString();
            textBox_mzmin.Text = _parameter.startMZ.ToString();

        }

        private void DefaultSettingForMS2()
        {
            textBox_NCE.Enabled = true;
            textBox_iso_window.Enabled = true;
            textBox_maxZ.Enabled = false;
            textBox_minZ.Enabled = false;
            textBox_pepInt.Enabled = false;
            textBox_mzmax.Enabled = false;
            textBox_mzmin.Enabled = false;


            textBox_maxZ.Text = "0";
            textBox_minZ.Text = "0";
            textBox_pepInt.Text = "-";
            textBox_iso_window.Text = _scanParameter.IsolationWindowWidth;
            textBox_NCE.Text = _scanParameter.NCE;
            textBox_mzmax.Text = "0";
            textBox_mzmin.Text = "0";
        }
        private void DefaultSettingForDDAMS2()
        {
            textBox_maxZ.Enabled = true;
            textBox_minZ.Enabled = true;
            textBox_pepInt.Enabled = true;
            textBox_maxZ.Text = _parameter.maxCharge.ToString();
            textBox_minZ.Text = _parameter.minCharge.ToString();
            textBox_pepInt.Text = _parameter.IntensityThreshold.ToString();
        }
        private void DefaultSettingForSIM()
        {
            textBox_mzmax.Enabled = false;
            textBox_mzmin.Enabled = false;
            textBox_maxZ.Text = "0";
            textBox_minZ.Text = "0";
        }

        private void UpdateCommonParameters()
        {
            if (_scanParameter != null)
            {
                textBox_Resolution.Text = _scanParameter.Resolution;
                textBox_maxIT.Text = _scanParameter.MaxIT;
                textBox_agc.Text = _scanParameter.AGC_Target;
                textBox_delay.Text = _scanParameter.SingleProcessingDelay.ToString();
                // check all the parameters 
            }
        }

        private bool SaveCurrentParameter()
        {
            _warning = new StringBuilder();
            bool ans = true;

            

            if (_scanParameter != null)
            {
                int value;
                float fvalue;
                double dvalue = 0d;


                if (int.TryParse(textBox_Resolution.Text, out value))
                {
                    if (value <= 150000 && value >= 8000 )
                        _scanParameter.Resolution = textBox_Resolution.Text;
                    else
                        _warning.AppendLine("Resolution should be a number between 8000.0-150000.0");
                }
                else
                    _warning.AppendLine("Resolution should be a number between 8000.0-150000.0");
                if (float.TryParse(textBox_maxIT.Text, out fvalue))
                {
                    //0.03-3000.00
                    if (fvalue >= 0.03 && fvalue <= 3000)
                    {
                        _scanParameter.MaxIT = textBox_maxIT.Text;
                    }
                    else
                        _warning.AppendLine("max IT should be a number between 0.03-3000.00");
                }
                else
                    _warning.AppendLine("max IT should be a number between 0.03-3000.00");

                if (textBox_NCE.Enabled)
                {
                    if (int.TryParse(textBox_NCE.Text, out value))
                    {
                        if (value >= 0)
                            _scanParameter.NCE = textBox_NCE.Text;
                        else
                            _warning.AppendLine("The NCE should be a number higher or equal to 0");
                    }
                    else
                        _warning.AppendLine("The NCE should be a number higher or equal to 0");
                }
                

                if (int.TryParse(textBox_agc.Text, out value))
                {
                    //1-10000000
                    if (value >= 1 && value <= 10000000)
                        _scanParameter.AGC_Target = textBox_agc.Text;
                    else
                        _warning.AppendLine("AGC Target should be a number between 1 to 10000000");
                }
                else
                    _warning.AppendLine("AGC Target should be a number between 1 to 10000000");

                if (double.TryParse(textBox_delay.Text, out dvalue))
                {
                    if (dvalue >= 0)
                        _scanParameter.SingleProcessingDelay = dvalue;
                    else
                        _warning.AppendLine("The single delay should be a number higher or equal to 0");
                }
                else
                    _warning.AppendLine("The single delay should be a number higher or equal to 0");

                if (textBox_mzmax.Enabled && textBox_mzmin.Enabled)
                {
                    double max = 0d;
                    double min = 0d;
                    if (double.TryParse(textBox_mzmax.Text, out max) && double.TryParse(textBox_mzmin.Text, out min))
                    {
                        if (max > min)
                        {
                            _parameter.endMZ = max;
                            _parameter.startMZ = min;
                        }
                        else
                            _warning.Append("The m/z range setting is wrong, please check m/z range is valid.");
                    }
                    else
                    {
                        _warning.Append("The m/z range setting is wrong, please check m/z range is valid.");
                    }
                }

                if (textBox_iso_window.Enabled)
                {
                    if (float.TryParse(textBox_iso_window.Text, out fvalue))
                    {
                        if (fvalue > 1)
                        {
                            _scanParameter.IsolationWindowWidth = textBox_iso_window.Text;
                        }
                        else
                            _warning.AppendLine("the isolation window width for DIA scan should be widder than 1 Th");
                    }
                }

                if (textBox_maxZ.Enabled && textBox_minZ.Enabled)
                {
                    int maxZ = 0;
                    int minZ = 0;
                    if (int.TryParse(textBox_maxZ.Text, out maxZ) && int.TryParse(textBox_minZ.Text, out minZ))
                    {
                        if (maxZ >= minZ && minZ != 0 && maxZ <= 5)
                        {
                            _parameter.maxCharge = maxZ;
                            _parameter.minCharge = minZ;
                        }
                        else
                            _warning.Append("Target charge states are not valid. It should be a value between 1 to 5");
                    }
                    else
                    {
                        _warning.Append("Target charge states are not valid. It should be a value between 1 to 5");
                    }
                }

                
                if (textBox_pepInt.Enabled)
                {
                    if (float.TryParse(textBox_pepInt.Text, out fvalue))
                    {
                        _parameter.IntensityThreshold = fvalue;
                    }
                    else
                        _warning.Append("Peptide intensity threshold is not valid");

                }

            }
            else
            {
                //_warning.AppendLine("parameter object is null....check code");
                return true;
            }

            if (_warning.Length > 0)
            {
                MessageBox.Show(_warning.ToString());
                return false;
            }
            return ans;
        }



    

     
    }
}

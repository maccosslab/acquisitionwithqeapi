﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public class UC_summary : WizardUCBase
    {
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_projectName;
        private System.Windows.Forms.Label label_acquisition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private TextBox textBox_path;
        private System.Windows.Forms.Label label1;
    
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_projectName = new System.Windows.Forms.Label();
            this.label_acquisition = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox_path = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project/rawfile name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Acquisition type:";
            // 
            // label_projectName
            // 
            this.label_projectName.AutoSize = true;
            this.label_projectName.Location = new System.Drawing.Point(186, 73);
            this.label_projectName.Name = "label_projectName";
            this.label_projectName.Size = new System.Drawing.Size(13, 13);
            this.label_projectName.TabIndex = 2;
            this.label_projectName.Text = "--";
            // 
            // label_acquisition
            // 
            this.label_acquisition.AutoSize = true;
            this.label_acquisition.Location = new System.Drawing.Point(186, 105);
            this.label_acquisition.Name = "label_acquisition";
            this.label_acquisition.Size = new System.Drawing.Size(13, 13);
            this.label_acquisition.TabIndex = 3;
            this.label_acquisition.Text = "--";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Parmeter Setting file:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(75, 215);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Change output folder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox_path
            // 
            this.textBox_path.BackColor = System.Drawing.SystemColors.Control;
            this.textBox_path.Location = new System.Drawing.Point(97, 161);
            this.textBox_path.Multiline = true;
            this.textBox_path.Name = "textBox_path";
            this.textBox_path.ReadOnly = true;
            this.textBox_path.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_path.Size = new System.Drawing.Size(338, 48);
            this.textBox_path.TabIndex = 7;
            // 
            // UC_summary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.textBox_path);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label_acquisition);
            this.Controls.Add(this.label_projectName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UC_summary";
            this.Size = new System.Drawing.Size(512, 407);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public UC_summary()
        {
            InitializeComponent();
            //PreviousPageType = PageType.CommonParameter;
            CurrentPageType = PageType.Summary;
            
        }

        
        public override void SetupDefaultParameters()
        {
            Parameter parameter = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);
            label_projectName.Text = ExperimentManager.GetInstance().TempRawfilename;
            label_acquisition.Text = parameter.AcquisitionMethod.ToString();
            textBox_path.Text = ExperimentManager.GetInstance().TempParameterFilepath;
            //label_path.Text = PathinMultipleLine(ExperimentManager.GetInstance().TempParameterFilepath);
        }

        // this is called when "create" is clicked
        public override bool UpdateSetting()
        {
            //FileIO.WriteSettingFile(ExperimentManager.GetInstance().);
            return true;
        }

        public override PageType NextPageType()
        {
            return PageType.Summary;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folder = new FolderBrowserDialog();
            if (folder.ShowDialog() == DialogResult.OK)
            {
                ExperimentManager.GetInstance().Setfilefolder(folder.SelectedPath+@"\");
                string newpath = folder.SelectedPath +@"\"+ Path.GetFileNameWithoutExtension(ExperimentManager.GetInstance().TempParameterFilepath)+".para";

                textBox_path.Text = newpath;
                //label_path.Text = PathinMultipleLine(newpath);
                ExperimentManager.GetInstance().UpdateTempParameterFilepath(newpath);
            }
        }

        private string PathinMultipleLine(string filepath)
        {
            StringBuilder sbp = new StringBuilder();

            int nomore = 50;
            for (int i = 0; i < filepath.Length - nomore; i += nomore)
            {
                int len = nomore;
                if (filepath.Length < i + nomore)
                    len = filepath.Length - i;
                sbp.AppendLine(filepath.Substring(i, len));
            }
            return sbp.ToString();
        }
    }
}

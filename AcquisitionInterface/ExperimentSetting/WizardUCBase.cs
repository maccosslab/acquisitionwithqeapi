﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AcquisitionInterface
{
    public partial class WizardUCBase : UserControl
    {
        public WizardUCBase()
        {
            InitializeComponent();
        }
        public enum PageType
        {
            CreateProject,
            mDDA,
            TargetDDAp,
            ReporterIonSetting,
            CommonParameter,
            SettingSaving,
            Summary,
            ScanSetting,
            Close,

        }

        protected StringBuilder _warning = new StringBuilder();
        public PageType PreviousPageType = PageType.CreateProject;
        public PageType CurrentPageType = PageType.CreateProject;
        
        public virtual PageType NextPageType()
        {
            return PageType.CreateProject;
        }
        public string WarningMessage
        {
            get { return _warning.ToString(); }
        }
        public virtual bool UpdateSetting()
        {
            return false;
        }

        public virtual void SetupDefaultParameters()
        {
            
        }

        
    }


}

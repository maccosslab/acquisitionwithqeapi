﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public partial class UC_mDDA : WizardUCBase
    {
        public UC_mDDA()
        {
            InitializeComponent();
            CurrentPageType = PageType.mDDA;
            DefaultSetting();
        }
        private void DefaultSetting()
        {
            comboBox_baseMono.Items.Add("Monoisotope");
            comboBox_baseMono.Items.Add("Base peak");
            comboBox_baseMono.SelectedIndex = 0;
            comboBox_excludeiso.Items.Add("True");
            comboBox_excludeiso.Items.Add("False");
            comboBox_excludeiso.SelectedIndex = 0;
        }
        private Parameter _parameter = null;
        public override void SetupDefaultParameters()
        {
            _parameter = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);

            textBox_topN.Text = _parameter.TopN.ToString();
            textBox_dysec.Text = _parameter.DynamicExclusion.ToString();
            textBox_dyppm.Text = _parameter.PepTolerance.ToString();
            textBox_minZ.Text = _parameter.minCharge.ToString();
            textBox_maxZ.Text = _parameter.maxCharge.ToString();
            textBox_pepInt.Text = _parameter.IntensityThreshold.ToString();

            if (_parameter.UseMonoMZToDecideBins)
                comboBox_baseMono.SelectedIndex = 0;
            else
                comboBox_baseMono.SelectedIndex = 1;
            if (_parameter.ExcludeIsotopes)
                comboBox_excludeiso.SelectedIndex = 0;
            else
                comboBox_excludeiso.SelectedIndex = 1;
        }

        public override bool UpdateSetting()
        {
            int topN = 0;
            int dysec = 0;
            int dyppm = 0;
            int minZ = 0;
            int maxZ = 0;
            float pepIntThreshold = 0f;

            if (int.TryParse(textBox_topN.Text, out topN))
            {
                if (topN <= 0)
                    _warning.AppendLine("please give valid integer for top N setting (N >0)");
            }
            else
                _warning.AppendLine("please give valid integer for top N setting");


            if (int.TryParse(textBox_dyppm.Text , out dyppm))
            {
                if (dyppm <= 0)
                    _warning.AppendLine("please input a valid mass tolerance value for dynamic exclusion setting (value > 0)");
            }
            else
                _warning.AppendLine("please input a valid mass tolerance value for dynamic exclusion setting");

            if (int.TryParse(textBox_dysec.Text, out dysec))
            {
                if (dysec <= 0)
                    _warning.AppendLine("please input a valid time period for dynamic exclusion setting (value > 0)");
            }
            else
                _warning.AppendLine("please input a valid time period for dynamic exclusion setting");


            if (int.TryParse(textBox_minZ.Text, out minZ) && int.TryParse(textBox_maxZ.Text, out maxZ))
            {
                if (maxZ < minZ)
                    _warning.AppendLine("please input a valid charge state range");
                if (minZ < 1)
                    _warning.AppendLine("the min charge state should higher than +1");
            }
            else
                _warning.AppendLine("please input a valid charge state range");


            if (float.TryParse(textBox_pepInt.Text, out pepIntThreshold))
            {
                if (pepIntThreshold < 0)
                    _warning.AppendLine("the intensity threshould should larger or equal to 0");
            }
            else
                _warning.AppendLine("please input a valid intensity threshold (>=0)");


            if (_warning.Length > 0)
                return false;

            _parameter.TopN = topN;
            _parameter.DynamicExclusion = dysec;
            _parameter.PepTolerance = dyppm;
            _parameter.minCharge = minZ;
            _parameter.maxCharge = maxZ;
            _parameter.IntensityThreshold = pepIntThreshold;

            if (comboBox_baseMono.SelectedIndex == 0)
                _parameter.UseMonoMZToDecideBins = true;
            else
                _parameter.UseMonoMZToDecideBins = false;
            if (comboBox_excludeiso.SelectedIndex == 0)
                _parameter.ExcludeIsotopes = true;
            else
                _parameter.ExcludeIsotopes = false;
            if (_warning.Length > 0)
                return false;
            return true;
        }

        public override PageType NextPageType()
        {
            return PageType.ScanSetting;
        }
    }
}

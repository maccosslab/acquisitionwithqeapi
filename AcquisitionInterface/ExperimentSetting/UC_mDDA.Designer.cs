﻿namespace AcquisitionInterface
{
    partial class UC_mDDA
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_topN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_pepInt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_maxZ = new System.Windows.Forms.TextBox();
            this.textBox_minZ = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_baseMono = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_dyppm = new System.Windows.Forms.TextBox();
            this.textBox_dysec = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_excludeiso = new System.Windows.Forms.ComboBox();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboBox_excludeiso);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textBox_topN);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.textBox_pepInt);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textBox_maxZ);
            this.groupBox3.Controls.Add(this.textBox_minZ);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.comboBox_baseMono);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.textBox_dyppm);
            this.groupBox3.Controls.Add(this.textBox_dysec);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(27, 80);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(357, 263);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "DDA specific parameters";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 13);
            this.label2.TabIndex = 77;
            this.label2.Text = "Exclude isotope peaks as well:";
            // 
            // textBox_topN
            // 
            this.textBox_topN.Location = new System.Drawing.Point(159, 58);
            this.textBox_topN.Name = "textBox_topN";
            this.textBox_topN.Size = new System.Drawing.Size(47, 20);
            this.textBox_topN.TabIndex = 76;
            this.textBox_topN.Text = "20";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 75;
            this.label1.Text = "Isolate Top N peak:    N = ";
            // 
            // textBox_pepInt
            // 
            this.textBox_pepInt.Location = new System.Drawing.Point(183, 221);
            this.textBox_pepInt.Name = "textBox_pepInt";
            this.textBox_pepInt.Size = new System.Drawing.Size(64, 20);
            this.textBox_pepInt.TabIndex = 72;
            this.textBox_pepInt.Text = "10000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 13);
            this.label5.TabIndex = 71;
            this.label5.Text = "Peptide ion intensity threshold:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(187, 187);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 70;
            this.label12.Text = "-";
            // 
            // textBox_maxZ
            // 
            this.textBox_maxZ.Location = new System.Drawing.Point(203, 187);
            this.textBox_maxZ.Name = "textBox_maxZ";
            this.textBox_maxZ.Size = new System.Drawing.Size(36, 20);
            this.textBox_maxZ.TabIndex = 69;
            this.textBox_maxZ.Text = "4";
            // 
            // textBox_minZ
            // 
            this.textBox_minZ.Location = new System.Drawing.Point(142, 187);
            this.textBox_minZ.Name = "textBox_minZ";
            this.textBox_minZ.Size = new System.Drawing.Size(38, 20);
            this.textBox_minZ.TabIndex = 68;
            this.textBox_minZ.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 67;
            this.label6.Text = "Peptide charge states:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(211, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 65;
            this.label4.Text = "sec ";
            // 
            // comboBox_baseMono
            // 
            this.comboBox_baseMono.FormattingEnabled = true;
            this.comboBox_baseMono.Location = new System.Drawing.Point(244, 26);
            this.comboBox_baseMono.Name = "comboBox_baseMono";
            this.comboBox_baseMono.Size = new System.Drawing.Size(94, 21);
            this.comboBox_baseMono.TabIndex = 64;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(213, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "Isolate on base peak or monoisotopic peak:";
            // 
            // textBox_dyppm
            // 
            this.textBox_dyppm.Location = new System.Drawing.Point(211, 154);
            this.textBox_dyppm.Name = "textBox_dyppm";
            this.textBox_dyppm.Size = new System.Drawing.Size(40, 20);
            this.textBox_dyppm.TabIndex = 57;
            this.textBox_dyppm.Text = "10";
            // 
            // textBox_dysec
            // 
            this.textBox_dysec.Location = new System.Drawing.Point(158, 123);
            this.textBox_dysec.Name = "textBox_dysec";
            this.textBox_dysec.Size = new System.Drawing.Size(47, 20);
            this.textBox_dysec.TabIndex = 52;
            this.textBox_dysec.Text = "20";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 126);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(124, 13);
            this.label7.TabIndex = 55;
            this.label7.Text = "Dynamic exclusion (sec):";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(257, 154);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 58;
            this.label8.Text = "ppm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 13);
            this.label9.TabIndex = 56;
            this.label9.Text = "Dynamic exclustion mass tolerance:";
            // 
            // comboBox_excludeiso
            // 
            this.comboBox_excludeiso.FormattingEnabled = true;
            this.comboBox_excludeiso.Location = new System.Drawing.Point(182, 92);
            this.comboBox_excludeiso.Name = "comboBox_excludeiso";
            this.comboBox_excludeiso.Size = new System.Drawing.Size(94, 21);
            this.comboBox_excludeiso.TabIndex = 78;
            // 
            // UC_mDDA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Name = "UC_mDDA";
            this.Size = new System.Drawing.Size(415, 430);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox_pepInt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_maxZ;
        private System.Windows.Forms.TextBox textBox_minZ;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_baseMono;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_dyppm;
        private System.Windows.Forms.TextBox textBox_dysec;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_topN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox_excludeiso;
    }
}

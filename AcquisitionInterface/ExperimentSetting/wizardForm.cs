﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;
using CristiPotlog.Controls;

namespace AcquisitionInterface
{
    public partial class wizardForm : Form
    {
        public wizardForm()
        {
            InitializeComponent();
            CreateUCs();
            this.w_panel.Controls.Add(_wizardDic[WizardUCBase.PageType.CreateProject]);
        }

        Dictionary<WizardUCBase.PageType, WizardUCBase> _wizardDic = new Dictionary<WizardUCBase.PageType, WizardUCBase>();

        private void CreateUCs()
        {
            _wizardDic.Add(WizardUCBase.PageType.CommonParameter, new UC_Common());
            _wizardDic.Add(WizardUCBase.PageType.CreateProject, new UC_newProject());
            _wizardDic.Add(WizardUCBase.PageType.ReporterIonSetting, new UC_diada());
            _wizardDic.Add(WizardUCBase.PageType.ScanSetting, new UC_ScanSetting());
            _wizardDic.Add(WizardUCBase.PageType.TargetDDAp, new UC_Target_DDAp());
            _wizardDic.Add(WizardUCBase.PageType.Summary, new UC_summary());
            _wizardDic.Add(WizardUCBase.PageType.mDDA, new UC_mDDA());
            _wizardDic.Add(WizardUCBase.PageType.SettingSaving, new UC_saveChangedSetting());
            //DDAp
        }

      
        #region button events
        // load new setting
        private void wizard1_AfterSwitchPages(object sender, Wizard.AfterSwitchPagesEventArgs e)
        {
            //MessageBox.Show("after");
        }

        // check all the setting 
        private void wizard1_BeforeSwitchPages(object sender, Wizard.BeforeSwitchPagesEventArgs e)
        {

            // check which is the next panel.
            WizardPage currentPage = this.wizard1.Pages[e.OldIndex];
            WizardUCBase.PageType nextPageType;
            

            // any unexpected error
            if (currentPage.Controls.Count == 0)
            {
                ThrowDefaultErrorMessage();
                e.NewIndex = e.OldIndex;
                return;
            }


            int move = e.NewIndex - e.OldIndex;
            int newpageIdx = 1; // default
            WizardUCBase page = currentPage.Controls[0] as WizardUCBase;
            
            // if move forward
            if (move > 0)
            {
                if (!page.UpdateSetting())
                {
                    string message = page.WarningMessage;
                    if (message.Length > 0)
                        MessageBox.Show(message);
                    nextPageType = page.CurrentPageType;
                    e.NewIndex = e.OldIndex; // stay in the same page
                }
                else
                {
                    if (this.wizard1.NextText == "Finish")
                        wizard1_Finish(sender, e);

                    nextPageType = page.NextPageType();
                    e.NewIndex = newpageIdx;
                    
                    if (_wizardDic.ContainsKey(nextPageType))
                    {
                        _wizardDic[nextPageType].PreviousPageType = page.CurrentPageType;
                        _wizardDic[nextPageType].SetupDefaultParameters();
                        if (this.wizard1.Pages[newpageIdx].Controls.Count > 0)
                            this.wizard1.Pages[newpageIdx].Controls.RemoveAt(0);
                        this.wizard1.Pages[newpageIdx].Controls.Add(_wizardDic[nextPageType]);
                    }
                    else
                        ThrowDefaultErrorMessage();
                }
            }
            else // back to previous page
            {
                if (page.PreviousPageType == WizardUCBase.PageType.CreateProject)
                    newpageIdx = 0;
                else 
                    newpageIdx = 1;

                e.NewIndex = newpageIdx;
                nextPageType = page.PreviousPageType;

                if (_wizardDic.ContainsKey(nextPageType))
                {
                    //_wizardDic[nextPageType].PreviousPageType = page.CurrentPageType;
                    _wizardDic[nextPageType].SetupDefaultParameters();
                    if (this.wizard1.Pages[newpageIdx].Controls.Count > 0)
                        this.wizard1.Pages[newpageIdx].Controls.RemoveAt(0);
                    this.wizard1.Pages[newpageIdx].Controls.Add(_wizardDic[nextPageType]);
                }
                else
                    ThrowDefaultErrorMessage();
            }

            if (nextPageType == WizardUCBase.PageType.Summary)
                this.wizard1.NextText = "Finish";
            else
                this.wizard1.NextText = "Next";

        }

        //private void UpdateWizardControlUC(WizardUCBase.PageType nextPageType, WizardUCBase.PageType currentPageType,WizardPage wizardpage)
        //{
            
        //    Console.WriteLine("number of controls on panel: " + this.w_panel.Controls.Count);

        //    if (_wizardDic.ContainsKey(nextPageType))
        //    {
        //        _wizardDic[nextPageType].PreviousPageType = currentPageType;
        //        _wizardDic[nextPageType].SetupDefaultParameters();
        //        wizardpage.Controls.Add(_wizardDic[nextPageType]);

        //        if (nextPageType == WizardUCBase.PageType.Summary)
        //            this.wizard1.NextText = "Create";
        //        else
        //            this.wizard1.NextText = "Next";
        //    }
        //    else
        //        ThrowDefaultErrorMessage();
        //}


        private void ThrowDefaultErrorMessage()
        {
            MessageBox.Show("I am sorry, but there is something wrong in the program..\n Please contact me to fix it, hyyang@uw.edu");
        }

        

        private void wizard1_Cancel(object sender, CancelEventArgs e)
        {
            //MessageBox.Show("cancel");
        }
        private void wizard1_Finish(object sender, EventArgs e)
        {
            //MessageBox.Show("finish");
            //Console.WriteLine(this.Name);
            //Console.WriteLine(this.wizard1.Parent.Name);
            //Console.WriteLine(this.wizard1.ParentForm.Name);
            //this.DialogResult = DialogResult.Cancel;
            Parameter temp = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);
            ParameterFileIO.WriteCurrentParameter(temp,ExperimentManager.GetInstance().TempParameterFilepath);
            Parameter parameter =  ParameterFileIO.GetParameterSetting(ExperimentManager.GetInstance().TempParameterFilepath);
            //ParameterFileIO.WriteCurrentParameter(parameter, AppDomain.CurrentDomain.BaseDirectory+@"/check_parameters");


            ExperimentManager.GetInstance().AddExperiment(ExperimentManager.GetInstance().TempRawfilename,parameter);
            MessageBox.Show("Set up experiment successfully");
            
            //this.Dispose();
            //this.Close();


            //this.wizard1.Parent.Dispose();

        }
        #endregion




    }
}

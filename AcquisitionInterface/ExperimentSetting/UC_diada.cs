﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public class UC_diada : WizardUCBase
    {
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_intensity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton_all_reporter;
        private System.Windows.Forms.RadioButton radioButton_a_reporter;
        private System.Windows.Forms.TextBox textBox_addreporter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_delete;
        private System.Windows.Forms.ListBox listBox_existingReporters;
        private System.Windows.Forms.Button button_add;
        private TextBox textBox_consec;
        private Label label5;
        private System.Windows.Forms.Label label1;
    
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_intensity = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton_all_reporter = new System.Windows.Forms.RadioButton();
            this.radioButton_a_reporter = new System.Windows.Forms.RadioButton();
            this.textBox_addreporter = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_delete = new System.Windows.Forms.Button();
            this.listBox_existingReporters = new System.Windows.Forms.ListBox();
            this.button_add = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_consec = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_consec);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox_intensity);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.radioButton_all_reporter);
            this.groupBox1.Controls.Add(this.radioButton_a_reporter);
            this.groupBox1.Controls.Add(this.textBox_addreporter);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button_delete);
            this.groupBox1.Controls.Add(this.listBox_existingReporters);
            this.groupBox1.Controls.Add(this.button_add);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(59, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 348);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Report ion setting";
            // 
            // textBox_intensity
            // 
            this.textBox_intensity.Location = new System.Drawing.Point(190, 298);
            this.textBox_intensity.Name = "textBox_intensity";
            this.textBox_intensity.Size = new System.Drawing.Size(89, 20);
            this.textBox_intensity.TabIndex = 16;
            this.textBox_intensity.Text = "10000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 301);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Reporter ion intensity threshold:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Matching criterium:";
            // 
            // radioButton_all_reporter
            // 
            this.radioButton_all_reporter.AutoSize = true;
            this.radioButton_all_reporter.Location = new System.Drawing.Point(262, 236);
            this.radioButton_all_reporter.Name = "radioButton_all_reporter";
            this.radioButton_all_reporter.Size = new System.Drawing.Size(97, 17);
            this.radioButton_all_reporter.TabIndex = 13;
            this.radioButton_all_reporter.TabStop = true;
            this.radioButton_all_reporter.Text = "All reporter ions";
            this.radioButton_all_reporter.UseVisualStyleBackColor = true;
            this.radioButton_all_reporter.CheckedChanged += new System.EventHandler(this.radioButton_all_reporter_CheckedChanged);
            // 
            // radioButton_a_reporter
            // 
            this.radioButton_a_reporter.AutoSize = true;
            this.radioButton_a_reporter.Checked = true;
            this.radioButton_a_reporter.Location = new System.Drawing.Point(66, 236);
            this.radioButton_a_reporter.Name = "radioButton_a_reporter";
            this.radioButton_a_reporter.Size = new System.Drawing.Size(118, 17);
            this.radioButton_a_reporter.TabIndex = 12;
            this.radioButton_a_reporter.TabStop = true;
            this.radioButton_a_reporter.Text = "One of reporter ions";
            this.radioButton_a_reporter.UseVisualStyleBackColor = true;
            this.radioButton_a_reporter.CheckedChanged += new System.EventHandler(this.radioButton_all_reporter_CheckedChanged);
            // 
            // textBox_addreporter
            // 
            this.textBox_addreporter.Location = new System.Drawing.Point(154, 29);
            this.textBox_addreporter.Name = "textBox_addreporter";
            this.textBox_addreporter.Size = new System.Drawing.Size(124, 20);
            this.textBox_addreporter.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Target reporter ions:";
            // 
            // button_delete
            // 
            this.button_delete.Location = new System.Drawing.Point(284, 180);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(75, 23);
            this.button_delete.TabIndex = 9;
            this.button_delete.Text = "Delete";
            this.button_delete.UseVisualStyleBackColor = true;
            // 
            // listBox_existingReporters
            // 
            this.listBox_existingReporters.FormattingEnabled = true;
            this.listBox_existingReporters.Location = new System.Drawing.Point(66, 92);
            this.listBox_existingReporters.Name = "listBox_existingReporters";
            this.listBox_existingReporters.Size = new System.Drawing.Size(293, 82);
            this.listBox_existingReporters.TabIndex = 8;
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(284, 27);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(75, 23);
            this.button_add.TabIndex = 7;
            this.button_add.Text = "Add";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "m/z of new reporter ion:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "# of consecutive scans:";
            // 
            // textBox_consec
            // 
            this.textBox_consec.Location = new System.Drawing.Point(189, 264);
            this.textBox_consec.Name = "textBox_consec";
            this.textBox_consec.Size = new System.Drawing.Size(89, 20);
            this.textBox_consec.TabIndex = 18;
            this.textBox_consec.Text = "3";
            // 
            // UC_diada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox1);
            this.Name = "UC_diada";
            this.Size = new System.Drawing.Size(523, 409);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        public UC_diada()
        {
            InitializeComponent();
            CurrentPageType = PageType.ReporterIonSetting;

        }

        private List<double> _reporters = new List<double>();
        private Parameter _parameter = null;
        private bool _matchall = false;

        public override void SetupDefaultParameters()
        {
            _parameter = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);
            listBox_existingReporters.Items.Clear();
            _reporters.Clear();
            foreach (double reporter in _parameter.ReportIons)
            {
                _reporters.Add(reporter);
                listBox_existingReporters.Items.Add(reporter);
            }
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            double reporter = 0d;
            bool added = false;

            if (double.TryParse(textBox_addreporter.Text, out reporter))
            {
                if (!_reporters.Contains(reporter))
                {
                    if (reporter > 0)
                    {
                        _reporters.Add(reporter);
                        added = true;
                    }
                    else
                        MessageBox.Show("the m/z value of reporter ion should be a positive number");
                }
                else
                    MessageBox.Show("the reoproter ion at m/z " + reporter + " has been added");
            }
            else
                MessageBox.Show("the input value is not a number");

            if (added)
                listBox_existingReporters.Items.Add(reporter);
        }

        
        private void radioButton_all_reporter_CheckedChanged(object sender, EventArgs e)
        {
            _matchall = radioButton_all_reporter.Checked;
        }

        public override bool UpdateSetting()
        {
            // update reporterions
            float intensity = 0;
            if (!float.TryParse(textBox_intensity.Text, out intensity))
            {
                _warning.AppendLine("the intensity threshold is not valid");
            }
            if (_reporters.Count == 0)
                _warning.AppendLine("Need one or more reporters for target");
            
            int no = 1;
            if (!int.TryParse(textBox_consec.Text, out no))
            {
                _warning.AppendLine("# of consectutive scan should be a postive integer");
            }
            else 
            { 
                if (no < 1)
                    _warning.AppendLine("# of consectutive scan should be a postive integer");
            }

            if (_warning.Length > 0)
                return false;


            _parameter.ReportIons.Clear();
            _parameter.ReportIons.AddRange(_reporters);
            _parameter.FragmentIntThreshold = intensity;
            _parameter.MatchAllRepoeters = radioButton_all_reporter.Checked;
            _parameter.ReporterIonInNscans = no;

            return true;
        }

        public override PageType NextPageType()
        {
            //return PageType.CommonParameter;
            return PageType.ScanSetting;
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            double reporter = double.Parse(listBox_existingReporters.Items[listBox_existingReporters.SelectedIndex].ToString());
            _reporters.Remove(reporter);
        }
    }
}

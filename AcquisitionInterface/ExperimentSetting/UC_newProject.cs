﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public class UC_newProject : WizardUCBase
    {
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_selectedfile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_namefile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButton_existing;
        private System.Windows.Forms.Button button_open;
        private GroupBox groupBox2;
        private ComboBox comboBox_acqMethod;
        private Label label6;
        private Label label5;
        private Label label3;
        private Label label1;
        private Label label2;
        private TextBox textBox_projectName;
        private System.Windows.Forms.RadioButton radioButton_new;

        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_open = new System.Windows.Forms.Button();
            this.textBox_selectedfile = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_namefile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButton_existing = new System.Windows.Forms.RadioButton();
            this.radioButton_new = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox_acqMethod = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_projectName = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_open);
            this.groupBox1.Controls.Add(this.textBox_selectedfile);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox_namefile);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.radioButton_existing);
            this.groupBox1.Controls.Add(this.radioButton_new);
            this.groupBox1.Location = new System.Drawing.Point(28, 192);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(494, 154);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parameter Setting";
            // 
            // button_open
            // 
            this.button_open.Location = new System.Drawing.Point(377, 119);
            this.button_open.Name = "button_open";
            this.button_open.Size = new System.Drawing.Size(58, 23);
            this.button_open.TabIndex = 8;
            this.button_open.Text = "Open";
            this.button_open.UseVisualStyleBackColor = true;
            this.button_open.Click += new System.EventHandler(this.button_open_Click);
            // 
            // textBox_selectedfile
            // 
            this.textBox_selectedfile.Location = new System.Drawing.Point(180, 119);
            this.textBox_selectedfile.Name = "textBox_selectedfile";
            this.textBox_selectedfile.Size = new System.Drawing.Size(190, 20);
            this.textBox_selectedfile.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(106, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Selected file:";
            // 
            // textBox_namefile
            // 
            this.textBox_namefile.Location = new System.Drawing.Point(222, 56);
            this.textBox_namefile.Name = "textBox_namefile";
            this.textBox_namefile.Size = new System.Drawing.Size(190, 20);
            this.textBox_namefile.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(105, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Name your setting file:";
            // 
            // radioButton_existing
            // 
            this.radioButton_existing.AutoSize = true;
            this.radioButton_existing.Location = new System.Drawing.Point(84, 92);
            this.radioButton_existing.Name = "radioButton_existing";
            this.radioButton_existing.Size = new System.Drawing.Size(206, 17);
            this.radioButton_existing.TabIndex = 1;
            this.radioButton_existing.TabStop = true;
            this.radioButton_existing.Text = "Load an existing experiment setting file";
            this.radioButton_existing.UseVisualStyleBackColor = true;
            this.radioButton_existing.CheckedChanged += new System.EventHandler(this.radioButton_existing_CheckedChanged);
            // 
            // radioButton_new
            // 
            this.radioButton_new.AutoSize = true;
            this.radioButton_new.Location = new System.Drawing.Point(84, 33);
            this.radioButton_new.Name = "radioButton_new";
            this.radioButton_new.Size = new System.Drawing.Size(198, 17);
            this.radioButton_new.TabIndex = 0;
            this.radioButton_new.TabStop = true;
            this.radioButton_new.Text = "Create an new experiment setting file";
            this.radioButton_new.UseVisualStyleBackColor = true;
            this.radioButton_new.CheckedChanged += new System.EventHandler(this.radioButton_new_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox_acqMethod);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox_projectName);
            this.groupBox2.Location = new System.Drawing.Point(28, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(494, 148);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Project setting";
            // 
            // comboBox_acqMethod
            // 
            this.comboBox_acqMethod.FormattingEnabled = true;
            this.comboBox_acqMethod.Location = new System.Drawing.Point(160, 105);
            this.comboBox_acqMethod.Name = "comboBox_acqMethod";
            this.comboBox_acqMethod.Size = new System.Drawing.Size(197, 21);
            this.comboBox_acqMethod.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Acquisition method:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(81, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(341, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "For example: if the raw file name is DDAp_Yeast.raw, the project name ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(143, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "should be DDAp_Yeast";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Project name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(81, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(352, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "**The project name should be exactly the SAME with your raw file name**";
            // 
            // textBox_projectName
            // 
            this.textBox_projectName.Location = new System.Drawing.Point(130, 19);
            this.textBox_projectName.Name = "textBox_projectName";
            this.textBox_projectName.Size = new System.Drawing.Size(227, 20);
            this.textBox_projectName.TabIndex = 7;
            // 
            // UC_newProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UC_newProject";
            this.Size = new System.Drawing.Size(556, 405);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        private string _settingfile = "";
        private string _projectName = "";

        public UC_newProject()
        {
            
            this.InitializeComponent();
            textBox_projectName.Text = "projectName";
            comboBox_acqMethod.Items.Add("mDDA");
            comboBox_acqMethod.Items.Add("DIAIA");
            comboBox_acqMethod.Items.Add("DIADA");
            //comboBox_acqMethod.Items.Add("DDAp");
            //comboBox_acqMethod.Items.Add("TargetDDAp");
            
            
            
            comboBox_acqMethod.SelectedIndex = 0;
            radioButton_new.Checked = true;
            radioButton_existing.Checked = false;
            button_open.Enabled = false;
            textBox_selectedfile.ReadOnly = true;
            textBox_selectedfile.Enabled = false;
            CurrentPageType = PageType.CreateProject;
        }


        private void radioButton_new_CheckedChanged(object sender, EventArgs e)
        {
            textBox_namefile.Enabled = radioButton_new.Checked;
        }
        private void radioButton_existing_CheckedChanged(object sender, EventArgs e)
        {
            textBox_selectedfile.Enabled = radioButton_existing.Checked;
            textBox_namefile.Enabled = !radioButton_existing.Checked;
            button_open.Enabled = radioButton_existing.Checked;
            if (!radioButton_existing.Checked)
            {
                textBox_selectedfile.Text = "";
                _skipsetting = false;
            }
            
        }

        private bool _skipsetting = false;
        private void button_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Multiselect = false;
            op.Filter = "*.para|*.para";
            if (op.ShowDialog() == DialogResult.OK)
            {
                _settingfile = op.FileName;
                ExperimentManager.GetInstance().Setfilefolder(Path.GetDirectoryName(_settingfile)+@"\");
                //_settingfile = Path.GetFileNameWithoutExtension(_settingfile);
                textBox_selectedfile.Text = Path.GetFileNameWithoutExtension(_settingfile);
                _skipsetting = true;
            }
        }

  
        
        //public string ProjectName
        //{
        //    get { return _projectName; }
        //}

        private Types.AcuqisitionMethodType _selectedTypes;
        public override bool UpdateSetting()
        {
            _warning = new StringBuilder();
            
            string projectName = textBox_projectName.Text.Trim();
            string actype = comboBox_acqMethod.Items[comboBox_acqMethod.SelectedIndex].ToString();
            

            if (actype != "DIADA" && actype != "DIAIA" && actype != "mDDA")
            {
                _warning.AppendLine("DDAp and TargetDDAp are under testing");
                return false;
            }
            _selectedTypes = (Types.AcuqisitionMethodType)Enum.Parse(typeof(Types.AcuqisitionMethodType), actype);
      
            if (textBox_projectName.Text == "")
            {
                _warning.AppendLine("Please input project name -- this should be the same with your rawfile filename");
            }
            else if (ExperimentManager.GetInstance().ContainExperimentSetting(projectName))
            {
                _warning.AppendLine(projectName + " already exist\nThe project name has to be unique, please rename your project");
            }
            else if (radioButton_new.Checked)
            {
                if (textBox_namefile.Text.Trim() == "")
                    _warning.AppendLine("Please name your setting file");
                else
                    _settingfile = ExperimentManager.GetInstance().Filefolder  + Path.GetFileNameWithoutExtension(textBox_namefile.Text) + ".para";
                if (File.Exists(_settingfile))
                {
                    MessageBox.Show("the parameter file: " + Path.GetFileNameWithoutExtension(_settingfile) + " already exist\nThe existing file will be rewrote, or you can input another name");
                    textBox_namefile.Focus();
                }
            }
            else if (radioButton_existing.Checked)
            {
                if (_settingfile == "")
                    _warning.AppendLine("Please select an existing setting file or create a new one");
                else
                {
                    Types.AcuqisitionMethodType exitingType = ParameterFileIO.AcquisitionTypeInSettingFile(_settingfile);
                    if (exitingType == Types.AcuqisitionMethodType.None)
                        _warning.AppendLine("File " + Path.GetFileNameWithoutExtension(_settingfile) + " is not a qualified parameter file\nPlease select a qualified parameter file or create a new one");
                    else if (_selectedTypes != exitingType)
                        _warning.AppendLine("The existing setting file is not for " + comboBox_acqMethod.Items[comboBox_acqMethod.SelectedIndex].ToString() + " experiment\nPlease select another setting file or create a new setting file");
                }
            }
            
            if (_warning.Length > 0)
                return false;


            ExperimentManager.GetInstance().AddTempExperiment(projectName, _settingfile);
            Parameter parameter = ExperimentManager.GetInstance().GetParameter(projectName);
            parameter.AcquisitionMethod = _selectedTypes;

            //_projectName = projectName;
            return true;
        }

        public override PageType NextPageType()
        {
            if (_skipsetting)
                return PageType.Summary;
            else if (_selectedTypes == Types.AcuqisitionMethodType.DIADA || _selectedTypes == Types.AcuqisitionMethodType.DIAIA)
                return PageType.ReporterIonSetting;
            else if (_selectedTypes == Types.AcuqisitionMethodType.TargetDDAp)
                return PageType.TargetDDAp;
            else if (_selectedTypes == Types.AcuqisitionMethodType.mDDA)
                return PageType.mDDA;
            else // DDAplus 
                return PageType.CommonParameter;
        }
    }
}

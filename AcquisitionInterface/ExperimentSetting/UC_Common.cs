﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcquisitionMethod;

namespace AcquisitionInterface
{
    public class UC_Common:WizardUCBase
    {

        #region designer generated code

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox_pepInt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox_maxZ;
        private System.Windows.Forms.TextBox textBox_minZ;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_baseMono;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_mzHigh;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_binSize;
        private System.Windows.Forms.TextBox textBox_mzLow;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;


        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox_pepInt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox_maxZ = new System.Windows.Forms.TextBox();
            this.textBox_minZ = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_baseMono = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_mzHigh = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_binSize = new System.Windows.Forms.TextBox();
            this.textBox_mzLow = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox_pepInt);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textBox_maxZ);
            this.groupBox3.Controls.Add(this.textBox_minZ);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.comboBox_baseMono);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.textBox_mzHigh);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.textBox_binSize);
            this.groupBox3.Controls.Add(this.textBox_mzLow);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(86, 57);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(357, 187);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Isolation criteria";
            // 
            // textBox_pepInt
            // 
            this.textBox_pepInt.Location = new System.Drawing.Point(184, 151);
            this.textBox_pepInt.Name = "textBox_pepInt";
            this.textBox_pepInt.Size = new System.Drawing.Size(64, 20);
            this.textBox_pepInt.TabIndex = 72;
            this.textBox_pepInt.Text = "10000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 13);
            this.label5.TabIndex = 71;
            this.label5.Text = "Peptide ion intensity threshold:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(189, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 70;
            this.label12.Text = "-";
            // 
            // textBox_maxZ
            // 
            this.textBox_maxZ.Location = new System.Drawing.Point(204, 117);
            this.textBox_maxZ.Name = "textBox_maxZ";
            this.textBox_maxZ.Size = new System.Drawing.Size(36, 20);
            this.textBox_maxZ.TabIndex = 69;
            this.textBox_maxZ.Text = "4";
            // 
            // textBox_minZ
            // 
            this.textBox_minZ.Location = new System.Drawing.Point(143, 117);
            this.textBox_minZ.Name = "textBox_minZ";
            this.textBox_minZ.Size = new System.Drawing.Size(38, 20);
            this.textBox_minZ.TabIndex = 68;
            this.textBox_minZ.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 67;
            this.label6.Text = "Peptide charge states:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(253, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 65;
            this.label4.Text = "(m/z range of MS1)";
            // 
            // comboBox_baseMono
            // 
            this.comboBox_baseMono.FormattingEnabled = true;
            this.comboBox_baseMono.Location = new System.Drawing.Point(244, 26);
            this.comboBox_baseMono.Name = "comboBox_baseMono";
            this.comboBox_baseMono.Size = new System.Drawing.Size(94, 21);
            this.comboBox_baseMono.TabIndex = 64;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(186, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 60;
            this.label10.Text = "-";
            // 
            // textBox_mzHigh
            // 
            this.textBox_mzHigh.Location = new System.Drawing.Point(201, 53);
            this.textBox_mzHigh.Name = "textBox_mzHigh";
            this.textBox_mzHigh.Size = new System.Drawing.Size(47, 20);
            this.textBox_mzHigh.TabIndex = 59;
            this.textBox_mzHigh.Text = "1400";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(213, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "Isolate on base peak or monoisotopic peak:";
            // 
            // textBox_binSize
            // 
            this.textBox_binSize.Location = new System.Drawing.Point(143, 87);
            this.textBox_binSize.Name = "textBox_binSize";
            this.textBox_binSize.Size = new System.Drawing.Size(40, 20);
            this.textBox_binSize.TabIndex = 57;
            this.textBox_binSize.Text = "2";
            // 
            // textBox_mzLow
            // 
            this.textBox_mzLow.Location = new System.Drawing.Point(135, 53);
            this.textBox_mzLow.Name = "textBox_mzLow";
            this.textBox_mzLow.Size = new System.Drawing.Size(47, 20);
            this.textBox_mzLow.TabIndex = 52;
            this.textBox_mzLow.Text = "400";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 56);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 55;
            this.label7.Text = "M/z range of interest:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(189, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 13);
            this.label8.TabIndex = 58;
            this.label8.Text = "Th";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 13);
            this.label9.TabIndex = 56;
            this.label9.Text = "Isolation window width:";
            // 
            // UC_Common
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.groupBox3);
            this.Name = "UC_Common";
            this.Size = new System.Drawing.Size(533, 467);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion


        public UC_Common()
        {
            InitializeComponent();
            CurrentPageType = PageType.CommonParameter;
            DefaultSetting();
        }

        private void DefaultSetting()
        {
            comboBox_baseMono.Items.Add("Monoisotope");
            comboBox_baseMono.Items.Add("Base peak");
            comboBox_baseMono.SelectedIndex = 0;
        }

        private Parameter _parameter = null;
        public override void SetupDefaultParameters()
        {
            _parameter = ExperimentManager.GetInstance().GetParameter(ExperimentManager.GetInstance().TempRawfilename);

            textBox_mzLow.Text = _parameter.startMZ.ToString();
            textBox_mzHigh.Text = _parameter.endMZ.ToString();
            textBox_binSize.Text = _parameter.windowWidth.ToString();
            textBox_minZ.Text = _parameter.minCharge.ToString();
            textBox_maxZ.Text = _parameter.maxCharge.ToString();
            textBox_pepInt.Text = _parameter.IntensityThreshold.ToString();
            
            if (_parameter.UseMonoMZToDecideBins)
                comboBox_baseMono.SelectedIndex = 0;
            else
                comboBox_baseMono.SelectedIndex = 1;
        }

        public override bool UpdateSetting()
        {
            double mzlow = 0d;
            double mzhigh = 0d;
            double isowidth = 0d;
            int minZ = 0;
            int maxZ = 0;
            float pepIntThreshold = 0f;
   

            if (double.TryParse(textBox_mzLow.Text, out mzlow) && double.TryParse(textBox_mzHigh.Text, out mzhigh))
            {
                if (mzhigh < mzlow)
                {
                    _warning.AppendLine("plese input a valid m/z range");
                }
            }
            else
                _warning.AppendLine("the input m/z range is not valid");
            
            if (!double.TryParse(textBox_binSize.Text, out isowidth))
            {
                _warning.AppendLine("please input a valid isolation window width");
            }

            if (int.TryParse(textBox_minZ.Text, out minZ) && int.TryParse(textBox_maxZ.Text, out maxZ))
            {
                if (maxZ < minZ)
                    _warning.AppendLine("please input a valid charge state range");
                if (minZ < 1)
                    _warning.AppendLine("the min charge state should higher than +1");
            }
            else
                _warning.AppendLine("please input a valid charge state range");


            if (float.TryParse(textBox_pepInt.Text, out pepIntThreshold))
            {
                if (pepIntThreshold < 0)
                    _warning.AppendLine("the intensity threshould should larger or equal to 0");
            }
            else
                _warning.AppendLine("please input a valid intensity threshold (>=0)");
            
            
            if (_warning.Length > 0)
                return false;

            _parameter.startMZ = mzlow;
            _parameter.endMZ = mzhigh;
            _parameter.windowWidth = isowidth;
            _parameter.minCharge = minZ;
            _parameter.maxCharge = maxZ;
            _parameter.IntensityThreshold = pepIntThreshold;
            
            if (comboBox_baseMono.SelectedIndex == 0)
                _parameter.UseMonoMZToDecideBins = true;
            else
                _parameter.UseMonoMZToDecideBins = false;

            if (_warning.Length > 0)
                return false;
            return true;
        }


        public override PageType NextPageType()
        {
            return PageType.ScanSetting;
        }
    }
}

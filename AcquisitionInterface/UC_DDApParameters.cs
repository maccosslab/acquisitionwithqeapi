﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;

namespace AcquisitionV4
{
    public partial class UC_DDApParameters : UserControl
    {
        public UC_DDApParameters()
        {
            InitializeComponent();
            FillInfo();
        }

        private void FillInfo()
        {
            textBox_Nms2.Text = Parameter.GetInstance().UpDataFrequency.ToString();
            textBox_TopN.Text = Parameter.GetInstance().TopN.ToString();
            textBox_place_ms1_after_ms2.Text = Parameter.GetInstance().PlaceAMSoneAfterNMS2.ToString();
            textBox_exclustion.Text = Parameter.GetInstance().DynamicExclusion.ToString();
            textBox_nMS1.Text = Parameter.GetInstance().PeptideInNMSone.ToString();
            textBox_mzHigh.Text = Parameter.GetInstance().endMZ.ToString();
            textBox_mzLow.Text = Parameter.GetInstance().startMZ.ToString();
            textBox_binSize.Text = Parameter.GetInstance().windowWidth.ToString();
            comboBox_baseMono.Items.Add("Base peak");
            comboBox_baseMono.Items.Add("Monoisotopic peak");

        }

        private void button_update_Click(object sender, EventArgs e)
        {
            StringBuilder warning = new StringBuilder();


            double mzL = 0d;
            double mzH = 0d;
            bool pass = false;
            if (double.TryParse(textBox_mzLow.Text, out mzL) && double.TryParse(textBox_mzHigh.Text,out mzH))
            {
                if (mzL > 0 && mzH > mzL)
                {
                    Parameter.GetInstance().startMZ = mzL;
                    Parameter.GetInstance().endMZ = mzH;
                    pass = true;
                }
            }
            if (!pass)
                warning.AppendLine("Invalid m/z range");

            double bin = 0d;
            pass = false;
            if (double.TryParse(textBox_binSize.Text, out bin))
            {
                if (bin > 0 && bin%1 == 0)
                {
                    Parameter.GetInstance().windowWidth = bin;
                    pass = true;
                }
            }
            if (!pass)
                warning.AppendLine("Invalid bin width, it has to be interger");
                      


            int ms2 = 0;
            pass = false;
            if (int.TryParse(textBox_Nms2.Text, out ms2))
            {
                if (ms2 > 0)
                    pass = true;
            }
            if (!pass)
                warning.AppendLine("Invalid value for update frequence");
            else
                Parameter.GetInstance().UpDataFrequency = ms2;

            pass = false;
            int ms1 = 0;
            if (int.TryParse(textBox_nMS1.Text, out ms1))
            {
                if (ms1 > 0)
                    pass = true;
            }
            if (!pass)
                warning.AppendLine("Invalid value for # of ms1");
            else
                Parameter.GetInstance().PeptideInNMSone = ms1;

            pass = false;
            int topN = 0;
            if (int.TryParse(textBox_TopN.Text, out topN))
            {
                if (topN > 0)
                    pass = true;
            }
            if (!pass)
                warning.AppendLine("Invalid value for TopN");
            else
                Parameter.GetInstance().TopN = topN;

            pass = false;
            double de = 0;
            //if (!Double.TryParse(textBox_exclustion.Text, out de))
            //{
            //    if (de >= 0)
            //        pass = true;
            //}
            //if (!pass)
            //    warning.AppendLine("Invalid value for Dynamic exclution");
            //else
            //    Parameter.GetInstance().DynamicExclusion = de;

            pass = false;
            int afterNMS2 = 0;
            if (int.TryParse(textBox_place_ms1_after_ms2.Text, out afterNMS2))
            {
                if (afterNMS2 >= 0)
                    pass = true;
            }
            if (!pass)
                warning.AppendLine("Invalid value for \"After N MS2\"");
            else
                Parameter.GetInstance().PlaceAMSoneAfterNMS2 = afterNMS2;

            if (comboBox_baseMono.Items[comboBox_baseMono.SelectedIndex].ToString() == "Base peak")
                Parameter.GetInstance().UseMonoMZToDecideBins = false;
            else if (comboBox_baseMono.Items[comboBox_baseMono.SelectedIndex].ToString() == "Monoisotopic peak")
                Parameter.GetInstance().UseMonoMZToDecideBins = true;
            else
                warning.AppendLine("please check the peak selection option");
            


            if (warning.Length>0)
                MessageBox.Show(warning.ToString());

        }


    }
}

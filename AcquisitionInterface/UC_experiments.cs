﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AcquisitionMethod;
namespace AcquisitionInterface
{
    public partial class UC_experiments : UserControl
    {
        public UC_experiments()
        {
            InitializeComponent();
            DefaultSetting();
        }

        private void DefaultSetting()
        {
            dataGridView1.Columns.Add("Project", "Project");
            dataGridView1.Columns.Add("Parameter", "Parameter");

            foreach (string project in  ExperimentManager.GetInstance().Projects())
            {
                int idx = dataGridView1.Rows.Add(new DataGridViewRow());

                dataGridView1.Rows[idx].Cells["Project"].Value = project;
                dataGridView1.Rows[idx].Cells["Parameter"].Value = ExperimentManager.GetInstance().GetParameter(project).ParameterFileName;
            }
        }



    }
}

﻿namespace AcquisitionV4
{
    partial class UC_DDApParameters
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_nMS1 = new System.Windows.Forms.TextBox();
            this.textBox_TopN = new System.Windows.Forms.TextBox();
            this.button_update = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_exclustion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Nms2 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox_place_ms1_after_ms2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_mzHigh = new System.Windows.Forms.TextBox();
            this.textBox_binSize = new System.Windows.Forms.TextBox();
            this.textBox_mzLow = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox_baseMono = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_nMS1
            // 
            this.textBox_nMS1.Location = new System.Drawing.Point(234, 90);
            this.textBox_nMS1.Name = "textBox_nMS1";
            this.textBox_nMS1.Size = new System.Drawing.Size(40, 20);
            this.textBox_nMS1.TabIndex = 54;
            this.textBox_nMS1.Text = "2";
            // 
            // textBox_TopN
            // 
            this.textBox_TopN.Enabled = false;
            this.textBox_TopN.Location = new System.Drawing.Point(70, 25);
            this.textBox_TopN.Name = "textBox_TopN";
            this.textBox_TopN.Size = new System.Drawing.Size(71, 20);
            this.textBox_TopN.TabIndex = 52;
            this.textBox_TopN.Text = "20";
            // 
            // button_update
            // 
            this.button_update.Location = new System.Drawing.Point(275, 423);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(75, 23);
            this.button_update.TabIndex = 46;
            this.button_update.Text = "Update";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Persistent Peptide Feature in N MSs: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 55;
            this.label4.Text = "Top N:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 56;
            this.label5.Text = "Dynamic Exclustion:";
            // 
            // textBox_exclustion
            // 
            this.textBox_exclustion.Enabled = false;
            this.textBox_exclustion.Location = new System.Drawing.Point(133, 56);
            this.textBox_exclustion.Name = "textBox_exclustion";
            this.textBox_exclustion.Size = new System.Drawing.Size(71, 20);
            this.textBox_exclustion.TabIndex = 57;
            this.textBox_exclustion.Text = "100";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(211, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 58;
            this.label6.Text = "ms";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 13);
            this.label1.TabIndex = 59;
            this.label1.Text = "Update List After Acquiring N MS2 scans:";
            // 
            // textBox_Nms2
            // 
            this.textBox_Nms2.Location = new System.Drawing.Point(233, 34);
            this.textBox_Nms2.Name = "textBox_Nms2";
            this.textBox_Nms2.Size = new System.Drawing.Size(40, 20);
            this.textBox_Nms2.TabIndex = 60;
            this.textBox_Nms2.Text = "40";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_exclustion);
            this.groupBox1.Controls.Add(this.textBox_TopN);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(42, 309);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(308, 91);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "mDDA parameters";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox_baseMono);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.textBox_place_ms1_after_ms2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.textBox_Nms2);
            this.groupBox2.Controls.Add(this.textBox_nMS1);
            this.groupBox2.Location = new System.Drawing.Point(44, 127);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(308, 176);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "DDAp parameters";
            // 
            // textBox_place_ms1_after_ms2
            // 
            this.textBox_place_ms1_after_ms2.Location = new System.Drawing.Point(233, 61);
            this.textBox_place_ms1_after_ms2.Name = "textBox_place_ms1_after_ms2";
            this.textBox_place_ms1_after_ms2.Size = new System.Drawing.Size(40, 20);
            this.textBox_place_ms1_after_ms2.TabIndex = 62;
            this.textBox_place_ms1_after_ms2.Text = "20";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Place a MS1 After N MS2:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.textBox_mzHigh);
            this.groupBox3.Controls.Add(this.textBox_binSize);
            this.groupBox3.Controls.Add(this.textBox_mzLow);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(42, 30);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(308, 91);
            this.groupBox3.TabIndex = 63;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Isolation window setting";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(130, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 60;
            this.label10.Text = "-";
            // 
            // textBox_mzHigh
            // 
            this.textBox_mzHigh.Location = new System.Drawing.Point(145, 25);
            this.textBox_mzHigh.Name = "textBox_mzHigh";
            this.textBox_mzHigh.Size = new System.Drawing.Size(47, 20);
            this.textBox_mzHigh.TabIndex = 59;
            this.textBox_mzHigh.Text = "1400";
            // 
            // textBox_binSize
            // 
            this.textBox_binSize.Location = new System.Drawing.Point(101, 56);
            this.textBox_binSize.Name = "textBox_binSize";
            this.textBox_binSize.Size = new System.Drawing.Size(40, 20);
            this.textBox_binSize.TabIndex = 57;
            this.textBox_binSize.Text = "2";
            // 
            // textBox_mzLow
            // 
            this.textBox_mzLow.Location = new System.Drawing.Point(80, 25);
            this.textBox_mzLow.Name = "textBox_mzLow";
            this.textBox_mzLow.Size = new System.Drawing.Size(47, 20);
            this.textBox_mzLow.TabIndex = 52;
            this.textBox_mzLow.Text = "400";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 55;
            this.label7.Text = "mz range:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(147, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 13);
            this.label8.TabIndex = 58;
            this.label8.Text = "mz";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 56;
            this.label9.Text = "Window size:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 125);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(213, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "Isolate on base peak or monoisotopic peak:";
            // 
            // comboBox_baseMono
            // 
            this.comboBox_baseMono.FormattingEnabled = true;
            this.comboBox_baseMono.Location = new System.Drawing.Point(148, 141);
            this.comboBox_baseMono.Name = "comboBox_baseMono";
            this.comboBox_baseMono.Size = new System.Drawing.Size(125, 21);
            this.comboBox_baseMono.TabIndex = 64;
            // 
            // UC_DDApParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button_update);
            this.Name = "UC_DDApParameters";
            this.Size = new System.Drawing.Size(395, 469);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_nMS1;
        private System.Windows.Forms.TextBox textBox_TopN;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_exclustion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Nms2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox_place_ms1_after_ms2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox_binSize;
        private System.Windows.Forms.TextBox textBox_mzLow;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_mzHigh;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox_baseMono;
    }
}
